package mx.nachintoch.vg.chaton;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;

import mx.nachintoch.vg.android.CustomFontButton;
import mx.nachintoch.vg.android.CustomFontTextView;
import mx.nachintoch.vg.android.GameActivity;

/**
 * Actividad de arranque del juego con pantalla inicial que invita a jugar.
 * @author <a href="mailto:manuel_castillo_cc@ciencias.unam.mx" >Manuel "Nachintoch" Castillo</a>
 * @author Ricardo L&aacute;zaro
 * @version 2.0, october 2016
 * @since Misterios en Pregui 0.5, december 2015
 */
public class MainMenu extends ChatonGameActivity {

    // atrbutos de clase

    // métodos de acceso

    // métodos de implementación

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);
        ((CustomFontTextView) findViewById(R.id.welcome)).setFont("GROBOLD.ttf");
        ((CustomFontButton) findViewById(R.id.play_button)).setFont("GROBOLD.ttf");
        ((CustomFontButton) findViewById(R.id.settings_button)).setFont("GROBOLD.ttf");
        ((CustomFontButton) findViewById(R.id.credits_button)).setFont("GROBOLD.ttf");
        if(ChatonGameActivity.DEBUGGING) {
            ChatonGameActivity.ChatonDBHelper dbHelper =
                    new ChatonGameActivity.ChatonDBHelper(this);
            SQLiteDatabase db;
            db = dbHelper.getWritableDatabase();
            ContentValues values = new ContentValues();
            gameProgress = GameActivity.PROGRESS_NONE;
            values.put(ChatonDBHelper.GAME_PROGRESS, gameProgress);
            settingsSave = ChatonGameActivity.getDefaultSave();
            values.put(ChatonDBHelper.SETTINGS, settingsSave);
            score = 0;
            values.put(ChatonDBHelper.SCORE, score);
            db.update(ChatonDBHelper.PARTY_TABLE, values,
                    ChatonDBHelper.PARTY_ID +"=" +ChatonGameActivity.ONLY_PARTY_ID,
                    null);
            db.close();
            dbHelper.close();
        }//resetea el juego por sesión
    }//onCreate

    @Override
    protected void onResume() {
        super.onResume();
        ChatonGameActivity.ChatonDBHelper dbHelper = new ChatonGameActivity.ChatonDBHelper(this);
        SQLiteDatabase db;
        db = dbHelper.getReadableDatabase();
        String[] attributes = new String[] {ChatonDBHelper.GAME_PROGRESS};
        Cursor c = db.query(ChatonDBHelper.PARTY_TABLE, attributes, null, null, null, null, null,
                null);
        c.moveToNext();
        gameProgress = c.getLong(0);
        c.close();
        if(gameProgress == GameActivity.PROGRESS_NONE) {
            ContentValues values = new ContentValues();
            gulty = (byte) (randomizer.nextInt(ChatonGameActivity.MAX_GULTY_ID)
                    +ChatonGameActivity.MIN_GULTY_ID);
            values.put(ChatonDBHelper.GULTY, gulty);
            db.update(ChatonDBHelper.PARTY_TABLE, values, ChatonDBHelper.PARTY_ID +"="
                    +ChatonGameActivity.ONLY_PARTY_ID, null);
        }//si no hay progreso, debe inventar al culpable
        db.close();
        dbHelper.close();
    }//onResume

    @Override
    protected void animationSequence() {}

    @Override
    protected void transitionControl(int lol) {}

    // métodos auxiliares

    /**
     * Responde a la selecci&oacute;n sobre los botones.
     * @param selected - Referencia al bot&oacute;n seleccionado.
     * @since Main Menu 2.0, october 2016
     */
    public void optionSelected(View selected) {
        Intent intent;
        switch (selected.getId()) {
            case R.id.play_button :
                intent = new Intent(this, Map.class);
                intent.putExtra(GameActivity.GAME_PROGRESS_ID, gameProgress);
                intent.putExtra(GameActivity.CURRENT_PARTY_ID, currentParty);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                break;
            case R.id.settings_button :
                intent = new Intent(this, SettingsActivity.class);
                intent.putExtra(GameActivity.GAME_PROGRESS_ID, gameProgress);
                intent.putExtra(GameActivity.CURRENT_PARTY_ID, currentParty);
                startActivity(intent);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                break;
            case R.id.credits_button :
                super.createAboutDialog();
        }//responde adecuadamente a cada botón
    }//opionSelected

    @Override
    protected void resumeGame() {}

    // métodos estáticos

}//MainMenu activity
