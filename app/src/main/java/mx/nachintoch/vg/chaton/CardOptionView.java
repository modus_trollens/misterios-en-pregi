package mx.nachintoch.vg.chaton;

import android.content.Context;
import android.util.AttributeSet;

import mx.nachintoch.vg.android.CustomFontButton;

/**
 * Vista de imagen especial para las opciones de las tarjetas del m&oacute;dulo uno. Incluye un
 * booleano que indica si la tarjeta es la erronea buscada o no.
 * @author <a href="mailto:manuel_castillo_cc@ciencias.unam.mx" >Manuel "Nachintoch" Castillo</a>
 * @version 1.0, december 2015
 * @since Misterios en Pregui 0.5, december 2015
 */
public class CardOptionView extends CustomFontButton {

    // atributos de clase

    /**
     * Indica si esta opci&oacute;n es la erronea buscada, o no.
     * @since Card Option View 1.0, december 2015
     */
    private boolean isErroneous;

    // métodos constructores

    /**
     * Construye una opci&oacute;n de vista de tarjeta con el contexto dado.
     * @param context - El contexto de la aplicaci&oacute;n.
     * @since Card Option View 1.0, december 2015
     */
    public CardOptionView(Context context) {
        super(context);
        setFont("Futura");
        isErroneous = false;
    }//constructor

    /**
     * Constuye una opci&oacute;n de vista de tarjeta con el contexto y atibutos de XML dados.
     * @param context - El contexto de la aplicaci&oacute;n.
     * @param attrs - Los atributos interpretados.
     * @since Card Option View 1.0, december 2015
     */
    public CardOptionView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setFont("Futura");
        isErroneous = false;
    }//constrctor

    // métodos de modificación

    /**
     * Asigna el valor erroneo de la tarjeta.
     * @param is - <tt>true</tt> para indica que esta opci&oacute;n es la erronea buscada en la
     * tarjeta. <tt>false</tt> en otro caso.
     * @since Card Option View 1.0, december 2015
     */
    public void setErroneous(boolean is) {
        isErroneous = is;
    }//setErroneous

    // métodos de acceso

    /**
     * Indica si esta es la opci&oacute;n erronea buscada.
     * @return boolean - <tt>true</tt> si esta opci&oacute;n es la erronea buscada. <tt>false</tt>
     * en otro caso.
     * @since Card Option view 1.0, december 2015
     */
    public boolean isErroneous() {
        return isErroneous;
    }//isErroneous

}//Card Option View
