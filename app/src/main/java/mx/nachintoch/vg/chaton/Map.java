package mx.nachintoch.vg.chaton;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

import mx.nachintoch.vg.android.CustomFontTextView;
import mx.nachintoch.vg.android.GameActivity;
import mx.nachintoch.vg.android.HScroll2D;
import mx.nachintoch.vg.android.MusicHandler;
import mx.nachintoch.vg.android.SpriteView;
import mx.nachintoch.vg.android.VScroll2D;

/**
 * Pantalla con el mapa principal del juego.
 * @author <a href="mailto:manuel_castillo_cc@ciencias.unam.mx" >Manuel "Nachintoch" Castillo</a>
 * @author Ricardo L&aacute;zaro
 * @version 2.0, october 2016
 * @since Misterios en Pregui 0.5, december 2015
 */
public class Map extends ChatonGameActivity {

    // atributos de clase

    /**
     * Referencia al bot&oacute;n s&iacute;.
     * @since Map 0.5, december 2015
     */
    private Button yesButton;

    /**
     * Referncia al bot&oacute;n no.
     * @since Map 0.5, december 2015
     */
    private Button noButton;

    /**
     * Recuerda cu&aacute;l fue el nivel solicitado.
     * @since Map 2.0, october 2016
     */
    private byte requestedLevel;

    /**
     * Referencias a los niveles.
     * @since Map 2.0, novemeber 2016
     */
    private SpriteView level1;
    private SpriteView level2;
    // TODO add other levels

    /**
     * Indica cual fue el &uacute;ltimo nivel elegido.
     * @since Map 2.0, november 2016
     */
    private byte lastLevel;

    /**
     * Referencia al animador de botones.
     * @since Map 2.0, november 2016
     */
    private ButtonsAnimator buttAnim;

    /**
     * Reproducen efectos de sonido no interrumpidos.
     * @since Map 2.0, march 2017
     */
    private MusicHandler levelSound;
    private MusicHandler windFx;

    /**
     * Referencia al Runnable que hace sonar el viento de vez en cuando.
     * @since Map 2.0, march 2017
     */
    private WindBlower windBlower;

    /**
     * Recuerda si ya se ha presentado el nivel.
     * @since Map 2.0, march 2017
     */
    private boolean levelPresented;

    /**
     * Indicador de segunda escena de instrucciones del mapa.
     * @since Map 0.5, december 2015
     */
    private static final byte INSTRUCTIONS2 = 2;
    private static final byte INSTRUCTIONS3 = 3;
    private static final byte INSTRUCTIONS4 = 4;

    /**
     * Indicador de segunda escena de instroducci&oacute;n al m&oacute;dulo 1.
     * @since Map 0.5, december 2015
     */
    private static final byte LEVEL12 = 6;
    private static final byte LEVEL13 = 7;
    private static final byte LEVEL22 = 8;
    private static final byte LEVEL23 = 9;
    private static final byte LEVEL24 = 10;

    /**
     * Indica que no se ha solicitado iniciar ning&uacute;n nivel.
     * @since Map 2.0, october 2016
     */
    private static final byte NONE = -1;

    /**
     * Indicador de transici&oacute;n al nivel 1.
     * @since Map 0.5, december 2015
     */
    private static final byte LEVEL1 = 1;
    private static final byte LEVEL2 = 2;

    /**
     * Identificadores de estados de la actividad.
     * @since Map 2.0, november 2016
     */
    private static final String LAST_LEVEL_ID = "mx.nachintoch.chaton.LAST_LEVEL";
    private static final String LEVEL_PRESENTED_ID = "mx.nachintoch.chaton.LEVEL_PRESENTED";


    // métodos de implementación

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.activated = true;
        setContentView(R.layout.activity_map);
        super.loadCommonGraphics(GameActivity.NO_LEVEL);
        character = (SpriteView) findViewById(R.id.character);
        yesButton = (Button) findViewById(R.id.yes_button);
        noButton = (Button) findViewById(R.id.no_button);
        vScroll = (VScroll2D) findViewById(R.id.vScroll);
        hScroll = (HScroll2D) findViewById(R.id.hScroll);
        level1 = (SpriteView) findViewById(R.id.art_studio);
        level2 = (SpriteView) findViewById(R.id.police_sation);
        background = (RelativeLayout) findViewById(R.id.background);
        requestedLevel = Map.NONE;
        upperFrame = (CustomFontTextView) findViewById(R.id.score_text);
        buttAnim = new ButtonsAnimator();
        upperFrame.setBackgroundResource(R.drawable.level1_top_frame_1824x94);
        setLowerFrameDrawable(R.drawable.level1_lower_frame_1824x91);
        upperFrame.setFont("GROBOLD.ttf");
        super.initAnimatorThreadsReference();
        levelColor = R.color.level1_frame;
            prepareCompleteDisplay(background, null);
        super.activated = false;
        lastLevel = -1;
        levelPresented = savedInstanceState != null &&
                savedInstanceState.getBoolean(Map.LEVEL_PRESENTED_ID, false);
    }//onCreate

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        pauseAllowed = resultCode != GameActivity.END_GAME_FINISH_STATUS;
    }//onActivityResult

    @Override
    protected void onResume() {
        super.onResume();
        windFx = new MusicHandler(this);
        windFx.load(R.raw.wind, false);
        levelSound = new MusicHandler(this);
        windBlower = new WindBlower();
        loadCommonSounds();
        soundsHandlers.add(levelSound);
        soundsHandlers.add(windFx);
        handler.postDelayed(windBlower, randomizer.nextInt(windFx.getTrackLength() *3));
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
            translationAnim(character, 0,
                    getResources().getDimension(R.dimen.chaton_margin), null);
        }//run
    }, GameActivity.ANIMATION_DURATION);
        super.activated = pauseAllowed;
        waiting = false;
        if(!pauseAllowed && !levelPresented) {
            initDialogAfterAnim(bubbleContainer, typewriterView, R.string.map_instructions1,
                    character, true, false, Map.INSTRUCTIONS2, R.raw.map_instructions1, true,
                    Math.round(GameActivity.ANIMATION_DURATION
                            *getFloatResource(R.dimen.map_presentaion_delay)), false, null, true);
            levelPresented = true;
        } else {
            absoluteBackground.animate().setDuration(GameActivity.ANIMATION_DURATION);
            absoluteBackground.animate().scaleX(1);
            absoluteBackground.animate().scaleY(1);
            absoluteBackground.animate().setListener(null);
            absoluteBackground.animate().start();
            super.enableSprites(true, true, level1, level2); //TODO add other levels
        }//si la pausa no está permitida
        switch(lastLevel) {
            case Map.LEVEL2 :
                vScroll.scrollTo(0, vScroll.getHeight());
                hScroll.scrollTo(0, hScroll.getHeight());
                // TODO add other levels
        }//scrollea al último nivel accedido
        displayScore();
        switch(lastLevel) {
            case Map.LEVEL2 :
                background.setX(0);
                background.setY(50);
                break;
            // todo add other levels */
        }//scrollea apropiadamente al volver de  un nivel
        if(lastLevel != Map.NONE) {
            terminateDialog(bubbleContainer, character, taps, toTap);
        }//si se viene de otro nivel
        ChatonDBHelper dbHelper = new ChatonDBHelper(this);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        gameProgress |= ChatonGameActivity.PROGRESS_HISTORY;
        ContentValues values = new ContentValues();
        values.put(ChatonDBHelper.GAME_PROGRESS, gameProgress);
        values.put(ChatonDBHelper.SCORE, score);
        db.update(ChatonDBHelper.PARTY_TABLE, values, ChatonDBHelper.PARTY_ID +" = "
                +ChatonGameActivity.ONLY_PARTY_ID, null);
        db.close();
        dbHelper.close();
        fadeInAnim(fadeScreen).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                if(background.getX() != 0) {
                    background.animate().scaleX(1);
                    background.animate().scaleY(1);
                    background.animate().x(0);
                    background.animate().y(0);
                    background.animate().setDuration(GameActivity.ANIMATION_DURATION);
                    background.animate().setListener(null);
                    background.animate().start();
                    if(!pauseAllowed) {
                        AnimatorSet anim = getScrollAnimation(
                                absoluteBackground.getMeasuredWidth() /2,
                                absoluteBackground.getMeasuredHeight() -150,
                                GameActivity.ANIMATION_DURATION);
                        anim.start();
                    }//realiza la animación que centra el mapa en los primeros niveles
                }//realiza la animación de introducción
            }//si hace falta animar el fondo
        }).start();
        System.gc();
    }//onResume

    @Override
    protected void onPause() {
        handler.removeCallbacks(windBlower);
        super.onPause();
    }//onPause

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return fadeScreen.getAlpha() == 1 || super.onTouchEvent(event);
    }//onTouch

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putByte(Map.LAST_LEVEL_ID, lastLevel);
        savedInstanceState.putBoolean(Map.LEVEL_PRESENTED_ID, levelPresented);
        super.onSaveInstanceState(savedInstanceState);
    }//onSaveInstanceState

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        lastLevel = savedInstanceState.getByte(Map.LAST_LEVEL_ID);
    }//onRestoreInstanceState

    /**
     * Responde al toque sobre el bot&oacute;n s&iacute;.
     * @param button - Referencia al bot&oacute;n s&iacute;.
     * @since Map 0.5, december 2015
     */
    public void yesClicked(View button) {
        nextSequence = GameActivity.DO_NOTHING;
        final Map _this = this;
        disappearDecisionButtons();
        absoluteBackground.animate().setDuration(GameActivity.ANIMATION_DURATION);
        absoluteBackground.animate().scaleX(getFloatResource(R.dimen.map_scale));
        absoluteBackground.animate().scaleY(getFloatResource(R.dimen.map_scale));
        switch(requestedLevel) {
            case Map.LEVEL2 :
                background.animate().x(background.getMeasuredWidth() /2);
                background.animate().setDuration(GameActivity.ANIMATION_DURATION);
                background.animate().start();
            // TODO traslate to other levels
        }//anima hacia el nivel seleccionado
        AnimatorSet anim = getScrollAnimation(absoluteBackground.getWidth(),
                absoluteBackground.getHeight(), GameActivity.ANIMATION_DURATION);
        absoluteBackground.animate().setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                Intent intent;
                switch(requestedLevel) {
                    case Map.LEVEL1 :
                        intent = new Intent(_this, Module1.class);
                        break;
                    case Map.LEVEL2 :
                        intent = new Intent(_this, Module2.class);
                        break;
                    default :
                        return;
                    // TODO si fue otro nivel
                }//actúa dependiendo la última secuencia de texto mostrada
                intent.putExtra(GameActivity.GAME_PROGRESS_ID, gameProgress);
                intent.putExtra(GameActivity.CURRENT_PARTY_ID, currentParty);
                intent.putExtra(GameActivity.SCORE_ID, score);
                startActivityForResult(intent, GameActivity.END_GAME_FINISH_STATUS);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }//inicia el nivel adecuado
        });
        fadeScreen.animate().alpha(1);
        fadeScreen.animate().start();
        anim.start();
        absoluteBackground.animate().start();
    }//yesClicked

    /**
     * Responde al toque sobre el bot&oacute;n no.
     * @param button - Referencia al bot&ocute;n  no.
     * @since Map 0.5, december 2015
     */
    public void noClicked(View button) {
        nextSequence = GameActivity.DO_NOTHING;
        terminateDialog(bubbleContainer, character, taps, toTap);
        disappearDecisionButtons();
        fadeScreen.animate().start();
        level1.setActive(true);
        level2.setActive(true);
        // TODO set active all levels
        waiting = false;
        pauseAllowed = true;
        activated = true;
    }//noClicked

    /**
     * Responde al toque sobre uno de los niveles.
     * @param level - Referencia al nivel tocado.
     * @since Map 2.0, october 2016
     */
    public void levelClicked(View level) {
        stopTaps(taps, toTap);
        if(!pauseAllowed) {
            return;
        }//si no está listo, a la goma
        pauseAllowed = false;
        waiting = true;
        ((SpriteView) level).setActive(false);
        switch(level.getId()) {
            case R.id.art_studio :
                requestedLevel = Map.LEVEL1;
                lastLevel = Map.LEVEL1;
                levelSound.load(R.raw.door_knock, false);
                if(voiceOn) levelSound.play();
                initDialogAfterAnim(bubbleContainer, typewriterView, R.string.mod1_presentation1,
                        character, true, false, Map.LEVEL12, R.raw.mod1_presentation1, false, 0,
                        true, null, true);
                break;
            case R.id.police_sation :
                requestedLevel = Map.LEVEL2;
                lastLevel = Map.LEVEL2;
                levelSound.load(R.raw.siren, false);
                if(voiceOn) levelSound.play();
                initDialogAfterAnim(bubbleContainer, typewriterView, R.string.mod2_presentation1,
                        character, true, false, Map.LEVEL22, R.raw.mod2_presentation1, false, 0,
                        true, null, true);
                break;
            // TODO implement other levels
            default:
                requestedLevel = Map.NONE;
        }//muestra el diálogo adecuado para cada nivel y prepara el estado de la aplicación
    }//studioClicked

    @Override
    protected void animationSequence() {
        switch (nextSequence) {
            case Map.INSTRUCTIONS2 :
                nextSequence = Map.INSTRUCTIONS3;
                displayDialog(R.string.map_instructions2, bubbleContainer, taps, toTap,
                        R.raw.map_instructions2);
                return;
            case Map.INSTRUCTIONS3 :
                nextSequence = Map.INSTRUCTIONS4;
                displayDialog(R.string.map_instructions3, bubbleContainer, taps, toTap,
                        R.raw.map_instructions3);
                break;
            case Map.INSTRUCTIONS4 :
                nextSequence = GameActivity.DO_NOTHING;
                displayDialog(R.string.map_instructions4, bubbleContainer, taps, toTap,
                        R.raw.map_instructions4);
                activated = true;
                pauseAllowed = true;
                break;
            case Map.LEVEL12 :
                nextSequence = Map.LEVEL13;
                displayDialog(R.string.mod1_presentation2, bubbleContainer, taps, toTap,
                        R.raw.mod1_presentation2);
                break;
            case Map.LEVEL13 :
                initDialog(bubbleContainer, typewriterView, R.string.mod1_presentation3, character,
                        true, false, GameActivity.NOP, R.raw.mod1_presentation3, false, 0, buttAnim,
                        false);
                break;
            case Map.LEVEL22 :
                nextSequence = LEVEL23;
                displayDialog(R.string.mod2_presentation2, bubbleContainer, taps, toTap,
                        R.raw.mod2_presentation2);
                break;
            case Map.LEVEL23 :
                nextSequence = Map.LEVEL24;
                displayDialog(R.string.mod2_presentation3, bubbleContainer, taps, toTap,
                        R.raw.mod2_presentation3);
                break;
            case Map.LEVEL24 :
                nextSequence = GameActivity.NOP;
                initDialog(bubbleContainer, typewriterView, R.string.mod2_presentation4, character,
                        true, false, GameActivity.NOP, R.raw.mod2_presentation4, false, 0, buttAnim,
                        false);
                break;
            case GameActivity.NOP :
                break;
            case GameActivity.SET_READY :
                pauseAllowed = true;
                nextSequence = GameActivity.DO_NOTHING;
            case GameActivity.DO_NOTHING :
                terminateDialog(bubbleContainer, character, taps, toTap);
                startTaps(drag, toDrag, GameActivity.CLICK_INDICATOR_DURATION);
                stopTaps(taps, toTap);
            default :
        }//actua de acuerdo a la etapa en la que se encuentre
    }//animationSequence

    @Override
    protected void transitionControl(int next) {}

    /**
     * Hace visibles los botones de decisi&oacute;n.
     * @since Map 1.0, january 2016
     */
    private void appearDecisionButtons() {
        float y = bubbleContainer.getY() +bubbleContainer.getHeight()
                -bubbleContainer.getPaddingBottom() *2 -yesButton.getMeasuredHeight();
        yesButton.setX(bubbleContainer.getX() +bubbleContainer.getPaddingStart() *2);
        yesButton.setY(y);
        noButton.setX(bubbleContainer.getX() +bubbleContainer.getMeasuredWidth()
                -noButton.getMeasuredWidth() -bubbleContainer.getPaddingEnd() *2);
        noButton.setY(y);
        fadeScreen.setVisibility(View.VISIBLE);
        fadeScreen.animate().setDuration(GameActivity.ANIMATION_DURATION);
        fadeScreen.animate().alpha(getFloatResource(R.dimen.map_options_fade));
        fadeScreen.animate().setListener(null);
        fadeScreen.animate().start();
        yesButton.setVisibility(View.VISIBLE);
        noButton.setVisibility(View.VISIBLE);
    }//appearDecisionButtons

    /**
     * Hace invisibles los botones de decisi&oacute;n.
     * @since Map 2.0, october 2016
     */
    private void disappearDecisionButtons() {
        fadeScreen.animate().setDuration(GameActivity.ANIMATION_DURATION);
        fadeScreen.animate().alpha(0);
        yesButton.setVisibility(View.INVISIBLE);
        noButton.setVisibility(View.INVISIBLE);
    }//disappearDecisionButtons

    @Override
    protected void resumeGame() {}

    // clases anidadas

    /**
     * Anima los botones para aparecer dentro del globo de texto.
     * @author <a href="mailto:manuel_castillo_cc@ciencias.unam.mx" >Manuel "Nachintoch"
     * Castillo</a>
     * @version 1.0, november 2016
     * @since Map 2.0, november 2016
     */
    private class ButtonsAnimator extends AnimatorListenerAdapter {

        @Override
        public void onAnimationEnd(Animator anim) {
            appearDecisionButtons();
        }//onAnimationEnd

    }//ButtonsAnimator

    /**
     * Se encarga de hacer sonar el viento cada cierto tiempo. La referencia nos ayuda a congelar el
     * sonido una vez que se ha pasado el mapa a segundo plano.
     * @author <a href="mailto:manuel_castillo_cc@ciencias.unam.mx" >Manuel "Nachintoch"
     * Castillo</a>
     * @version 1.0, march 2017
     * @since Map 2.0, march 2017
     */
    private class WindBlower implements Runnable {

        @Override
        public void run() {
            handler.removeCallbacks(this);
            windFx.rewind();
            if(voiceOn) windFx.play();
            handler.postDelayed(this, randomizer.nextInt(windFx.getTrackLength() *3));
        }//vuelve a reproducir el sonido del viento

    }//Wind Blower Runnable

}//Map Game Activity
