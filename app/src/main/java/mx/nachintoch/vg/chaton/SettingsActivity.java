package mx.nachintoch.vg.chaton;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;

import mx.nachintoch.vg.android.CustomFontButton;
import mx.nachintoch.vg.android.CustomFontCheckBox;
import mx.nachintoch.vg.android.CustomFontRadioButton;
import mx.nachintoch.vg.android.CustomFontTextView;
import mx.nachintoch.vg.android.GameActivity;

/**
 * Muestra las opciones de configuraci&oacute;n de la aplicaci&oacute;n.
 * @author <a href="mailto:manuel_castillo_cc@ciencias.unam.mx" >Manuel "Nachintoch" Castillo</a>
 * @version 1.0, october 2016
 * @since Misterios en Pregui 2.0, october 2016
 */
public class SettingsActivity extends ChatonGameActivity {

    // atributos de clase

    /**
     * Referencia al bot&oacute;n binario para activar o desactivar la m&uacute;sica.
     * @since Settings Activity 1.0, october 2016
     */
    private CustomFontCheckBox musicSetting;

    /**
     * Referencia al bot&oacute;n binario para activar o desactivar los efectos de sonido.
     * @since Settings Activity 1.0, october 2016
     */
    private CustomFontCheckBox fxSetting;

    /**
     * Referencia al bot&oacute;n de grupo que indica el nivel de dificultad <b>f&aacute;cil</b>
     * @since Settings Activity 1.0, october 2016
     */
    private CustomFontRadioButton easySetting;

    /**
     * Referencia al bot&oacute;n de grupo que indica el nivel de dificultad <b>normal</b>
     * @since Settings Activity 1.0, october 2016
     */
    private CustomFontRadioButton normalSetting;

    /**
     * Referencia al bot&oacute;n de grupo que indica el nivel de dificultad <b>dif&iacute;cil</b>
     * @since Settings Activity 1.0, october 2016
     */
    private CustomFontRadioButton hardSetting;

    /**
     * Referencia al manejador de la base de datos del juego.
     * @since Settings Activity 1.0, february 2017
     */
    private SQLiteDatabase gameDatabase;

    // métodos de implementación

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        ((CustomFontTextView) findViewById(R.id.settings_title)).setFont("GROBOLD.ttf");
        musicSetting = (CustomFontCheckBox) findViewById(R.id.music_setting);
        musicSetting.setFont("Futura");
        fxSetting = (CustomFontCheckBox) findViewById(R.id.soundfx_setting);
        fxSetting.setFont("Futura");
        ((CustomFontTextView) findViewById(R.id.difficulty_setting)).setFont("Futura");
        easySetting = (CustomFontRadioButton) findViewById(R.id.easy_setting);
        easySetting.setFont("Futura");
        normalSetting = (CustomFontRadioButton) findViewById(R.id.normal_setting);
        normalSetting.setFont("Futura");
        hardSetting = (CustomFontRadioButton) findViewById(R.id.hard_setting);
        hardSetting.setFont("Futura");
        ((CustomFontButton) findViewById(R.id.errase_setting)).setFont("GROBOLD.ttf");
        ((CustomFontButton) findViewById(R.id.back_button)).setFont("GROBOLD.ttf");
    }//onCreate

    @Override
    protected void onResume() {
        super.onResume();
        ChatonDBHelper dbHelper = new ChatonDBHelper(this);
        gameDatabase = dbHelper.getWritableDatabase();
        Cursor c = gameDatabase.query(ChatonDBHelper.PARTY_TABLE, new String[]{ChatonDBHelper.GAME_PROGRESS,
                        ChatonDBHelper.SCORE, ChatonDBHelper.GULTY, ChatonDBHelper.SETTINGS},
                ChatonDBHelper.PARTY_ID +" = " +ChatonGameActivity.ONLY_PARTY_ID, null, null, null,
                null);
        c.moveToNext();
        gameProgress = c.getLong(0);
        score = c.getLong(1);
        gulty = (byte) c.getInt(2);
        settingsSave = c.getInt(3);
        c.close();
        setSettings();
        musicSetting.setChecked(musicOn);
        fxSetting.setChecked(voiceOn);
        easySetting.setChecked(currentDifficulty == GameActivity.EASY);
        normalSetting.setChecked(currentDifficulty == GameActivity.NORMAL);
        hardSetting.setChecked(currentDifficulty == GameActivity.HARD);
    }//onResume

    @Override
    protected void onStop() {
        gameDatabase.close();
        super.onStop();
    }//onStop

    @Override
    protected void animationSequence() {}

    @Override
    protected void transitionControl(int nextSequence) {}

    /**
     * Responde a el uso sobre los elemetos de configuraci&oacute;n.
     * @param item - El elemento seleccionado en pantalla.
     * @since Settings Activity 1.0, october 2016
     */
    public void itemSelected(View item) {
        ContentValues values = new ContentValues();
        boolean updateSettings = false;
        switch(item.getId()) {
            case R.id.music_setting :
                musicOn = musicSetting.isChecked();
                settingsSave = musicOn ? settingsSave |ChatonGameActivity.MUSIC_SETTING :
                        settingsSave &(~ChatonGameActivity.MUSIC_SETTING);
                values.put(ChatonDBHelper.SETTINGS, settingsSave);
                updateSettings = true;
                break;
            case R.id.soundfx_setting :
                voiceOn = fxSetting.isChecked();
                settingsSave = voiceOn ? settingsSave |ChatonGameActivity.FX_SETTING :
                        settingsSave &(~ChatonGameActivity.FX_SETTING);
                values.put(ChatonDBHelper.SETTINGS, settingsSave);
                updateSettings = true;
                break;
            case R.id.errase_setting :
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle(R.string.erase_all_setting);
                builder.setMessage(getString(R.string.erase_all_confirm));
                builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ContentValues values = new ContentValues();
                        gameProgress = GameActivity.PROGRESS_NONE;
                        values.put(ChatonDBHelper.GAME_PROGRESS, gameProgress);
                        settingsSave = ChatonGameActivity.getDefaultSave();
                        values.put(ChatonDBHelper.SETTINGS, settingsSave);
                        score = 0;
                        values.put(ChatonDBHelper.SCORE, score);
                        gameDatabase.update(ChatonDBHelper.PARTY_TABLE, values,
                                ChatonDBHelper.PARTY_ID +"=" +ChatonGameActivity.ONLY_PARTY_ID,
                                null);
                        dialog.dismiss();
                        finish();
                    }//onClick
                });
                builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }//onClick
                });
                builder.create().show();
                break;
            case R.id.back_button :
                finish();
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                break;
            case R.id.easy_setting :
                currentDifficulty = GameActivity.EASY;
                settingsSave &= ChatonGameActivity.RESET_DEFAULT_DIFFICULTY;
                settingsSave |= ChatonGameActivity.DEFAULT_EASY_DIFFICULTY;
                values.put(ChatonDBHelper.SETTINGS, settingsSave);
                updateSettings = true;
                break;
            case R.id.normal_setting :
                currentDifficulty = GameActivity.NORMAL;
                settingsSave &= ChatonGameActivity.RESET_DEFAULT_DIFFICULTY;
                settingsSave |= ChatonGameActivity.DEFAULT_NORMAL_DIFFICULTY;
                values.put(ChatonDBHelper.SETTINGS, settingsSave);
                updateSettings = true;
                break;
            case R.id.hard_setting :
                currentDifficulty = GameActivity.HARD;
                settingsSave &= ChatonGameActivity.RESET_DEFAULT_DIFFICULTY;
                settingsSave |= ChatonGameActivity.DEFAULT_HARD_DIFFICULTY;
                values.put(ChatonDBHelper.SETTINGS, settingsSave);
                updateSettings = true;
        }//actúa dependiendo el elemento seleccionado
        if(updateSettings) {
            gameDatabase.update(ChatonDBHelper.PARTY_TABLE, values, ChatonDBHelper.PARTY_ID
                    +"=" +ChatonGameActivity.ONLY_PARTY_ID, null);
        }//actualiza las configuraciones en la base de datos
    }//itemSelected

    @Override
    protected void resumeGame() {}

}//Settings Activity
