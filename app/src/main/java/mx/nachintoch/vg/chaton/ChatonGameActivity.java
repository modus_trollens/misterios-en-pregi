package mx.nachintoch.vg.chaton;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.drawable.AnimationDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

import mx.nachintoch.vg.android.CustomFontButton;
import mx.nachintoch.vg.android.CustomFontTextView;
import mx.nachintoch.vg.android.DragScreen;
import mx.nachintoch.vg.android.GameActivity;
import mx.nachintoch.vg.android.MusicHandler;
import mx.nachintoch.vg.android.TextBubbleContainer;
import mx.nachintoch.vg.android.Typewriter;

/**
 * Define constantes y m&eacute;todos generales para todas las actividades en el juego.
 * @author <a href="mailto:manuel_castillo_cc@ciencias.unam.mx" >Manuel "Nachintoch" Castillo</a>
 * @version 1.0, october 2016
 * @since Misterios en Pregui 3.0, october 2016
 */
public abstract class ChatonGameActivity extends DragScreen {

    /**
     * Referencia a la vista de desvanecimiento.
     * @since Map 2.0, october 2016
     */
    protected ImageView fadeScreen;

    /**
     * Referencia al fondo absoluto de todas las vistas en el mapa
     * @since Map 2.0, october 2016
     */
    protected RelativeLayout absoluteBackground;

    /**
     * Referencia a la vista superior de la pantalla. Es la misma que contiene el puntaje
     * @since Module 1 3.0, november 2016
     */
    protected CustomFontTextView upperFrame;

    /**
     * Referencia a la vista que contiene el globo de texto.
     * @since Module 1 3.0, november 2016
     */
    protected TextBubbleContainer bubbleContainer;

    /**
     * Contiene el n&uacute;mer de tarjetas mostradas.
     * @since Module 1 1.0, january 2016
     */
    protected byte shownQuestions;

    /**
     * Cuenta el n&uacute;mero de preguntas que han sido correctamente
     * respondidas por el jugador.
     * @since Level 1 1.5, may 2016
     */
    protected int correctQuestions;

    /**
     * Cuenta el n&uacute;mero de errores cometidos durante la sesi&oacute;n.
     * @since Module 1 1.0, january 2016
     */
    protected byte errorsCounter;

    /**
     * Referencia del recurso del color del nivel.
     * @since Chaton Game Activity 1.0, november 2016
     */
    protected int levelColor;

    /**
     * Referencia al animador de Chat&oacute;n.
     * @since Chaton Game Activity 1.0, november 2016
     */
    protected ChatonAnimator chatAnim;

    /**
     * Recuerda la &uacute;ltima posici&oacute;n de la frase correcta a responder.
     * @since Chaton Game Activity 1.0, november 2016
     */
    protected byte lastAnsPos;

    /**
     * Referencia al portafolio de pistas.
     * @since Chaton Game Activty 1.0, december 2016
     */
    protected RelativeLayout cluesPorfolio;

    /**
     * Contiene el tama&ntilde;o de la pantalla, entre otra informaci&oacute;n.
     * @since Chaton Game Activty 1.0, december 2016
     */
    protected DisplayMetrics displayMetrics;

    /**
     * Indica si se est&aacute;n mostrando las pistas o no.
     * @since chaton Game Activity 1.0, december 2016
     */
    protected boolean showingClues;

    /**
     * Referencia a la pista que ser&aacute; animada cuando el jugador junte una pista.
     * @since Chaton Game Activity 1.0, february 2017
     */
    protected ImageView jigsaw;

    /**
     * Recuerda si ya se le ha dado una pista al jugador en un nivel dado.
     * @since Chaton Game Activity 1.0, ferbuary 2017
     */
    protected byte clueAwarded;

    /**
     * Reproductores de sonido comunes a todos los niveles.
     * @since Chaton Game Activity 1.0, march 2017
     */
    protected MusicHandler normalTimer;
    protected MusicHandler hardTimer;

    /**
     * Recuerda si est&aacute; animando las pistas.
     * @since Chaton Game Activity 1.0, march 2'17
     */
    protected boolean animatingClues;

    /**
     * Recuerda qui&eacute;n es el culpable de este juego.
     * @since Chaton Game Activity 1.0, march 2017
     */
    protected byte gulty;

    /**
     * Recuerda la dificultad actual
     * @since Chaton Game Activity 1.0, march 2017
     */
    protected int settingsSave;

    /**
     * Indica si se debe ajustar la dificultad de un nivel o no. Solo deber&iacute;a hacerse en caso
     * de error.
     * @since Chaton Game Activity 1.0, march 2017
     */
    protected boolean adjustDifficulty;

    /**
     * Referencias a la im&aacute;gen y su animaci&oacute;n que indican al usuario qu&eacute; hacer
     * durante el tutorial.
     * @since Chaton Game Activity 1.0, march 2017
     */
    protected ImageView tutorialTap;
    protected AnimationDrawable tutorialTaps;

    /**
     * Recuerda la altura del marco inferior
     * @since Chaton Game Activity 1.0, april 2017
     */
    protected int lowerFrameH;

    /**
     * Indica que a llegado a ver la historia.
     * @since Game Activity 1.0, december 2015
     */
    protected static final long PROGRESS_HISTORY = 0b00000000000000001;

    /**
     * Indicadores de pistas obtenidas.
     * @since Game Activity 1.0, december 2015
     */
    protected static final long PROGRESS_LEVEL1_1 = 0b0000000000000000000000000000000000000000010;
    protected static final long PROGRESS_LEVEL1_2 = 0b0000000000000000000000000000000000000000100;
    protected static final long PROGRESS_LEVEL1_3 = 0b0000000000000000000000000000000000000001000;
    protected static final long PROGRESS_LEVEL1_4 = 0b0000000000000000000000000000000000000010000;
    protected static final long PROGRESS_LEVEL12_1 =0b0000000000000000000000000000000000000100000;
    protected static final long PROGRESS_LEVEL12_2 =0b0000000000000000000000000000000000001000000;
    protected static final long PROGRESS_LEVEL13_1 =0b0000000000000000000000000000000000010000000;
    protected static final long PROGRESS_LEVEL13_2 =0b0000000000000000000000000000000000100000000;
    protected static final long PROGRESS_LEVEL15_1 =0b0000000000000000000000000000000001000000000;
    protected static final long PROGRESS_LEVEL15_2 =0b0000000000000000000000000000000010000000000;

    protected static final long PROGRESS_LEVEL2_1 = 0b0000000000000000000000000000000100000000000;
    protected static final long PROGRESS_LEVEL2_2 = 0b0000000000000000000000000000001000000000000;
    protected static final long PROGRESS_LEVEL2_3 = 0b0000000000000000000000000000010000000000000;
    protected static final long PROGRESS_LEVEL2_4 = 0b0000000000000000000000000000100000000000000;
    protected static final long PROGRESS_LEVEL24_1 =0b0000000000000000000000000001000000000000000;
    protected static final long PROGRESS_LEVEL24_2 =0b0000000000000000000000000010000000000000000;
    protected static final long PROGRESS_LEVEL25_1 =0b0000000000000000000000000100000000000000000;
    protected static final long PROGRESS_LEVEL25_2 =0b0000000000000000000000001000000000000000000;

    protected static final long PROGRESS_LEVEL3_1 = 0b0000000000000000000000010000000000000000000;
    protected static final long PROGRESS_LEVEL3_2 = 0b0000000000000000000000100000000000000000000;
    protected static final long PROGRESS_LEVEL3_3 = 0b0000000000000000000001000000000000000000000;
    protected static final long PROGRESS_LEVEL3_4 = 0b0000000000000000000010000000000000000000000;
    protected static final long PROGRESS_LEVEL34_1 =0b0000000000000000000100000000000000000000000;
    protected static final long PROGRESS_LEVEL34_2 =0b0000000000000000001000000000000000000000000;
    protected static final long PROGRESS_LEVEL35_1 =0b0000000000000000010000000000000000000000000;
    protected static final long PROGRESS_LEVEL35_2 =0b0000000000000000100000000000000000000000000;

    protected static final long PROGRESS_LEVEL4_1 = 0b0000000000000001000000000000000000000000000;
    protected static final long PROGRESS_LEVEL4_2 = 0b0000000000000010000000000000000000000000000;
    protected static final long PROGRESS_LEVEL4_3 = 0b0000000000000100000000000000000000000000000;
    protected static final long PROGRESS_LEVEL4_4 = 0b0000000000001000000000000000000000000000000;
    protected static final long PROGRESS_LEVEL45_1 =0b0000000000010000000000000000000000000000000;
    protected static final long PROGRESS_LEVEL45_2 =0b0000000000100000000000000000000000000000000L;

    protected static final long PROGRESS_LEVEL5_1 = 0b0000000001000000000000000000000000000000000L;
    protected static final long PROGRESS_LEVEL5_2 = 0b0000000010000000000000000000000000000000000L;
    protected static final long PROGRESS_LEVEL5_3 = 0b0000000100000000000000000000000000000000000L;
    protected static final long PROGRESS_LEVEL5_4 = 0b0000001000000000000000000000000000000000000L;

    protected static final long PROGRESS_LEVEL1_TUTORIAL =
            0b0000010000000000000000000000000000000000000L;
    protected static final long PROGRESS_LEVEL2_TUTORIAL =
            0b0000100000000000000000000000000000000000000L;
    protected static final long PROGRESS_LEVEL3_TUTORIAL =
            0b0001000000000000000000000000000000000000000L;
    protected static final long PROGRESS_LEVEL4_TUTORIAL =
            0b0010000000000000000000000000000000000000000L;
    protected static final long PROGRESS_LEVEL5_TUTORIAL =
            0b0100000000000000000000000000000000000000000L;
    protected static final long PROGRESS_LEVEL6_TUTORIAL =
            0b1000000000000000000000000000000000000000000L;

    /**
     * Identificadores de distribuci6oacute;n de informaci&oacute;n en el campo de configuraciones
     * de la base de datos.
     * @since Main Menu 2.0, february 2017
     */
    public static final int MUSIC_SETTING = 0b00001;
    public static final int FX_SETTING = 0b00010;
    public static final int DEFAULT_EASY_DIFFICULTY = 0b00100;
    public static final int DEFAULT_NORMAL_DIFFICULTY = 0b01000;
    public static final int DEFAULT_HARD_DIFFICULTY = 0b10000;
    public static final int RESET_DEFAULT_DIFFICULTY = 0b00011;

    /**
     * Identificadores numericos de los niveles.
     * @since Game Activity 1.0, december 2015
     */
    protected static final byte LEVEL1 = 1;
    protected static final byte LEVEL2 = 2;
    protected static final byte LEVEL3 = 3;
    protected static final byte LEVEL4 = 4;
    protected static final byte LEVEL5 = 5;
    protected static final byte LEVEL6 = 6;

    /**
     * Multiplicador de dificultad f&aacute;cil.
     * @since Level 1 2.0, may 2016
     */
    public static final byte EASY_MULTIPLIER = 1;

    /**
     * Multiplicador de dificultad normal.
     * @since Level 1 2.0, may 2016
     */
    public static final byte NORMAL_MULTIPLIER = 2;

    /**
     * Multiplicador de dificultad dif&iacute;cil.
     * @since Level 1 2.0, may 2016
     */
    public static final byte HARD_MULTIPLIER = 3;

    /**
     * Valor de una pregunta.
     * @since Level 1 2.0, may 2016
     */
    public static final byte QUESTION_VALUE = 10;

    /**
     * Multiplicador de puntos para el nivel 1.
     * @since Level 1 2.0, may 2016.
     */
    public static final byte LEVEL1_MULTIPLIER = 5;
    public static final byte LEVEL2_MULTIPLIER = 10;

    // TODO add other levels

    /**
     * N&uacute;mero de preguntas a hacer al jugador m&aacute;ximo por
     * sesi&acute;n.
     * @since Level 1 2.0, may 2016
     */
    public static final byte QUESTION_LIMIT = 10;

    /**
     * Tiempo de retraso entre cada cuadro de las animaciones.
     * @since Chaton Game Activity 1.0, november 2016
     */
    public static final long ANIMATION_DELAY = 100;

    /**
     * Indica el m&aacute;ximo n&uacute;mero de intentos fallidos.
     * @since Module 1 1.0, january 2016
     */
    public static final byte ERROR_LIMIT = 3;

    /**
     * Indica cuando el l&iacute;mite de tiempo para responder una tarjeta.
     * @since Level 1 2.0, may 2016
     */
    public static final long TIMEOUT = 65000;

    /**
     * Secuencia de tiempo para que ocurra un evento.
     * @since Module 1 3.0, november 2016
     */
    public static final long TIME_STEP = TIMEOUT /3;

    /**
     * Identificador de la venti&uacute;nica partida que maneja la juego.
     * @since Module 1 3.0, february 2017
     */
    protected static final byte ONLY_PARTY_ID = 1;

    /**
     * N&uacute;mero de ejercicios a los cuales se asigna una pista.
     * @since Chaton Game Activity 1.0, february 2017
     */
    public static final byte CLUE_GET_THRESHOLD = 5;

    /**
     * Indica el estado de la aplicaci&oacute;n: depuraci&oacute;n o en producci&oacute;n.
     * @since Chaton Game Activity 1.0, march 2017
     */
    public static final boolean DEBUGGING = true;

    /**
     * N&uacute;mero total de pistas en el juego.
     * @since Chaton Game Activity 1.0, march 2017
     */
    public static final byte TOTAL_CLUES_NUMBER = 36;

    /**
     * Identificadores de los personajes como culpables.
     * @since Chaton Game Activity 2017
     */
    public static final byte UNDEFINED_GULTY = -1;
    public static final byte PACO_GULTY = 1;
    public static final byte JL_GULTY = 2;
    public static final byte JUAN_GULTY = 3;
    public static final byte MIN_GULTY_ID = 1;
    public static final byte MAX_GULTY_ID = 3;

    // métodos de implemetación

    @Override
    protected void onCreate(Bundle savedInstance) {
        super.onCreate(savedInstance);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
        displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        randomizer = new Random();
        handler = new Handler();
    }//onCreate

    @Override
    protected AlertDialog createPauseMenu() {
        final AlertDialog pauseDialog = new AlertDialog.Builder(this).create();
        CustomFontTextView title = new CustomFontTextView(this);
        title.setText(R.string.pause);
        title.setFont("Futura");
        title.setTextSize(getResources().getDimension(R.dimen.text_size));
        title.setBackgroundResource(levelColor);
        title.setPadding(getResources().getDimensionPixelSize(R.dimen.title_padding), 0, 0, 0);
        pauseDialog.setCustomTitle(title);
        View promptView = LayoutInflater.from(this).inflate(R.layout.dialog_pause, null);
        CustomFontButton button = (CustomFontButton) promptView.findViewById(R.id.continue_button);
        button.setFont("Futura");
        promptView.setBackgroundResource(levelColor);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View dialog) {
                pauseRequested = false;
                resumeAnimatorThreads();
                resumeGame();
                pauseDialog.dismiss();
            }//onClick
        });
        button = (CustomFontButton) promptView.findViewById(R.id.about_button);
        button.setFont("Futura");
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View dialog) {
                createAboutDialog();
            }//onClick
        });
        if(this instanceof Map) {
            button = (CustomFontButton) promptView.findViewById(R.id.exit_button);
            button.setFont("Futura");
            button.setText(R.string.exit);
            button.setVisibility(View.VISIBLE);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View dialog) {
                    setResult(GameActivity.END_GAME_FINISH_STATUS);
                    if (loopPlayer != null) {
                        loopPlayer.dispose();
                    }//libera los recursos del reproductor
                    pauseDialog.dismiss();
                    finish();
                }//onClick
            });
        } else {
            button.setFont("Futura");
            button.setText(R.string.back_to_map);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View dialog) {
                    Intent result = new Intent();
                    result.putExtra(GameActivity.SCORE_ID, score);
                    setResult(Activity.RESULT_OK, result);
                    if (loopPlayer != null) {
                        loopPlayer.dispose();
                    }//libera los recursos del reproductor
                    pauseDialog.dismiss();
                    finish();
                }//onClick
            });
        }//pone el botón negativo
        pauseDialog.setView(promptView);
        pauseDialog.show();
        return pauseDialog;
    }//createPauseMenu

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if((requestCode == GameActivity.END_GAME_FINISH_STATUS &&
                resultCode == GameActivity.END_GAME_FINISH_STATUS) || data == null) {
            return;
        }//si se solicitó terminar el juego, reacción en cadena hacia atrás
        score = data.getLongExtra(GameActivity.SCORE_ID, 0);
    }//onActivityResult

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode == KeyEvent.KEYCODE_BACK) {
            if (showingClues) {
                pauseAllowed = false;
                hideCluesBriefcase();
                return true;
            }//si está mostrando las pistas
        }//si se toco volver
        return super.onKeyDown(keyCode, event);
    }//onKeyDown

    @Override
    protected void onResume() {
        super.onResume();
        if(!((this instanceof MainMenu) || (this instanceof SettingsActivity))) {
            loadGame();
        }//si no es el menú principal
    }//onResume

    /**
     * Carga los gr&aacute;ficos comunes a todos los escenarios.
     * @since Chaton Game Activity 1.0, februay 2017
     */
    protected void loadCommonGraphics(int level) {
        taps = super.createTaps(R.drawable.tap_screen_f1, R.drawable.tap_screen_f2);
        toTap = (ImageView) findViewById(R.id.taps_holder);
        drag = super.createTaps(R.drawable.move_screen_f1, R.drawable.move_screen_f2);
        toDrag = (ImageView) findViewById(R.id.drag_holder);
        toTap.setImageDrawable(taps);
        toDrag.setImageDrawable(drag);
        cluesPorfolio = (RelativeLayout) findViewById(R.id.clue_holder);
        fadeScreen = (ImageView) findViewById(R.id.fade_screen);
        fadeScreen.setClickable(false);
        typewriterView = (Typewriter) findViewById(R.id.dialog_box);
        typewriterView.setFont("Futura");
        bubbleContainer = (TextBubbleContainer) findViewById(R.id.text_bubble);
        bubbleContainer.autoSetScales();
        absoluteBackground = (RelativeLayout) findViewById(R.id.absolute_background);
        jigsaw = (ImageView) findViewById(R.id.clue_award);
        boolean create = false;
        switch (level) {
            case ChatonGameActivity.LEVEL1 :
                create = (gameProgress &ChatonGameActivity.PROGRESS_LEVEL1_TUTORIAL) == 0;
                break;
            case ChatonGameActivity.LEVEL2 :
                create = (gameProgress &ChatonGameActivity.PROGRESS_LEVEL2_TUTORIAL) == 0;
                break;
            case ChatonGameActivity.LEVEL3 :
                create = (gameProgress &ChatonGameActivity.PROGRESS_LEVEL3_TUTORIAL) == 0;
                break;
            case ChatonGameActivity.LEVEL4 :
                create = (gameProgress &ChatonGameActivity.PROGRESS_LEVEL4_TUTORIAL) == 0;
                break;
            case ChatonGameActivity.LEVEL5 :
                create = (gameProgress &ChatonGameActivity.PROGRESS_LEVEL5_TUTORIAL) == 0;
                break;
            case ChatonGameActivity.LEVEL6 :
                create = (gameProgress &ChatonGameActivity.PROGRESS_LEVEL6_TUTORIAL) == 0;
                break;
        } if(create) {
            tutorialTaps = super.createTaps(R.drawable.touch_screen_f1, R.drawable.touch_screen_f2);
            tutorialTap = (ImageView) findViewById(R.id.tutorial_taps);
            tutorialTap.setImageDrawable(tutorialTaps);
            tutorialTap.setVisibility(View.INVISIBLE);
        }//decide si crear los taps o no
    }//loadCommonGraphics

    /**
     * Carga los sonidos comunes a todos los niveles.
     * @since Chaton Game Activity 1.0, march 2017
     */
    protected void loadCommonSounds() {
        initSoundsHandlersReference();
        normalTimer = new MusicHandler(this);
        normalTimer.load(R.raw.timer1, true);
        hardTimer = new MusicHandler(this);
        hardTimer.load(R.raw.timer2, true);
        soundsHandlers.add(normalTimer);
        soundsHandlers.add(hardTimer);
    }//loadCommonSounds

    /**
     * Crea el di&aacute;logo que muestra un texto sobre la aplicaci&oacute;n.
     * @return AlertDialog - El dialogo de acerca de.
     * @since Chaton Game Activity 1.0, october 2016
     */
    protected AlertDialog createAboutDialog() {
        View promptView = LayoutInflater.from(this).inflate(R.layout.dialog_about, null);
        ((CustomFontTextView) promptView.findViewById(R.id.about_title)).setFont("Futura");
        ((CustomFontTextView) promptView.findViewById(R.id.about_content)).setFont("Futura");
        CustomFontButton button = (CustomFontButton) promptView.findViewById(R.id.back_button);
        button.setFont("GROBOLD.ttf");
        final AlertDialog aboutDialog = new AlertDialog.Builder(this).create();
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View dialog) {
                aboutDialog.dismiss();
            }//onClick
        });
        aboutDialog.setView(promptView);
        aboutDialog.show();
        return aboutDialog;
    }//createAboutDialog

    /**
     * Responde a toques sobre la pantalla desvanecedora.
     * @param fs - La pantalla desvanecedora
     * @since Chaton Game Activity 1.0, march 2017
     */
    public void fadeScreenTouched(View fs) {
        if(showingClues) {
            hideCluesBriefcase();
        }//si está mostrando las pistas
    }//fadeScreenTouched

    /**
     * Devuelve el multiplicador de difricultad adecuado; dependiendo de la
     * dificultad que tenga el juego confifgurada en un momento dado.
     * @return int - El multiplicador de dificultad.
     * @since Level 1 1.1, august 2016
     */
    protected final int getDifficultyMultiplier() {
        return currentDifficulty == GameActivity.EASY ? ChatonGameActivity.EASY_MULTIPLIER :
                currentDifficulty == GameActivity.NORMAL ? ChatonGameActivity.NORMAL_MULTIPLIER :
                        ChatonGameActivity.HARD_MULTIPLIER;
    }//getDifficultyMultiplier

    /**
     * Inicializa un di&aacute;logo. Usa el m&eeacute;todo inidDialog de DragScreen pero hace
     * constantes los tiempos de animaci&oacute;n para toda la aplicaci&oacute;n.
     * @param bubble - Burbuja de texto.
     * @param textV - Referencia a la vista de texto.
     * @param dialogId - El identificador del texto que se quiere mostrar.
     * @param character - El personaje que está hablando.
     * @param bOrientation - La orientaci&oacute;n del globo de texto.
     * @param tOrientation - La orientati&oacute;n del texto.
     * @param nextSec - La siguiente secuencia de texto a mostrar.
     * @param dialogTrackId - Identificador de la narraci&oacute;n del texto.
     * @param activateScroll - Si el scroll debe estar activado o no.
     * @param delay - El retraso para iniciar la animaci&oacute;n.
     * @param animListener - Refceptor de eventos de animaci&oacute;n
     * @return byte - El estado del di&aacute;logo inicializado.
     * @since Chaton Game Activity 1.0, november 2016
     */
    protected byte initDialog(TextBubbleContainer bubble, View textV, int dialogId, View character,
                              boolean bOrientation, boolean tOrientation, byte nextSec,
                              int dialogTrackId, boolean activateScroll, long delay,
                              Animator.AnimatorListener animListener, boolean useTaps) {
        super.stopTaps(drag, toDrag);
        return useTaps ? super.initDialog(bubble, textV, character, dialogId, bOrientation,
                tOrientation, taps, toTap, nextSec, dialogTrackId, activateScroll,
                Typewriter.TEXT_DELAY, Math.round(
                        GameActivity.ANIMATION_DURATION *getFloatResource(R.dimen.text_intro_delay)), delay,
                animListener) : super.initDialog(bubble, textV, character, dialogId, bOrientation,
                tOrientation, null, null, nextSec, dialogTrackId, activateScroll,
                Typewriter.TEXT_DELAY, Math.round(
                        GameActivity.ANIMATION_DURATION *getFloatResource(R.dimen.text_intro_delay)), delay,
                animListener);
    }//initDialog

    /**
     * Inicializa un di&aacute;logo esperando a que finalice el anterior que existiera. Usa el
     *
     * @param bubble - Burbuja de texto.
     * @param textV - Referencia a la vista de texto.
     * @param dialogId - El identificador del texto que se quiere mostrar.
     * @param character - El personaje que está hablando.
     * @param bOrientation - La orientaci&oacute;n del globo de texto.
     * @param tOrientation - La orientati&oacute;n del texto.
     * @param nextSec - La siguiente secuencia de texto a mostrar.
     * @param dialogTrackId - Identificador de la narraci&oacute;n del di&aacute;logo.
     * @param activateScroll - Si el scroll debe estar activado o no.
     * @param delay - El retraso para iniciar la animaci&oacute;n.
     * @param overwrite - Indica si debe sobreescribirse el globo de texto actual o esperar por uno
     * nuevo.
     * @param animListener - Receptor de eventos de animaci&oacute;n.
     * @since Chaton Game Activity 1.0, november 2016
     */
    protected void initDialogAfterAnim(TextBubbleContainer bubble, View textV, int dialogId,
                                       View character, boolean bOrientation, boolean tOrientation,
                                       byte nextSec, int dialogTrackId, boolean activateScroll,
                                       long delay, boolean overwrite,
                                       Animator.AnimatorListener animListener, boolean useTaps) {
        super.stopTaps(drag, toDrag);
        if(useTaps) {
            super.initDialogAfterAnim(bubble, textV, dialogId, character, bOrientation,
                    tOrientation, taps, toTap, nextSec, dialogTrackId, activateScroll,
                    Typewriter.TEXT_DELAY, Math.round(
                            GameActivity.ANIMATION_DURATION *getFloatResource(R.dimen.text_intro_delay)), delay,
                    overwrite, animListener);
        } else {
            super.initDialogAfterAnim(bubble, textV, dialogId, character, bOrientation,
                    tOrientation, null, null, nextSec, dialogTrackId, activateScroll,
                    Typewriter.TEXT_DELAY, Math.round(GameActivity.ANIMATION_DURATION
                            *getFloatResource(R.dimen.text_intro_delay)), delay, overwrite,
                    animListener);
        }//actúa dependiendo de si se requieren los taps o no
    }//initDialogAfterAnim

    /**
     * Cambia el mensaje del puntaje actual.
     * @since Chaton Game Activity 1.0, november 2016
     */
    protected void displayScore() {
        super.displayScore(upperFrame, R.string.score);
    }//displayScore

    /**
     * Asigna la imagen para la vista del margen del fondo.
     * @param drawableId - El identificador de la imagen que quiere colocarse en la vista.
     * @since Chaton Game Activity 1.0, november 2016
     */
    protected void setLowerFrameDrawable(int drawableId) {
        final ImageView lowerFrame = (ImageView) findViewById(R.id.lower_frame);
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            lowerFrame.setImageDrawable(getDrawable(drawableId));
        } else {
            lowerFrame.setImageDrawable(getResources().getDrawable(drawableId));
        }//usa un método disponible
        lowerFrame.getViewTreeObserver().addOnGlobalLayoutListener(
                new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                lowerFrame.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                lowerFrameH = lowerFrame.getMeasuredHeight();
            }//onGlobalLayout
        });
    }//setLowerFrameDrawable

    /**
     * Destruye el hilo que anima a chat&oacute;n.
     * @since Module 2 3.0, november 2016
     */
    protected void killChatonAnimThread() {
        if(chatAnim != null) {
            chatAnim.run = chatAnim.pause = false;
            animatorThreads.remove(ChatonAnimator.class);
            boolean retry = true;
            while(retry) {
                try {
                    chatAnim.join();
                    retry = false;
                } catch (InterruptedException e) {
                    Log.v(GameActivity.LOGGER_ID, "Chaton animator thread interrrupted", e);
                }//trata de detener el hilo animador
            }
            chatAnim = null;
        }//si hay un hilo que destruir
    }//killChatonAnimThread

    /**
     * Muestra las pistas obtenidas.
     * @param v - El portafolio de pistas.
     * @since Chaton Game Activity 1.0, december 2016
     */
    public void showClues(View v) {
        if(!pauseAllowed || animatingClues) {
            return;
        }//si no debe permitir estados de pausa
        pauseAllowed = false;
        pauseRequested = true;
        pauseAnimatorThreads();
        fadeScreen.setVisibility(View.VISIBLE);
        fadeScreen.animate().alpha(0.75f);
        fadeScreen.animate().setDuration(GameActivity.ANIMATION_DURATION);
        float finalY = cluesPorfolio.getY();
        cluesPorfolio.setY(displayMetrics.heightPixels);
        cluesPorfolio.setVisibility(View.VISIBLE);
        cluesPorfolio.animate().setDuration(GameActivity.ANIMATION_DURATION);
        cluesPorfolio.animate().y(finalY);
        cluesPorfolio.animate().setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                animatingClues = false;
            }//onAnimationEnd
        });
        showingClues = true;
        if(soundFx == null) soundFx = new MusicHandler(this);
        soundFx.load(R.raw.clues_portfolio, false);
        soundFx.play();
        cluesPorfolio.animate().start();
        fadeScreen.animate().start();
        fadeScreen.setClickable(true);
        animatingClues = true;
    }//showClues

    /**
     * Oculta el portafolio de pistas.
     * @since Chaton Game Activity 1.0, december 2016
     */
    protected void hideCluesBriefcase() {
        if(animatingClues) return;
        fadeScreen.animate().alpha(0);
        fadeScreen.animate().setDuration(GameActivity.ANIMATION_DURATION);
        final float finalY = cluesPorfolio.getY();
        cluesPorfolio.animate().y(displayMetrics.heightPixels +cluesPorfolio.getMeasuredHeight() /2);
        cluesPorfolio.animate().setDuration(GameActivity.ANIMATION_DURATION);
        cluesPorfolio.animate().setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                cluesPorfolio.setVisibility(View.INVISIBLE);
                cluesPorfolio.setY(finalY);
                pauseAllowed = true;
                animatingClues = false;
                fadeScreen.setClickable(false);
            }//onAnimationEnd
        });
        showingClues = false;
        pauseRequested = false;
        cluesPorfolio.animate().start();
        fadeScreen.animate().start();
        resumeAnimatorThreads();
        animatingClues = true;
    }//hideClues

    /**
     * Anima la pieza de rompecabezas para meterla en el portafolio de pistas.
     * @since Chaton Game Actity 1.0, february 2017
     */
    protected final void clueAnimation() {
        jigsaw.setScaleX(1.5f);
        jigsaw.setScaleY(1.5f);
        jigsaw.setX(0);
        jigsaw.setY(displayMetrics.heightPixels -jigsaw.getMeasuredWidth()); //*/
        jigsaw.setAlpha(1f);
        jigsaw.animate().alpha(0);
        jigsaw.animate().setDuration(GameActivity.ANIMATION_DURATION);
        jigsaw.animate().scaleX(0.5f);
        jigsaw.animate().scaleY(0.5f);
        jigsaw.animate().x(displayMetrics.widthPixels);
        jigsaw.animate().y(cluesPorfolio.getY());
        soundFx.pause();
        soundFx.load(R.raw.clue_get, false);
        soundFx.play();
        jigsaw.setVisibility(View.VISIBLE);
        jigsaw.animate().start();
    }//clueAnimation

    /**
     * Guarda la partida.
     * @param progress - El progreso alcanzado. Este debe ser una combinaci&oacute;n usando el
     * operador bit a bit OR "|" de las constantes PROGRESS_
     * @return boolean - <tt>true</tt> si ha cambiado el progreso del juego. <tt>false</tt> en otro
     * caso.
     * @since Game Activity 1.0, december 2015
     */
    protected final boolean saveGame(long progress) {
        long modProgress = gameProgress |progress;
        if(modProgress == gameProgress) {
            return false;
        }//si no hubo cambios
        gameProgress = modProgress;
        final ChatonDBHelper dbHelper = new ChatonDBHelper(this);
        Thread saver = new Thread("mx.nachintoch.vg$CHATON_SAVER_THREAD") {
            @Override
            public void run() {
                SQLiteDatabase db = dbHelper.getWritableDatabase();
                ContentValues values = new ContentValues();
                values.put(ChatonDBHelper.GAME_PROGRESS, gameProgress);
                values.put(ChatonDBHelper.SCORE, score);
                db.update(ChatonDBHelper.PARTY_TABLE, values, ChatonDBHelper.PARTY_ID +" = "
                        +ChatonGameActivity.ONLY_PARTY_ID, null);
                db.close();
                dbHelper.close();
            }//run
        };
        saver.start();
        drawClues();
        return true;
    }//saveGame

    /**
     * Carga el progreso del jugador.
     * @since Chaton Game Activity 1.0, february 2017
     */
    protected final void loadGame() {
        ChatonDBHelper dbHelper = new ChatonDBHelper(this);
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor c = db.query(ChatonDBHelper.PARTY_TABLE, new String[]{ChatonDBHelper.GAME_PROGRESS,
                ChatonDBHelper.SCORE, ChatonDBHelper.GULTY, ChatonDBHelper.SETTINGS},
                ChatonDBHelper.PARTY_ID +" = " +ChatonGameActivity.ONLY_PARTY_ID, null, null, null,
                null);
        c.moveToNext();
        gameProgress = c.getLong(0);
        score = c.getLong(1);
        gulty = (byte) c.getInt(2);
        settingsSave = c.getInt(3);
        c.close();
        db.close();
        dbHelper.close();
        setSettings();
        drawClues();
        displayScore();
    }//loadGame

    /**
     * Muestra las pistas en el portafolio de pistas.
     * @since Chaton Game Activity 1.0, february 2017
     */
    protected final void drawClues() {
        float alpha = Long.bitCount(gameProgress) /(float) TOTAL_CLUES_NUMBER;
        if(alpha < 0.5) alpha = 0.5f;
        if(alpha > 1) alpha = 1;
        if((gameProgress &ChatonGameActivity.PROGRESS_LEVEL1_1) != 0) {
            drawClue(R.id.clue1_1, alpha);
        } if((gameProgress &ChatonGameActivity.PROGRESS_LEVEL1_2) != 0) {
            drawClue(R.id.clue1_2, alpha);
        } if((gameProgress &ChatonGameActivity.PROGRESS_LEVEL1_3) != 0) {
            drawClue(R.id.clue1_3, alpha);
        } if((gameProgress &ChatonGameActivity.PROGRESS_LEVEL1_4) != 0) {
            drawClue(R.id.clue1_4, alpha);
        } if((gameProgress &ChatonGameActivity.PROGRESS_LEVEL12_1) != 0) {
            drawClue(R.id.clue12_1, alpha);
        } if((gameProgress &ChatonGameActivity.PROGRESS_LEVEL12_2) != 0) {
            drawClue(R.id.clue12_2, alpha);
        } if((gameProgress &ChatonGameActivity.PROGRESS_LEVEL13_1) != 0) {
            drawClue(R.id.clue13_1, alpha);
        } if((gameProgress &ChatonGameActivity.PROGRESS_LEVEL13_2) != 0) {
            drawClue(R.id.clue13_2, alpha);
        } if((gameProgress &ChatonGameActivity.PROGRESS_LEVEL5_1) != 0) {
            drawClue(R.id.clue15_1, alpha);
        } if((gameProgress &ChatonGameActivity.PROGRESS_LEVEL15_2) != 0) {
            drawClue(R.id.clue15_2, alpha);
        } if((gameProgress &ChatonGameActivity.PROGRESS_LEVEL2_1) != 0) {
            drawClue(R.id.clue2_1, alpha);
        } if((gameProgress &ChatonGameActivity.PROGRESS_LEVEL2_2) != 0) {
            drawClue(R.id.clue2_2, alpha);
        } if((gameProgress &ChatonGameActivity.PROGRESS_LEVEL2_3) != 0) {
            drawClue(R.id.clue2_3, alpha);
        } if((gameProgress &ChatonGameActivity.PROGRESS_LEVEL2_4) != 0) {
            drawClue(R.id.clue2_4, alpha);
        } if((gameProgress &ChatonGameActivity.PROGRESS_LEVEL24_1) != 0) {
            drawClue(R.id.clue24_1, alpha);
        } if((gameProgress &ChatonGameActivity.PROGRESS_LEVEL24_2) != 0) {
            drawClue(R.id.clue24_2, alpha);
        } if((gameProgress &ChatonGameActivity.PROGRESS_LEVEL25_1) != 0) {
            drawClue(R.id.clue25_1, alpha);
        } if((gameProgress &ChatonGameActivity.PROGRESS_LEVEL25_2) != 0) {
            drawClue(R.id.clue25_2, alpha);
        } if((gameProgress &ChatonGameActivity.PROGRESS_LEVEL3_1) != 0) {
            drawClue(R.id.clue3_1, alpha);
        } if((gameProgress &ChatonGameActivity.PROGRESS_LEVEL3_2) != 0) {
            drawClue(R.id.clue3_2, alpha);
        } if((gameProgress &ChatonGameActivity.PROGRESS_LEVEL3_3) != 0) {
            drawClue(R.id.clue3_3, alpha);
        } if((gameProgress &ChatonGameActivity.PROGRESS_LEVEL3_4) != 0) {
            drawClue(R.id.clue3_4, alpha);
        } if((gameProgress &ChatonGameActivity.PROGRESS_LEVEL34_1) != 0) {
            drawClue(R.id.clue34_1, alpha);
        } if((gameProgress &ChatonGameActivity.PROGRESS_LEVEL34_2) != 0) {
            drawClue(R.id.clue34_2, alpha);
        } if((gameProgress &ChatonGameActivity.PROGRESS_LEVEL35_1) != 0) {
            drawClue(R.id.clue35_1, alpha);
        } if((gameProgress &ChatonGameActivity.PROGRESS_LEVEL25_2) != 0) {
            drawClue(R.id.clue35_2, alpha);
        } if((gameProgress &ChatonGameActivity.PROGRESS_LEVEL4_1) != 0) {
            drawClue(R.id.clue4_1, alpha);
        } if((gameProgress &ChatonGameActivity.PROGRESS_LEVEL4_2) != 0) {
            drawClue(R.id.clue4_2, alpha);
        } if((gameProgress &ChatonGameActivity.PROGRESS_LEVEL4_3) != 0) {
            drawClue(R.id.clue4_3, alpha);
        } if((gameProgress &ChatonGameActivity.PROGRESS_LEVEL4_4) != 0) {
            drawClue(R.id.clue4_4, alpha);
        } if((gameProgress &ChatonGameActivity.PROGRESS_LEVEL45_1) != 0) {
            drawClue(R.id.clue45_1, alpha);
        } if((gameProgress &ChatonGameActivity.PROGRESS_LEVEL45_2) != 0) {
            drawClue(R.id.clue45_2, alpha);
        } if((gameProgress &ChatonGameActivity.PROGRESS_LEVEL5_1) != 0) {
            drawClue(R.id.clue5_1, alpha);
        } if((gameProgress &ChatonGameActivity.PROGRESS_LEVEL5_2) != 0) {
            drawClue(R.id.clue5_2, alpha);
        } if((gameProgress &ChatonGameActivity.PROGRESS_LEVEL5_3) != 0) {
            drawClue(R.id.clue5_3, alpha);
        } if((gameProgress &ChatonGameActivity.PROGRESS_LEVEL5_4) != 0) {
            drawClue(R.id.clue5_4, alpha);
        }//muestra la case
    }//drawClues

    /**
     * Lee las configuraciones del usuario a partir de su estado crudo en la base de datos y asgina
     * la dificultad por defecto del juego.
     * @since Chaton Game Activity 1.0, 2017
     */
    protected void setSettings() {
        musicOn = (settingsSave &ChatonGameActivity.MUSIC_SETTING) != 0;
        voiceOn = (settingsSave &ChatonGameActivity.FX_SETTING) != 0;
        currentDifficulty = (settingsSave &ChatonGameActivity.DEFAULT_EASY_DIFFICULTY) != 0 ?
                GameActivity.EASY : (settingsSave &ChatonGameActivity.DEFAULT_NORMAL_DIFFICULTY) != 0 ?
                GameActivity.NORMAL : GameActivity.HARD;
    }//setSettings

    /**
     * Dado un identificador de nivel, lo garda apropiadamente; registrando el avance con la
     * obtenci&oacute;n de pistas nuevas.
     * @param levelId - El identificador del nivel.
     * @return boolean - <tt>true</tt> si hay avance en el juego. <tt>false</tt> en otro caso.
     * @since Chaton Game Activity 1.0, 2017
     */
    protected boolean saveNormalProgress(byte levelId) {
        switch (levelId) {
            case LEVEL1 :
                return saveGame(PROGRESS_LEVEL1_1) || saveGame(PROGRESS_LEVEL1_2) ||
                        saveGame(PROGRESS_LEVEL1_3) || saveGame(PROGRESS_LEVEL1_4) ||
                        saveSpecialProgress(LEVEL1);
            case LEVEL2 :
                return saveGame(PROGRESS_LEVEL2_1) || saveGame(PROGRESS_LEVEL2_2) ||
                        saveGame(PROGRESS_LEVEL2_3) || saveGame(PROGRESS_LEVEL2_4) ||
                        saveSpecialProgress(LEVEL2);
            case LEVEL3 :
                return saveGame(PROGRESS_LEVEL3_1) || saveGame(PROGRESS_LEVEL3_2) ||
                        saveGame(PROGRESS_LEVEL3_3) || saveGame(PROGRESS_LEVEL3_4) ||
                        saveSpecialProgress(LEVEL3);
            case LEVEL4 :
                return saveGame(PROGRESS_LEVEL4_1) || saveGame(PROGRESS_LEVEL4_2) ||
                        saveGame(PROGRESS_LEVEL4_3) || saveGame(PROGRESS_LEVEL4_4) ||
                        saveSpecialProgress(LEVEL4);
            case LEVEL5 :
                return saveGame(PROGRESS_LEVEL5_1) || saveGame(PROGRESS_LEVEL5_2) ||
                        saveGame(PROGRESS_LEVEL5_3) || saveGame(PROGRESS_LEVEL5_4) ||
                        saveSpecialProgress(LEVEL5);
        }//acúa de acuerdo al nivel
        Logger.getLogger(GameActivity.LOGGER_ID).log(Level.WARNING, "A non-existing level tryed " +
                "to save the game");
        return false;
    }//saveNormalGame

    /**
     * Intenta guardar progreso del juego a partir de realizar mec&aacute;nicas secundarias.
     * @param levelId - El identificador del nivel.
     * @return boolean - <tt>true</tt> si hay avance en el juego, <tt>false</tt> en otro caso.
     * @since Chaton Game Activity 1.0, 2017
     */
    protected boolean saveSpecialProgress(byte levelId) {
        switch (levelId) {
            case LEVEL1 :
                return saveGame(PROGRESS_LEVEL12_1) || saveGame(PROGRESS_LEVEL12_2) ||
                        saveGame(PROGRESS_LEVEL13_1) || saveGame(PROGRESS_LEVEL13_2) ||
                        saveGame(PROGRESS_LEVEL15_1) || saveGame(PROGRESS_LEVEL15_2);
            case LEVEL2 :
                return saveGame(PROGRESS_LEVEL12_1) || saveGame(PROGRESS_LEVEL12_2) ||
                        saveGame(PROGRESS_LEVEL24_1) || saveGame(PROGRESS_LEVEL24_2) ||
                        saveGame(PROGRESS_LEVEL25_1) || saveGame(PROGRESS_LEVEL25_2);
            case LEVEL3 :
                return saveGame(PROGRESS_LEVEL13_1) || saveGame(PROGRESS_LEVEL13_2) ||
                        saveGame(PROGRESS_LEVEL34_1) || saveGame(PROGRESS_LEVEL34_2) ||
                        saveGame(PROGRESS_LEVEL35_1) || saveGame(PROGRESS_LEVEL35_2);
            case LEVEL4 :
                return saveGame(PROGRESS_LEVEL24_1) || saveGame(PROGRESS_LEVEL24_2) ||
                        saveGame(PROGRESS_LEVEL34_1) || saveGame(PROGRESS_LEVEL34_2) ||
                        saveGame(PROGRESS_LEVEL45_1) || saveGame(PROGRESS_LEVEL45_2);
            case LEVEL5 :
                return saveGame(PROGRESS_LEVEL15_1) || saveGame(PROGRESS_LEVEL15_2) ||
                        saveGame(PROGRESS_LEVEL25_1) || saveGame(PROGRESS_LEVEL25_2) ||
                        saveGame(PROGRESS_LEVEL35_1) || saveGame(PROGRESS_LEVEL35_2) ||
                        saveGame(PROGRESS_LEVEL45_1) || saveGame(PROGRESS_LEVEL45_2);
        }//actúa de acuerdo al nivel
        Logger.getLogger(GameActivity.LOGGER_ID).log(Level.WARNING, "A non-existing level tryed " +
                "to save the game");
        return false;
    }//saveSpecialGame

    /**
     * Devuelve un estado de memoria inicial para las configuraciones de la patida por defecto.
     * @return byte - El estado por defecto de las configuraciones en la base de gatos.
     * @since Main Menu 2.0, february 2017
     */
    public static int getDefaultSave() {
        return ChatonGameActivity.DEFAULT_EASY_DIFFICULTY |ChatonGameActivity.MUSIC_SETTING
                |ChatonGameActivity.FX_SETTING;
    }//getDefaultSave

    /**
     * Dibuja la pista dada.
     * @param clue - La pista a dibujar.
     * @since Chaton Game Activity 1.0, march 2017
     */
    private void drawClue(int clue, float alpha) {
        ImageView v = (ImageView) findViewById(clue);
        v.setAlpha(alpha);
        v.setVisibility(View.VISIBLE);
        switch (gulty) {
            case ChatonGameActivity.JL_GULTY :
                v.setImageResource(getResForJLClueNo(clue));
                break;
            case ChatonGameActivity.JUAN_GULTY :
                v.setImageResource(getResForJuanClueNo(clue));
                break;
            case ChatonGameActivity.PACO_GULTY :
                v.setImageResource(getResForPacoClueNo(clue));
        }//agrega la imagen adecuada
    }//drawVlue

    /**
     * Devuelve el identificador de recurso adecuado para la pista dada que completa el rompecabeza
     * de Jos&eacute; Luis.
     * @param clue - El n&uacute;mero de pista desado.
     * @return int - El identificador de la imagen apropiada de acuerdo a la pista deseada.
     * @since Chaton Game Activity 1.0, march 2017
     */
    private int getResForJLClueNo(int clue) {
        switch (clue) {
            case R.id.clue1_1 :
                return R.drawable.jl_gulty_1_1;
            case R.id.clue1_2 :
                return R.drawable.jl_gulty_1_2;
            case R.id.clue1_3 :
                return R.drawable.jl_gulty_1_3;
            case R.id.clue1_4 :
                return R.drawable.jl_gulty_1_4;
            case R.id.clue2_1 :
                return R.drawable.jl_gulty_2_1;
            case R.id.clue2_2 :
                return R.drawable.jl_gulty_2_2;
            case R.id.clue2_3 :
                return R.drawable.jl_gulty_2_3;
            case R.id.clue2_4 :
                return R.drawable.jl_gulty_2_4;
            case R.id.clue3_1 :
                return R.drawable.jl_gulty_3_1;
            case R.id.clue3_2 :
                return R.drawable.jl_gulty_3_2;
            case R.id.clue3_3 :
                return R.drawable.jl_gulty_3_3;
            case R.id.clue3_4 :
                return R.drawable.jl_gulty_3_4;
            case R.id.clue4_1 :
                return R.drawable.jl_gulty_4_1;
            case R.id.clue4_2 :
                return R.drawable.jl_gulty_4_2;
            case R.id.clue4_3 :
                return R.drawable.jl_gulty_4_3;
            case R.id.clue4_4 :
                return R.drawable.jl_gulty_4_4;
            case R.id.clue5_1 :
                return R.drawable.jl_gulty_5_1;
            case R.id.clue5_2 :
                return R.drawable.jl_gulty_5_2;
            case R.id.clue5_3 :
                return R.drawable.jl_gulty_5_3;
            case R.id.clue5_4 :
                return R.drawable.jl_gulty_5_4;
            case R.id.clue12_1 :
                return R.drawable.jl_gulty_12_1;
            case R.id.clue12_2 :
                return R.drawable.jl_gulty_12_2;
            case R.id.clue13_1 :
                return R.drawable.jl_gulty_13_1;
            case R.id.clue13_2 :
                return R.drawable.jl_gulty_13_2;
            case R.id.clue15_1 :
                return R.drawable.jl_gulty_15_1;
            case R.id.clue15_2 :
                return R.drawable.jl_gulty_15_2;
            case R.id.clue24_1 :
                return R.drawable.jl_gulty_24_1;
            case R.id.clue24_2 :
                return R.drawable.jl_gulty_24_2;
            case R.id.clue25_1 :
                return R.drawable.jl_gulty_25_1;
            case R.id.clue25_2 :
                return R.drawable.jl_gulty_25_2;
            case R.id.clue34_1 :
                return R.drawable.jl_gulty_34_1;
            case R.id.clue34_2 :
                return R.drawable.jl_gulty_34_2;
            case R.id.clue35_1 :
                return R.drawable.jl_gulty_35_1;
            case R.id.clue35_2 :
                return R.drawable.jl_gulty_35_2;
            case R.id.clue45_1 :
                return R.drawable.jl_gulty_45_1;
            case R.id.clue45_2 :
                return R.drawable.jl_gulty_45_1;
        }//de acuerdo al número de pista deseado
        return -1;
    }//getResForJLClueNo

    /**
     * Devuelve el identificador de recurso adecuado para la pista dada que completa el rompecabeza
     * de Juan.
     * @param clue - El n&uacute;mero de pista desado.
     * @return int - El identificador de la imagen apropiada de acuerdo a la pista deseada.
     * @since Chaton Game Activity 1.0, march 2017
     */
    private int getResForJuanClueNo(int clue) {
        switch (clue) {
            case R.id.clue1_1 :
                return R.drawable.juan_gulty_1_1;
            case R.id.clue1_2 :
                return R.drawable.juan_gulty_1_2;
            case R.id.clue1_3 :
                return R.drawable.juan_gulty_1_3;
            case R.id.clue1_4 :
                return R.drawable.juan_gulty_1_4;
            case R.id.clue2_1 :
                return R.drawable.juan_gulty_2_1;
            case R.id.clue2_2 :
                return R.drawable.juan_gulty_2_2;
            case R.id.clue2_3 :
                return R.drawable.juan_gulty_2_3;
            case R.id.clue2_4 :
                return R.drawable.juan_gulty_2_4;
            case R.id.clue3_1 :
                return R.drawable.juan_gulty_3_1;
            case R.id.clue3_2 :
                return R.drawable.juan_gulty_3_2;
            case R.id.clue3_3 :
                return R.drawable.juan_gulty_3_3;
            case R.id.clue3_4 :
                return R.drawable.juan_gulty_3_4;
            case R.id.clue4_1 :
                return R.drawable.juan_gulty_4_1;
            case R.id.clue4_2 :
                return R.drawable.juan_gulty_4_2;
            case R.id.clue4_3 :
                return R.drawable.juan_gulty_4_3;
            case R.id.clue4_4 :
                return R.drawable.juan_gulty_4_4;
            case R.id.clue5_1 :
                return R.drawable.juan_gulty_5_1;
            case R.id.clue5_2 :
                return R.drawable.juan_gulty_5_2;
            case R.id.clue5_3 :
                return R.drawable.juan_gulty_5_3;
            case R.id.clue5_4 :
                return R.drawable.juan_gulty_5_4;
            case R.id.clue12_1 :
                return R.drawable.juan_gulty_12_1;
            case R.id.clue12_2 :
                return R.drawable.juan_gulty_12_2;
            case R.id.clue13_1 :
                return R.drawable.juan_gulty_13_1;
            case R.id.clue13_2 :
                return R.drawable.juan_gulty_13_2;
            case R.id.clue15_1 :
                return R.drawable.juan_gulty_15_1;
            case R.id.clue15_2 :
                return R.drawable.juan_gulty_15_2;
            case R.id.clue24_1 :
                return R.drawable.juan_gulty_24_1;
            case R.id.clue24_2 :
                return R.drawable.juan_gulty_24_2;
            case R.id.clue25_1 :
                return R.drawable.juan_gulty_25_1;
            case R.id.clue25_2 :
                return R.drawable.juan_gulty_25_2;
            case R.id.clue34_1 :
                return R.drawable.juan_gulty_34_1;
            case R.id.clue34_2 :
                return R.drawable.juan_gulty_34_2;
            case R.id.clue35_1 :
                return R.drawable.juan_gulty_35_1;
            case R.id.clue35_2 :
                return R.drawable.juan_gulty_35_2;
            case R.id.clue45_1 :
                return R.drawable.juan_gulty_45_1;
            case R.id.clue45_2 :
                return R.drawable.juan_gulty_45_1;
        }//de acuerdo al número de pista deseado
        return -1;
    }//getResForJuanClueNo

    /**
     * Devuelve el identificador de recurso adecuado para la pista dada que completa el rompecabeza
     * de Paco.
     * @param clue - El n&uacute;mero de pista desado.
     * @return int - El identificador de la imagen apropiada de acuerdo a la pista deseada.
     * @since Chaton Game Activity 1.0, march 2017
     */
    private int getResForPacoClueNo(int clue) {
        switch (clue) {
            case R.id.clue1_1 :
                return R.drawable.paco_gulty_1_1;
            case R.id.clue1_2 :
                return R.drawable.paco_gulty_1_2;
            case R.id.clue1_3 :
                return R.drawable.paco_gulty_1_3;
            case R.id.clue1_4 :
                return R.drawable.paco_gulty_1_4;
            case R.id.clue2_1 :
                return R.drawable.paco_gulty_2_1;
            case R.id.clue2_2 :
                return R.drawable.paco_gulty_2_2;
            case R.id.clue2_3 :
                return R.drawable.paco_gulty_2_3;
            case R.id.clue2_4 :
                return R.drawable.paco_gulty_2_4;
            case R.id.clue3_1 :
                return R.drawable.paco_gulty_3_1;
            case R.id.clue3_2 :
                return R.drawable.paco_gulty_3_2;
            case R.id.clue3_3 :
                return R.drawable.paco_gulty_3_3;
            case R.id.clue3_4 :
                return R.drawable.paco_gulty_3_4;
            case R.id.clue4_1 :
                return R.drawable.paco_gulty_4_1;
            case R.id.clue4_2 :
                return R.drawable.paco_gulty_4_2;
            case R.id.clue4_3 :
                return R.drawable.paco_gulty_4_3;
            case R.id.clue4_4 :
                return R.drawable.paco_gulty_4_4;
            case R.id.clue5_1 :
                return R.drawable.paco_gulty_5_1;
            case R.id.clue5_2 :
                return R.drawable.paco_gulty_5_2;
            case R.id.clue5_3 :
                return R.drawable.paco_gulty_5_3;
            case R.id.clue5_4 :
                return R.drawable.paco_gulty_5_4;
            case R.id.clue12_1 :
                return R.drawable.paco_gulty_12_1;
            case R.id.clue12_2 :
                return R.drawable.paco_gulty_12_2;
            case R.id.clue13_1 :
                return R.drawable.paco_gulty_13_1;
            case R.id.clue13_2 :
                return R.drawable.paco_gulty_13_2;
            case R.id.clue15_1 :
                return R.drawable.paco_gulty_15_1;
            case R.id.clue15_2 :
                return R.drawable.paco_gulty_15_2;
            case R.id.clue24_1 :
                return R.drawable.paco_gulty_24_1;
            case R.id.clue24_2 :
                return R.drawable.paco_gulty_24_2;
            case R.id.clue25_1 :
                return R.drawable.paco_gulty_25_1;
            case R.id.clue25_2 :
                return R.drawable.paco_gulty_25_2;
            case R.id.clue34_1 :
                return R.drawable.paco_gulty_34_1;
            case R.id.clue34_2 :
                return R.drawable.paco_gulty_34_2;
            case R.id.clue35_1 :
                return R.drawable.paco_gulty_35_1;
            case R.id.clue35_2 :
                return R.drawable.paco_gulty_35_2;
            case R.id.clue45_1 :
                return R.drawable.paco_gulty_45_1;
            case R.id.clue45_2 :
                return R.drawable.paco_gulty_45_1;
        }//de acuerdo al número de pista deseado
        return -1;
    }//getResForPacoClueNo

    /**
     * Responde a toques sobre el personaje.
     * @param v - Referencia a la vista del personaje.
     * @since Chaton Game Activity 1.0, march 2017
     */
    public void characterClicked(View v) {
        jumpText();
    }//characterClicked

    // clases anidadas

    /**
     * Hilo que anima a Chat&oacute;n. Cuando cambia de cuadro debe mostrarse nervioso.
     * @author <a href="mailto:manuel_castillo_cc@ciencias.unam.mx" >Manuel "Nachintoch"
     * Castillo</a>
     * @version 1.0, november 2016
     * @since Chaton Game Activity 1.0, november 2016
     */
    class ChatonAnimator extends AnimatorThread {

        /**
         * El estado actual del animador.
         * @since Chaton Animator, 1.0, november 2016
         */
        byte state;

        /**
         * El posteador de los eventos.
         * @since Chaton Animator, 1.0, november 2016
         */
        Poster poster;

        /**
         * Recuerda la posici&oacute;n original de Chat&oacute;n para devolverlo a su
         * posici&oacute;n original.
         * @since Chaton Animator, 1.0, november 2016
         */
        final float ORIGINAL_X, ORIGINAL_Y;

        /**
         * Identificadores de los estados del animador.
         * @since Chaton Animator, 1.0, november 2016
         */
        static final byte JUMPING_LOW = 0;
        static final byte JUMPING_HIGH = 1;
        static final byte FALLING_LOW = 2;
        static final byte FALLING_HIGH = 3;
        static final byte RUNNING_LEFT = 4;
        static final byte RUNNING_RIGHT = 5;

        /**
         * Tiempo que toma hacer la animaci&oacute;n de Chat&oacute;n.
         * @since Chaton Animator, 1.0, november 2016
         */
        static final long CHATON_ANIM_DELAY = 400;

        /**
         * Construye un hilo animador de Chat&oacute;n.
         * @param charX - La posici&oacute;n de chat&oacute;n en X.
         * @param charY - La posici&oacute;n de chat&oacute;n en Y.
         * @since Chaton Game Activity 1.0, november 2016
         */
        ChatonAnimator(float charX, float charY) {
            super("mx.nachintoch.vg.chaton.Chaton_Animator_Thread");
            ORIGINAL_X = charX;
            ORIGINAL_Y = charY;
            poster = new Poster();
            if(handler == null) handler = new Handler();
        }//construye un animador de chatón

        @Override
        public void run() {
            while(run) {
                if(pause) {
                    pause();
                    continue;
                }//pausa el animador
                handler.post(poster);
                try {
                    Thread.sleep(ChatonAnimator.CHATON_ANIM_DELAY);
                } catch(InterruptedException e) {
                    Log.i(GameActivity.LOGGER_ID, "Chaton animator thread interrupted", e);
                }//whoops
            }//mientras deba ejecutarse
            handler.post(poster);
        }//run

        /**
         * Posteador de los eventos del hilo gr&aacute;fico.
         * @since Chaton Game Activity, 1.0, november 2016
         */
        class Poster implements Runnable {

            @Override
            public void run() {
                character.animate().setListener(null);
                if(character.getCurrentFrameId() == R.drawable.chaton_f2_454x571) {
                    character.setX(ORIGINAL_X);
                    character.animate().setDuration(ChatonAnimator.CHATON_ANIM_DELAY -30);
                    if(character.getScaleX() < 0) {
                        character.setScaleX(-character.getScaleX());
                    }//si el personaje ha sido volteado
                    switch(state) {
                        case JUMPING_HIGH :
                            character.animate().y(ORIGINAL_Y -75);
                            state = ChatonAnimator.FALLING_HIGH;
                            break;
                        case FALLING_LOW :
                            character.animate().y(ORIGINAL_Y);
                            state = ChatonAnimator.JUMPING_HIGH;
                            break;
                        case FALLING_HIGH :
                            character.animate().y(ORIGINAL_Y);
                            state = ChatonAnimator.JUMPING_LOW;
                        default:
                        case JUMPING_LOW :
                            character.animate().y(ORIGINAL_Y -40);
                            state = ChatonAnimator.FALLING_LOW;
                            break;
                    }//hace saltar a chatón
                    character.animate().start();
                } else if(character.getCurrentFrameId() == R.drawable.chaton_f3_454x571) {
                    character.setY(ORIGINAL_Y);
                    character.animate().setDuration(ChatonAnimator.CHATON_ANIM_DELAY -30);
                    switch(state) {
                        default:
                        case ChatonAnimator.RUNNING_RIGHT :
                            if(character.getScaleX() < 0) {
                                character.setScaleX(-character.getScaleX());
                            }//si el personaje ha sido volteado
                            character.animate().x(ORIGINAL_X +75);
                            state = ChatonAnimator.RUNNING_LEFT;
                            break;
                        case ChatonAnimator.RUNNING_LEFT :
                            if(character.getScaleX() >= 0) {
                                character.setScaleX(-character.getScaleX());
                            }//si el personaje no ha sido volteado
                            character.animate().x(ORIGINAL_X -75);
                            state = ChatonAnimator.RUNNING_RIGHT;
                    }//hace correr a chatón
                    character.animate().start();
                } else {
                    character.setX(ORIGINAL_X);
                    character.setY(ORIGINAL_Y);
                    if(character.getScaleX() < 0) {
                        character.setScaleX(-character.getScaleX());
                    }//si el personaje ha sido volteado
                }//anima a chatón de la forma adecuada
                if(!run) {
                    character.animate().cancel();
                    character.setX(ORIGINAL_X);
                    character.setY(ORIGINAL_Y);
                    if(character.getScaleX() < 0) {
                        character.setScaleX(-character.getScaleX());
                    }//si el personaje ha sido volteado
                }//si debe terminar
            }//run

        }//Poster Runnable

    }//Chaton Animator

    /**
     * Maneja la base de datos de la aplicaci&oacute;n.
     * @author <a href="mailto:manuel_castillo_cc@ciencias.unam.mx" >Manuel "Nachintoch"
     * Castillo</a>
     * @version 1.0, february 2017
     * @since Chaton Game Activity 1.0, february 2017
     */
    class ChatonDBHelper extends SQLiteOpenHelper {

        // atributos de clase

        /**
         * Nombres de las columnas
         * @since Chaton DB Helper 1.0, february 2017
         */
        static final String PARTY_ID = "partyId";
        static final String GAME_PROGRESS = "gameProgress";
        static final String SETTINGS = "settings";
        static final String SCORE = "score";
        static final String GULTY = "gulty";

        /**
         * Nombre de la tabla que contiene el progreso, configuraciones y puntaje del jugador.
         * @since Chaton DB Heper 1.0, february 2017
         */
        static final String PARTY_TABLE = "PARTY";

        /**
         * Versi&oacute;n actual de la base de datos utilizada para guardar partidas.
         * @since Game Files DB Helper 1.0, december 2015
         */
        static final int DATABASE_VERSION = 1;

        /**
         * Nombre de la base de datos en memoria interna del dispositivo (en disco pues).
         * @since Game Files DB Helper 1.0, december 2015
         */
        static final String DATABASE_NAME = "mx.nachintoch.vg.chaton.GAME_DATA";

        // métodos constructores

        /**
         * Construye un manejador de la base de datos.
         * @param context - El contexto desde el que se est&aacute; accediendo a la base de datos.
         * @since Chaton DB Helper 1.0, february 2017
         */
        ChatonDBHelper(Context context) {
            super(context, ChatonDBHelper.DATABASE_NAME, null, ChatonDBHelper.DATABASE_VERSION);
        }//constructor con contexto

        // métodos de implementacaipon

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL("CREATE TABLE IF NOT EXISTS " +PARTY_TABLE +"(" +PARTY_ID +" INTEGER "
                    +"PRIMARY KEY, " + GAME_PROGRESS +" INTEGER, " +SETTINGS +" TINYINT, "
                    +ChatonDBHelper.SCORE +" INTEGER, " +GULTY +" INTEGER)");
            gameProgress = GameActivity.PROGRESS_NONE;
            db.execSQL("INSERT INTO " + ChatonGameActivity.ChatonDBHelper.PARTY_TABLE + "("
                    +ChatonGameActivity.ChatonDBHelper.PARTY_ID + ","
                    +ChatonGameActivity.ChatonDBHelper.GAME_PROGRESS + ","
                    +ChatonDBHelper.SETTINGS + "," +ChatonDBHelper.SCORE +"," +ChatonDBHelper.GULTY
                    +") VALUES("
                    +ChatonGameActivity.ONLY_PARTY_ID +"," +gameProgress + ","
                    +ChatonGameActivity.getDefaultSave() + ",0," +ChatonGameActivity.UNDEFINED_GULTY
                    +")");
        }//onCreate

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            // fixme para la versión 2 de esta clase, hacer respaldo del contenido de la base de
            // datos vieja y no nadamás tirar todo a la basura
            db.execSQL("DROP TABLE IF EXISTS " +PARTY_TABLE);
            onCreate(db);
        }//onUpgrade

        @Override
        public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            // fixme para la versión 2 de esta clase, hacer respaldo del contenido de la base de
            // datos vieja y no nadamás tirar todo a la basura
            db.execSQL("DROP TABLE IF EXISTS " +PARTY_TABLE);
            onCreate(db);
        }//onDowngrade

    }//Chaton Data Base Helper

}//Chaton Game Activity
