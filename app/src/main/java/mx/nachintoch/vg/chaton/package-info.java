/**
 * <p><b>MISTERIOS EN PREGUI</b><br/>
 * Chaton Detective<br/></p>
 * <p><b>Acerca de este paquete</b><br/>
 * Este paquete contiene las clases de Java que en conjunto, consituyen la aplicaci&oacute;n de
 * caracter videojuego "Misterios en Pregui"<br/>
 * La clase fundamental es <i>GameActivity</i>. Una actividad, es una pantalla de Android. La
 * Game Activity, define el comportamiento espec&iacute;fico de una pantalla de android
 * espec&iacute;fico para el juego, tal como responder a toques en pantalla cuando se muestra una
 * escena, desplegar texto poco a poco, cambiar el personaje que habla, transicionar entre
 * pantallas, etc, etc.<br/>
 * <i>Draw View</i> define una Vista (un elemento en la pantalla del Androide 16) que responde a
 * toques de tipo "movimiento" y "soltar la pantalla", para dibujar una serie de l&iacute;neas, de
 * manera que le permite al usuario (al jugador pues) realizar trazos a mano alzada. Esto se usa
 * en el lienzo que se muestra en el m&oacute;dulo (nivel) 1. Tiene la capacidad de ajustar el
 * grosor de la linea dibujada y el color de la pintura (aunque esto lo recibe por parte de la
 * actividad anfitriona y Module1 no lo implementa).<br/>
 * <i>Music Handler</i> es una clase que optimiza los <i>Media Player</i> nativos de Android para
 * reproducir sonidos y m&uacute;sica de fondo del juego. Simplifica el iniciar una canci&oacute;n,
 * indicar loops y el manejo de recursos, lo que es de gran ayuda para el programador no
 * familiarizado con Android.<br/>
 * <i>Session Manager</i> es una interfaz que define los servicios que debe implementar una
 * actividad que muestre al usuario en pantalla opciones de manejo de partida, que en particular es
 * <i>Main Menu</i>. Permite cosas como crear una partida, borrarla y copiar una partida. Las
 * partidas del jugador (tres por instalaci&oacute;n), se mantienen en una tradicional base de datos
 * SQLite nativa de Android. La base de gatos se compone por una &uacute;nica tabla que almacena la
 * siguiente informaci&oacute;n: Nombre de jugador, progreso e identificador de partida. El manejo
 * de la base de datos, se realiza a trav&eacute;z de la Game Activity, que a su vez se apoya de
 * una clase anidada espec&iacute;fica en el manejo de la base de gatos de partidas: <i>Game Files
 * DB Helper</i><br/>
 * <i>Shake Detector</i>, es un receptor de eventos de sensor tipo aceler&oacute;metro. Su
 * &uacute;nica funci&oacute;n es detectar cuando el dispositivo sea agitado. Esto se usa en uno de
 * los huevos de pascua del nivel 1. Si el jugador agita el dispositivo mientras se le permite
 * dibujar sobre el lienzo en pantalla, los personajes har&aacute;n de cuenta que ha ocurrido un
 * terremoto y se borrar&aacute; el lienzo, dej&aacute;ndolo en blanco.<br/>
 * <i>Typewriter</i>, como Draw View, tambi&eacute;n es una vista, pero esta es m&aacute;s
 * particular al ser una vista de texto. Su funci&oacute;n, es mostrar un di&aacute;logo completo
 * palabra por palabra respecto al tiempo.<br/>
 * <i>HistoryActivity</i>, <i>MainMenu</i>, <i>Map</i>, <i>Module1</i> y <i>Module2</i>, son
 * actividades que definen escenas que son parte del juego.</p>
 * <p><b>Acerca de la aplicaci&oacute;n</b><br/>
 * Como aplicaci&oacute;n, Misterios en Pregui fue dise&ntilde;ada para una alta escalabilidad.
 * Puesto que tuvimos problemas con las plataformas para videojuego (particularmente Unity3D, que
 * <i>se supone</i> soporta Android); nos quedamos con muy poco tiempo para implementar el juego.<br/>
 * La propuesta de reconstruir todo desde cero fue bien recibida, pero solo uno de los dos
 * desarrolladores ten&iacute;a experiencia desarrollando Android, por lo que la <tt>Game
 * Activity</tt> fue dise&ntilde;ada el s&oacute;lido prop&oacute;sito de abstraer lo m&aacute;s que
 * se pudiera el funcionamiento interno de la aplicaci&oacute;n, y as&iacute; tratar de evitar
 * entender los conceptos m&aacute;s t&eacute;cnicos de la plataforma para construir el juego.<br/>
 * El resultado fue un <tt>Game Engine</tt> bastante s&oacute;lido, abstracto y general, sobre el
 * cual se puede construir cualquier otro juego (o al menos prototipo) en apensas unas semanas. Este
 * Game Engine, puede ser la base de una nueva plataforma para el desarrollo de juegos, integrando
 * m&aacute;s elementos para m&aacute;s versatilidad y un editor gr&aacute;fico que abstraiga algunos
 * conceptos ambi&eacute;n muy t&eacute;cnicos de las Actividades de Android.<br/>
 * Otro elemento que consideramos un &eacute;xito y de importancia fue el manejo de partidas. Al
 * concebir la idea del juego, no hab&iacute;a seriaci&oacute;n entre los m&oacute;dulo de aprendizaje
 * que nos proporcion&oacute; la gente de Iztacala, decidimos respetar esto para los niveles y
 * desarrollamos el concepto del juego con una idea similar al original a <i>The Legend Of Zelda</i>
 * (Shigeru Miyamoto, Nintendo EAD, 1987), en donde el jugador puede acceder a cualquier nivel en
 * pr&aacute;cticamente cualquier orden. Para ello, el guardado de partidas se representa
 * s&oacute;lo como un entero. Los bits del entero, representan a qu&eacute; niveles se ha accedido
 * y con qu&eacute; nivel de dificultad, sin afectar otros niveles.<br/>
 * La aplicaci&oacute;n ha sido satisfactoriamente probada en los siguientes dispositivos:<br/><ul>
 * <li>Sony ST26A (Xperia J)</li><li>Samsung SM-A500M (Galaxy A5)</li><li>Dell Inspiron 6400 (si es
 * una laptop dise&ntilde;ada para Windows XP, pero el proyecto Android X86 nos hizo el paro)</li>
 * <li>Lenovo Tab A10</li><li>LG 80
 * </li></ul></p>
 * <p><b>Acerca del juego</b><br/>
 * Muchos juegos, a lo largo de nuestra gameriezca vida, han inspirado Misterios en Pregui. Como
 * ya mencionamos, est&aacute; Zelda, Mario Paint (a leguas), el boot es muy Nintendo, mientras que
 * el <i>look & feel</i> es de un <i>point & click adventure</i>. Y no se porqu&eacute;, no dejo de
 * pensar que tiene algo de la saga Mother en la historia.<br/>
 * Estamos perfectamente conscientes de que esto es s&oacute;lo un prototipo muy primitivo y que de
 * existir una versi&oacute;n lanzada al p&uacute;blico puede llegar a verse completamente
 * diferente. Pero parece que el concepto del juego tiene lo suyo, algo de pegue y ha sido; en lo
 * que cabe, bien recibida; y esta escencia, podr&iacute;a permanecer en dado caso.</p>
 * <p><b>Notas de los autores</b><br/>
 * Quisiera dejar en esta descripci&oacute;n del paquete unas notas. Me atrevo a decir a nombre de
 * mi compa&ntilde;ero Ricardo, que Misterios en Pregui tuvo mucha m&aacute;gia en su desarrollo.
 * Ahora que entregamos esta versi&oacute;n del juego, nos quedamos con una sensaci&oacute;n
 * extra&ntilde;a de que vamos a extra&ntilde;ar a los personajes; de alguna manera nos hemos
 * encari&ntilde;ado con el mundo fant&aacute;stico y sus habitantes que es Pregui.<br/>
 * Es como cuando terminas un juego que te ha gustado mucho y despu&eacute;s ya no sabes que hacer
 * con tu vida :P<br/>
 * &iquest;Qu&eacute; qu&eacute; significa Pregui? No tenemos la m&aacute;s remota idea &gt;_&lt;,
 * fue una de esas muchas cosas que al desarrollar conceptualmente el juego se nos vinieron a la
 * mente y fue un <i>shock</i> en el que de pronto deciamos <i>&iexcl;S&iacute;, s&iacute; genial!
 * &iexcl;hay que usar eso!</i><br/>
 * Misterios en Pregui: Chaton detective, ha sido una experiencia inolvidable. Ha significado mucho
 * para m&iacute;. El curso fue genial, mucho m&aacute;s de lo que esperaba y siento que tiene el
 * enfoque adecuado, al menos, para aquellos que de verdad queremos dedicarnos al desarrollo de
 * videojuegos.<br/>
 * No se que ha sido, pero aunque hubo muchos momentos de estr&eacute;s, error y frustraci&oacute;n
 * en el juego, ha sido tan divertido como jugar. Despu&eacute;s de esta experiencia, llegar todos
 * los d&iacute;s a la oficina de 9 a 6 y sentarme a escribir filtros, sockets e interfaces para
 * comunicaci&oacute;n por red (sin mencionar la lectura de art&iacute;culos y pantentes... UGH!
 * patentes...), me siento vac&iacute;o. A mi vida le falta m&aacute;s esa emoci&oacute;n de <i>
 * "&iquest;Y al jugador realmente le gustar&aacute; esto? &iquest;Qu&eacute; hago si esto no es
 * divertido?"</i><br/>
 * Ya para no tomar m&aacute;s del tiempo de quien se haya tomado la molestia de leer este choro,
 * solo quiero expresar que sent&iacute; una felicidad inmensa en la prueba cuando mi hermanita,
 * que no sab&iacute;a ni siquiera que estaba haciedo un juego, me dijera <i>"Si, ya acabe los dos
 * niveles"</i>, y yo le pregunte <i>"&iquest;Y porque no intentas jugarlos de nuevo? se pone
 * m&aacute;s dif&iacute;cil"</i>, <i>&iexcl;En serio se pone m&aacute;s dificil? &iexcl;Guau!</i>
 * y esa emoci&oacute;n al descubrir c&oacute;mo picarle a la pantalla en distintos lados provoca
 * que el juego reaccione con ciertos huevos de pascua... Fue genial, pocas veces he sentido esa
 * emoci&oacute;n y alegria... Por si tenian el pendiente :P<br/>
 * Esta experiencia ha sido maravillosa, claramente los videojuegos son acerca de transmitir una
 * experiencia :P . Siento que he aprendido mucho y al final de todo esto, me queda MUCHO m&aacute;s
 * clara la cita de Satoru Iwata con la que quiero terminar:<br/>
 * <i>Videogames are meant to be fun. Fun for everyone.</i><br/>
 * Nachintoch. Y gracias por jugar!</p><p>
 * ----------------------------------------------------------------------------------------<br/>
 * Quiero agradecer a Nacho el compartime toda la experiencia que tiene programando. Sin su ayuda 
 * el juego simplemente no hubiera quedado como qued&oacute; <br/>
 * Creo que Android Studio no es precisamente 
 * una IDE para principiantes. Creo que integra bastantes herramientas que facilitan -de alguna manera- 
 * el desarrollo de aplicaciones. Muchas veces me sent&iacute; extraviado, parado enfrente de un sinfin de 
 * clases y funciones cuyo origen e implementaci&oacute;n me eran completamente desconocidas. Aunado a 
 * eso, el servicio de Bit Bucket que empleamos para compartir archivos del proyecto hac&iacute;
 * de repente cosas que a mi me parec&iacute;an raras y eso se agregaba mas a la confusi&oacute; 
 * y frustraci&oacute;n de no tener claro que estaba ocurriendo. <br/>
 * Ahora que lo pienso, creo que un poco de las dificultades 
 * a las que me enfrent&eacute; trantando de absorber el mar de informaci&oacute;n que suponen Android 
 * y Bit Bucket las reflej&eacute; en el personaje de mi m&oacute;dulo, quien cuando inicia tiene los 
 * ojos cerrados pero cuando el -breve- m&oacute;dulo arranca presenta los ojos abiertos. <br/>
 * Y a pesar de que pas&eacute; muchas dificultades escribiendo el c&oacute;digo, la voz de los 
 * personajes del juego me llamaba y me dec&iacute;a que intentase una vez y otra vez el darles vida.
 * El juego desde el inicio presentaba grandes retos a resolver (Se trataba de un juego educativo, 
 * los cuales casi siempre resultan aburridos; Hab&iacute;a que gamificar un concepto, que tambi&eacute;n muchas veces
 * resulta ser una receta para el desastre) algo que me corrobor&oacute; que hab&iacute;a funcionado fue 
 * el atestiguar como uno de los Beta-Testers (Alma) una vez que termin&oacute; la prueba decidi&oacute; 
 * retomar el juego. En ese momento entend&iacute; que hab&iacute;a por lo menos una persona interesada en el 
 * juego mas all&aacute;de la prueba y me dije a mi mismo: "Funcion&oacute;. Est&aacute; lejos de ser perfecto, 
 * pero funcion&oacute;"
 * Ricardo.
 * </p>
 * @author <a href=mailto:caffeine305@gmail.com >Ricardo "Caffeine" L&aacute;zaro</a>
 * @author <a href="http://www.nachintoch.mx" >Manuel "Nachintoch" Castillo</a>
 * @version 0.5, december 2015
 */
package mx.nachintoch.vg.chaton;
