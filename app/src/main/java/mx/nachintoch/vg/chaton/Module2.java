package mx.nachintoch.vg.chaton;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import java.util.Random;
import java.util.TimerTask;

import mx.nachintoch.vg.android.CustomFontTextView;
import mx.nachintoch.vg.android.GameActivity;
import mx.nachintoch.vg.android.MusicHandler;
import mx.nachintoch.vg.android.SpriteView;

/**
 * Implementa el nivel 2: la estaci&oacute;n de policia.
 * @author Ricardo L&aacute;zaro
 * @author <a href="mailto:manuel_castillo_cc@ciencias.unam.mx" >Manuel "Nachintoch" Castillo</a>
 * @author Jonathan Guerrero
 * @version 3.0, november 2016
 * @since Misterios en Pregui 0.5, november 2015
 */
public class Module2 extends ChatonGameActivity {

    /**
     * Referencias a las tarjetas del juego.
     * @since Moddule 2 3.0, november 2016
     */
    private CustomFontTextView hintMsg;
    private CardOptionView jlText;
    private CardOptionView juanText;
    private CardOptionView pacoText;
    private RelativeLayout jl;
    private RelativeLayout juan;
    private RelativeLayout paco;
    private ImageView station;

    /**
     * Indica el estado del juego: <tt>true</tt> indica que est&aacute; mostrando tarjetas,
     * <tt>false</tt> indica que est&aacute; esperando por el mentiroso.
     * @since Module 2 3.0, november 2016
     */
    private boolean state;

    /**
     * Recuerda quien es el honesto.
     * @since Module 2 3.0, november 2016
     */
    private byte honest;

    /**
     * Cuenta el n&uacute;mero de mentiras que ha dicho el honesto, para no hacerlo parecer
     * mentiroso por accidente.
     * @since Module 2 3.0, november 2016
     */
    private byte honestCharHonestPNum;

    /**
     * Indica cuales tarjetas se han mostrado y cuales no.
     * @since Module 2 3.0, november 2016
     */
    private boolean[] cardShown;

    /**
     * Recuerda si ya se ha iniciado el juego.
     * @since Module 2 3.0, november 2016
     */
    private boolean ready;

    /**
     * Recuerda si ya se ha terminado el tiempo antes.
     * @since Module 2 3.0, november 2016
     */
    private byte done;

    /**
     * Reproductores de sonido para los efectos en el fondo
     * @since Module 2 3.0, march 2017
     */
    private MusicHandler phone, bones, chattering;

    /**
     * Referencia al Runnable encargado de reproducir el fondo
     * @since Module 2 3.0, march 2017
     */
    private BackgroundPlayer bkPlayer;

    /**
     * Contandor de tiempo.
     * @since Module 2 3.0 april 2017
     */
    private TimerCounter timerCounter;

    /**
     * Generador de n&uacute;meros aleatoreos.
     * @since Module 2 3.0, november 2016
     */
    private final Random RANDOM = new Random();

    private final Poster POSTER = new Poster();

    /**
     * Indicadores de secuencias de animaci&oacute;n.
     * @since Module 2 0.5, december 2015
     */
    private static final byte TEXT2 = 1;
    private static final byte TEXT3 = 2;
    private static final byte TEXT4 = 3;
    private static final byte TEXT5 = 4;

    /**
     * Indica la siguiente escena del final del juego.
     * @since Module 2 3.0, november 2016
     */
    private static final byte END_SEQUENCE1 = 20;
    private static final byte END_SEQUENCE2 = 21;
    private static final byte END_SEQUENCE3 = 22;
    private static final byte END_SEQUENCE4 = 23;
    private static final byte END_SEQUENCE5 = 24;
    private static final byte END_SEQUENCE6 = 25;

    /**
     * Indica la siguiente escena del final del juego malo.
     * @since Module 2 3.0, november 2016
     */
    private static final byte BAD_END = 30;

    /**
     * Contantes para indicar al honesto.
     * @since Module 2 3.0, november 2016
     */
    private static final byte HONEST_JL = 0;
    private static final byte HONEST_JUAN = 1;
    private static final byte HONEST_PACO = 2;

    /**
     * Cuenta el n&uacute;mero de tarjetas de cada dificultad.
     * @since Module 2 3.0, november 2016
     */
    private static final byte EASY_CARD_NUMBER = 12;
    private static final byte NORMAL_CARD_NUMBER = 17;
    private static final byte HARD_CARD_NUMBER = 15;

    /**
     * Indica el n&uacute;mero de preguntas por tarjeta.
     * @since Module 2 3.0, november 2016
     */
    private static final byte QUESTIONS_PER_CARD = 3;

    /**
     * Indica el n&uacute;mero de preguntas que ocurren antes de preguntar por el honesto.
     * @since Module 2 3.0, november 2016
     */
    private static final byte QUIZ_EVERY_SO_MANY_QUESTIONS = 4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_module2);
        super.loadCommonGraphics(ChatonGameActivity.LEVEL2);
        upperFrame = (CustomFontTextView) findViewById(R.id.score_text);
        upperFrame.setBackgroundResource(R.drawable.level2_top_frame_1824x94);
        upperFrame.setFont("GROBOLD.ttf");
        setLowerFrameDrawable(R.drawable.level2_lower_frame_1000x64);
        score = getIntent().getLongExtra(GameActivity.SCORE_ID, 0);
        displayScore();
        character = (SpriteView) findViewById(R.id.character);
        character.loadFrame(R.drawable.chaton_f1_454x571, getApplicationContext());
        character.loadFrame(R.drawable.chaton_f2_454x571, getApplicationContext());
        character.loadFrame(R.drawable.chaton_f3_454x571, getApplicationContext());
        hintMsg = (CustomFontTextView) findViewById(R.id.card);
        jlText = (CardOptionView) findViewById(R.id.jl_text);
        juanText = (CardOptionView) findViewById(R.id.juan_text);
        pacoText = (CardOptionView) findViewById(R.id.paco_text);
        jl = (RelativeLayout) findViewById(R.id.jl);
        juan = (RelativeLayout) findViewById(R.id.juan);
        paco = (RelativeLayout) findViewById(R.id.paco);
        station = (ImageView) findViewById(R.id.police_station);
        super.initAnimatorThreadsReference();
        hintMsg.setFont("Futura");
        jlText.setFont("Futura");
        juanText.setFont("Futura");
        pacoText.setFont("Futura");
        levelColor = R.color.level2_frame;
        if(tutorialTap != null) tutorialTap.bringToFront();
        hintMsg.bringToFront();
        findViewById(R.id.clues_portfolio).bringToFront();
        bubbleContainer.bringToFront();
        jigsaw.bringToFront();
        fadeScreen.bringToFront();
        hintMsg.setVisibility(View.INVISIBLE);
        timerCounter = new TimerCounter();
        timerCounter.run = true;
        timerCounter.start();
        animatorThreads.put(TimerCounter.class, timerCounter);
        cardShown = new boolean[
                Module2.EASY_CARD_NUMBER +Module2.NORMAL_CARD_NUMBER +Module2.HARD_CARD_NUMBER];
        shownQuestions = 1;
        absoluteBackground.getViewTreeObserver().addOnGlobalLayoutListener(
                new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        absoluteBackground.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                        character.setX(absoluteBackground.getMeasuredWidth()
                                -character.getMeasuredWidth());
                        character.setY(absoluteBackground.getMeasuredHeight());
                        fadeInAnim(fadeScreen).setListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                character.animate().y(absoluteBackground.getMeasuredHeight()
                                        -(character.getHeight() /1.25f));
                                character.animate().setDuration(GameActivity.ANIMATION_DURATION);
                                character.animate().setListener(new AnimatorListenerAdapter() {
                                    @Override
                                    public void onAnimationEnd(Animator animation) {
                                        chatAnim = new ChatonAnimator(character.getX(), character.getY());
                                        ready = true;
                                        if((gameProgress &ChatonGameActivity.PROGRESS_LEVEL2_TUTORIAL) != 0)
                                            nextCard();
                                    }//onAnimationEnd
                                });
                                if(voiceOn) bones.play();
                                character.animate().start();
                            }//onAnimationEnd
                        }).start();
                    }//onGlobalLayout
                });
        pauseAllowed = (gameProgress &ChatonGameActivity.PROGRESS_LEVEL2_TUTORIAL) != 0;
        if(!pauseAllowed)
            initDialogAfterAnim(bubbleContainer, typewriterView, R.string.mod2_text1, character, false,
                    true, Module2.TEXT2, R.raw.mod2_text1, false,
                    Math.round(GameActivity.ANIMATION_DURATION *1.25), false, null, true);
        honest = (byte) RANDOM.nextInt(3);
        state = true;
        jl.setClickable(false);
        paco.setClickable(false);
        juan.setClickable(false);
        chattering = new MusicHandler(this);
        phone = new MusicHandler(this);
        bones = new MusicHandler(this);
        loadCommonSounds();
        pauseAllowed = true;
        soundsHandlers.add(chattering);
        soundsHandlers.add(phone);
        soundsHandlers.add(bones);
        chattering.load(R.raw.crowd, true);
        phone.load(R.raw.phone_ring, false);
        bones.load(R.raw.clicking_teeth, false);
        bkPlayer = new BackgroundPlayer();
    }//onCreate

    @Override
    protected void onResume() {
        super.onResume();
        resumeGame();
    }//onResume

    @Override
    protected void onPause() {
        handler.removeCallbacks(bkPlayer);
        normalTimer.pause();
        hardTimer.pause();
        super.onPause();
    }//onPause

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if(dialogInProgress && !gameOver && ready) {
            animationSequence();
        }//responde al toque sobre la pantalla
        stopTaps(taps, toTap);
        return true;
    }//ontouchEvent

    @Override
    protected void resumeGame() {
        pause = false;
        if(voiceOn) {
            chattering.fadePlay(MusicHandler.DEFAULT_DELAY);
            if(waiting) {
                normalTimer.fadePlay(MusicHandler.DEFAULT_DELAY);
                if(time < ChatonGameActivity.TIMEOUT /3 && time > 0 && state)
                    hardTimer.fadePlay(MusicHandler.DEFAULT_DELAY);
            }//si hay que reproducir los timers
        }//si hay que reproducir sonidos
        resumeAnimatorThreads();
        handler.postDelayed(bkPlayer, randomizer.nextInt(phone.getTrackLength() +1) *5);
    }//resumeGame

    @Override
    public void animationSequence() {
        switch(nextSequence) {
            case Module2.TEXT2 :
                nextSequence = Module2.TEXT3;
                displayDialog(R.string.mod2_text2, bubbleContainer, taps, toTap, R.raw.mod2_text2);
                break;
            case Module2.TEXT3 :
                nextSequence = Module2.TEXT4;
                displayDialog(R.string.mod2_text3, bubbleContainer, taps, toTap, R.raw.mod2_text3);
                break;
            case Module2.TEXT4 :
                nextSequence = Module2.TEXT5;
                displayDialog(R.string.mod2_text4, bubbleContainer, taps, toTap, R.raw.mod2_text4);
                break;
            case Module2.TEXT5 :
                nextSequence = GameActivity.SET_READY;
                displayDialog(R.string.mod2_text5, bubbleContainer, null, null, R.raw.mod2_text5);
                stopTaps(taps, toTap);
                break;
            case GameActivity.DO_NOTHING :
                terminateDialog(bubbleContainer, character, taps, toTap);
                break;
            case GameActivity.SET_READY :
                nextSequence = GameActivity.DO_NOTHING;
                terminateDialog(bubbleContainer, character, taps, toTap);
                pauseAllowed = true;
                if(gameOver) break;
                if(shownQuestions %Module2.QUIZ_EVERY_SO_MANY_QUESTIONS != 0 ||
                        shownQuestions +1 >= ChatonGameActivity.QUESTION_LIMIT) {
                    nextCard();
                } else {
                    initDialogAfterAnim(bubbleContainer, typewriterView, R.string.mod2_honest,
                            character, false, true, GameActivity.DO_NOTHING, R.raw.mod2_honest,
                            true, 0, true, null, false);
                    paco.setClickable(true);
                    jl.setClickable(true);
                    juan.setClickable(true);
                    hintMsg.setText("");
                    pacoText.setVisibility(View.INVISIBLE);
                    jlText.setVisibility(View.INVISIBLE);
                    juanText.setVisibility(View.INVISIBLE);
                    character.changeFrame(R.drawable.chaton_f1_454x571);
                    if((gameProgress &ChatonGameActivity.PROGRESS_LEVEL2_TUTORIAL) == 0) {
                        posicionTaps(tutorialTap, honest == Module2.HONEST_JL ? jl :
                                honest == Module2.HONEST_JUAN ? juan : paco, 0, -lowerFrameH);
                        startTaps(tutorialTaps, tutorialTap, GameActivity.ANIMATION_DURATION);
                        saveGame(ChatonGameActivity.PROGRESS_LEVEL2_TUTORIAL);
                    }//si aún está mostrando el tutorial
                    if(voiceOn) normalTimer.play();
                    state = false;
                    shownQuestions++;
                    time = ChatonGameActivity.TIMEOUT;
                    waiting = true;
                }//si no va apreguntar por el honesto
                break;
            case Module2.END_SEQUENCE1 :
                normalTimer.pause();
                hardTimer.pause();
                handler.removeCallbacks(bkPlayer);
                terminateDialog(bubbleContainer, character, taps, toTap);
                fadeOutAnim(fadeScreen).setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        nextSequence = Module2.END_SEQUENCE2;
                        animationSequence();
                    }//onAnimationEnd
                }).start();
                nextSequence = GameActivity.DO_NOTHING;
                break;
            case Module2.END_SEQUENCE2 :
                jl.setVisibility(View.GONE);
                paco.setVisibility(View.GONE);
                juan.setVisibility(View.GONE);
                hintMsg.setVisibility(View.GONE);
                station.setVisibility(View.VISIBLE);
                findViewById(R.id.window).setVisibility(View.GONE);
                absoluteBackground.setBackgroundResource(0);
                if(Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP_MR1) {
                    absoluteBackground.setBackgroundColor(getResources().getColor(R.color.grass));
                } else {
                    absoluteBackground.setBackgroundColor(getColor(R.color.grass));
                }//si es una versión menor o igual a API 22
                character.changeFrame(R.drawable.chaton_f3_454x571);
                chattering.pause();
                fadeInAnim(fadeScreen).setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        nextSequence = Module2.END_SEQUENCE3;
                        animationSequence();
                    }//onAnimationEnd
                }).start();
                break;
            case Module2.END_SEQUENCE3 :
                initDialogAfterAnim(bubbleContainer, typewriterView, R.string.mod2_ending1,
                        character, false, true, Module2.END_SEQUENCE4, R.raw.mod2_ending1, false, 0,
                        true, null, false);
                break;
            case Module2.END_SEQUENCE4 :
                nextSequence = Module2.END_SEQUENCE5;
                displayDialog(R.string.mod2_ending2, bubbleContainer, null, null,
                        R.raw.mod2_ending2);
                break;
            case Module2.END_SEQUENCE5 :
                nextSequence = Module2.END_SEQUENCE6;
                character.changeFrame(R.drawable.chaton_f2_454x571);
                displayDialog(R.string.mod2_ending3, bubbleContainer, null, null,
                        R.raw.mod2_ending3);
                break;
            case Module2.END_SEQUENCE6 :
                nextSequence = GameActivity.END_LEVEL;
                if(soundFx == null) soundFx = new MusicHandler(this);
                else if(soundFx.isInitialized() && soundFx.isPlaying()) soundFx.pause();
                displayDialog(R.string.mod2_ending4, bubbleContainer, null, null,
                        R.raw.mod2_ending4);
                break;
            case GameActivity.END_LEVEL :
                if(soundFx == null) soundFx = new MusicHandler(this);
                else if(soundFx.isInitialized() && soundFx.isPlaying()) soundFx.pause();
                soundFx.load(R.raw.good_end, false);
                if(voiceOn) soundFx.play();
            case Module2.BAD_END :
                nextSequence = GameActivity.DO_NOTHING;
                timerCounter.run = false;
                terminateDialog(bubbleContainer, character, taps, toTap);
                handler.postDelayed(new TimerTask() {
                    @Override
                    public void run() {
                        Intent result = new Intent();
                        result.putExtra(GameActivity.SCORE_ID, score);
                        setResult(Activity.RESULT_OK, result);
                        finish();
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                    }
                }, GameActivity.ANIMATION_DURATION);
                break;
        }//realiza la secuencia correspondiente
    }//animationSequence

    @Override
    public void transitionControl(int unused) {}

    /**
     * Responde a la acci&oacute;n sobre una de las tarjetas.
     * @param option - La tarjeta seleccionada.
     * @since Module 2 3.0, november 2016
     */
    public void optionSelected(View option) {
        paco.setClickable(false);
        jl.setClickable(false);
        juan.setClickable(false);
        waiting = false;
        stopTaps(taps, toTap);
        bones.rewind();
        if(tutorialTap != null && tutorialTaps != null) stopTaps(tutorialTaps, tutorialTap);
        if(voiceOn) bones.play();
        option.setAlpha(0.5f);
        option.animate().alpha(1);
        option.animate().setDuration(GameActivity.ANIMATION_DURATION /2);
        option.animate().setListener(null);
        option.animate().start();
        hintMsg.animate().alpha(0);
        hintMsg.animate().setDuration(GameActivity.ANIMATION_DURATION /8);
        hintMsg.animate().start();
        if(state) {
            switch(option.getId()) {
                case R.id.paco :
                    cardListener(pacoText);
                    break;
                case R.id.jl :
                    cardListener(jlText);
                    break;
                case R.id.juan :
                    cardListener(juanText);
            }//proporciona la tarjeta adecuada
        } else {
            characterListener(option.getId());
        }//actua conforme al estado de la aplicación
    }//optionSelected

    /**
     * Responde a los mentirosos.
     * @param charId - El identificador del personaje seleccionado.
     * @since Module 2 3.0, november 2016
     */
    private void characterListener(int charId) {
        normalTimer.pause();
        hardTimer.pause();
        state = true;
        boolean correct = false;
        switch(charId) {
            case R.id.paco :
                correct = honest == HONEST_PACO;
                break;
            case R.id.jl :
                correct = honest == HONEST_JL;
                break;
            case R.id.juan :
                correct = honest == HONEST_JUAN;
        }//responde al elegido
        normalTimer.pause();
        hardTimer.pause();
        if(correct) {
            score += ChatonGameActivity.QUESTION_VALUE
                    *(ChatonGameActivity.HARD_MULTIPLIER +ChatonGameActivity.LEVEL2_MULTIPLIER);
            if(saveSpecialProgress(LEVEL2)) {
                clueAnimation();
                initDialogAfterAnim(bubbleContainer, typewriterView, R.string.mod_clue_award,
                        character, false, true, GameActivity.SET_READY, R.raw.mod_clue_award, false,
                        0, true, null, false);
                if(soundFx == null) soundFx = new MusicHandler(this);
                else if(soundFx.isInitialized() && soundFx.isPlaying()) soundFx.pause();
                soundFx.load(R.raw.hiden_clue, false);
                if(voiceOn) soundFx.play();
            } else {
                initDialogAfterAnim(bubbleContainer, typewriterView, R.string.mod2_honest_well_done,
                        character, false, true, GameActivity.SET_READY, R.raw.mod2_honest_well_done,
                        false, 0, true, null, false);
                if(soundFx == null) soundFx = new MusicHandler(this);
                else if(soundFx.isInitialized() && soundFx.isPlaying()) soundFx.pause();
                soundFx.load(R.raw.mod_clue_award, false);
                if(voiceOn) soundFx.play();
            }//si la pista fue nueva y debe ser registrada
        } else {
            initDialogAfterAnim(bubbleContainer, typewriterView, R.string.mod2_honest_error,
                    character, false, true, GameActivity.SET_READY, R.raw.mod2_honest_error, false,
                    0, true, null, false);
            if(soundFx == null) soundFx = new MusicHandler(this);
            else if(soundFx.isInitialized() && soundFx.isPlaying()) soundFx.pause();
            soundFx.load(R.raw.chaton_bell, false);
            if(voiceOn) soundFx.play();
        }//muestra el mensaje adecuado
    }//characterListener

    /**
     * Responde a las acciones sobre las preguntas.
     * @param selectedCard - La tarjeta seleccionada.
     * @since Module 2 3.0, november 2016
     */
    private void cardListener(CardOptionView selectedCard) {
        killChatonAnimThread();
        normalTimer.pause();
        hardTimer.pause();
        adjustDifficulty = false;
        if(!selectedCard.isErroneous()) {
            score += ChatonGameActivity.QUESTION_VALUE
                    *(getDifficultyMultiplier() +ChatonGameActivity.LEVEL2_MULTIPLIER);
            correctQuestions++;
            boolean awarded = false;
            if(correctQuestions > ChatonGameActivity.CLUE_GET_THRESHOLD && clueAwarded == 0) {
                clueAwarded = 1;
                awarded = saveNormalProgress(LEVEL2);
            }//si se debe otorgar una pista
            if(awarded) {
                clueAnimation();
                initDialogAfterAnim(bubbleContainer, typewriterView, R.string.mod_clue_award,
                        character, false, true, GameActivity.SET_READY, R.raw.mod_clue_award, false,
                        0, true, null, false);
                if(soundFx == null) soundFx = new MusicHandler(this);
                else if(soundFx.isInitialized() && soundFx.isPlaying()) soundFx.pause();
                soundFx.load(R.raw.hiden_clue, false);
                if(voiceOn) soundFx.play();
            } else if(shownQuestions >= ChatonGameActivity.QUESTION_LIMIT) {
                pacoText.setVisibility(View.GONE);
                jlText.setVisibility(View.GONE);
                juanText.setVisibility(View.GONE);
                shownQuestions++;
                normalTimer.pause();
                hardTimer.pause();
                if(saveNormalProgress(LEVEL2)) {
                    clueAnimation();
                    initDialogAfterAnim(bubbleContainer, typewriterView, R.string.mod_clue_award,
                            character, false, true, Module2.END_SEQUENCE1, R.raw.mod_clue_award, false,
                            0, true, null, false);
                    if(soundFx == null) soundFx = new MusicHandler(this);
                    else if(soundFx.isInitialized() && soundFx.isPlaying()) soundFx.pause();
                    soundFx.load(R.raw.hiden_clue, false);
                    if(voiceOn) soundFx.play();
                } else {
                    initDialogAfterAnim(bubbleContainer, typewriterView, R.string.mod_end, character,
                            false, true, Module2.END_SEQUENCE1, R.raw.mod_end, false, 0, true, null,
                            false);
                    if(soundFx == null) soundFx = new MusicHandler(this);
                    else if(soundFx.isInitialized() && soundFx.isPlaying()) soundFx.pause();
                    soundFx.load(R.raw.correct_bell, false);
                    if(voiceOn) soundFx.play();
                    hintMsg.setText("");
                }//muestra el final de despedida adecuado
                gameOver = true;
                return;
            } else {
                initDialogAfterAnim(bubbleContainer, typewriterView, R.string.mod2_well_done,
                        character, false, true, GameActivity.SET_READY, R.raw.mod2_well_done, false,
                        0, true, null, false);
                if(soundFx == null) soundFx = new MusicHandler(this);
                else if(soundFx.isInitialized() && soundFx.isPlaying()) soundFx.pause();
                soundFx.load(R.raw.correct_bell, false);
                if(voiceOn) soundFx.play();
            }//muestra el mensaje adecuado
        } else if(errorsCounter >= ChatonGameActivity.ERROR_LIMIT) {
            shownQuestions++;
            pacoText.setVisibility(View.GONE);
            jlText.setVisibility(View.GONE);
            juanText.setVisibility(View.GONE);
            initDialogAfterAnim(bubbleContainer, typewriterView, R.string.mod_bad_end, character,
                    false, true, Module2.BAD_END, R.raw.mod_bad_end, false, 0, true, null, false);
            gameOver = true;
            if(soundFx == null) soundFx = new MusicHandler(this);
            else if(soundFx.isInitialized() && soundFx.isPlaying()) soundFx.pause();
            soundFx.load(R.raw.chaton_bell, false);
            if(voiceOn) soundFx.play();
            return;
        } else {
            errorsCounter++;
            adjustDifficulty = true;
            initDialogAfterAnim(bubbleContainer, typewriterView, R.string.mod2_error, character,
                    false, true, GameActivity.SET_READY, R.raw.mod2_error, false, 0, true, null,
                    false);
            if(soundFx == null) soundFx = new MusicHandler(this);
            else if(soundFx.isInitialized() && soundFx.isPlaying()) soundFx.pause();
            soundFx.load(R.raw.chaton_bell, false);
            if(voiceOn) soundFx.play();
        }//registra el fallo o acierto
        displayScore();
        shownQuestions++;
    }//cardListener

    /**
     * Genera la siguiente tarjeta.
     * @since Module 2, 0.5, november 2015
     */
    private void nextCard() {
        character.changeFrame(R.drawable.chaton_f1_454x571);
        jlText.setVisibility(View.INVISIBLE);
        pacoText.setVisibility(View.INVISIBLE);
        juanText.setVisibility(View.INVISIBLE);
        float percentage = correctQuestions /(float) shownQuestions;
        if(percentage < 0.3 && adjustDifficulty) {
            currentDifficulty = GameActivity.EASY;
        } else if(percentage >= 0.3 && percentage <= 0.6) {
            currentDifficulty = GameActivity.NORMAL;
        } else if(percentage > 0.6) {
            currentDifficulty = GameActivity.HARD;
        }//toma la dificultad actual
        nextQuestion(cardSelector());
        chatAnim = new ChatonAnimator(character.getX(), character.getY());
        chatAnim.run = true;
        chatAnim.start();
        animatorThreads.put(ChatonAnimator.class, chatAnim);
    }//nextCard

    /**
     * Genera la siguiente tarjeta, tomando las opciones de la tarjeta dada.
     * @param currentCard - La tarjeta actual.
     * @since Module 2 3.0, november 2016
     */
    private void nextQuestion(byte currentCard) {
        byte questionSelected;
        byte lowerLimit = 0;
        byte upperLimit = Module2.QUESTIONS_PER_CARD;
        boolean[] optionSelected = new boolean[Module2.QUESTIONS_PER_CARD];
        byte repeats = 0;
        String[] result = new String[Module2.QUESTIONS_PER_CARD];
        byte honestPos = -1;
        do {
            questionSelected = (byte) (RANDOM.nextInt(upperLimit) +lowerLimit);
            if(optionSelected[questionSelected]) {
                if(lowerLimit == questionSelected) {
                    lowerLimit++;
                    upperLimit--;
                } else if(upperLimit == questionSelected) {
                    upperLimit--;
                } if(upperLimit >= 0) {
                    continue;
                }// ???
            }// si escogió una pregunta ya elegida o no
            optionSelected[questionSelected] = true;
            switch(currentDifficulty) {
                case GameActivity.EASY :
                    result[repeats] = getEasyCardPhrase(currentCard, questionSelected);
                    break;
                case GameActivity.NORMAL :
                    result[repeats] = getNormalCardPhrase(currentCard, questionSelected);
                    break;
                case GameActivity.HARD :
                    result[repeats] = getHardCardPhrase(currentCard, questionSelected);
            }//toma la frase
            if(questionSelected == 0) {
                honestPos = repeats;
            }//la frace honesta siempre está en la posición 0 de todas las tarjetas
            repeats++;
        } while(repeats < Module2.QUESTIONS_PER_CARD);
        hintMsg.setAlpha(0);
        hintMsg.setVisibility(View.VISIBLE);
        final float oY = hintMsg.getY();
        hintMsg.setY(displayMetrics.heightPixels /2);
        hintMsg.animate().setDuration(GameActivity.ANIMATION_DURATION /2);
        hintMsg.animate().alpha(1);
        hintMsg.animate().setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                hintMsg.animate().y(oY);
                hintMsg.animate().setStartDelay(GameActivity.ANIMATION_DURATION);
                hintMsg.animate().setDuration(GameActivity.ANIMATION_DURATION /2);
                hintMsg.animate().setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                time = ChatonGameActivity.TIMEOUT;
                                if(voiceOn && normalTimer != null) normalTimer.play();
                                jlText.setVisibility(View.VISIBLE);
                                juanText.setVisibility(View.VISIBLE);
                                pacoText.setVisibility(View.VISIBLE);
                                jl.setClickable(true);
                                paco.setClickable(true);
                                juan.setClickable(true);
                                stopTaps(taps, toTap);
                                waiting = true;
                                if((gameProgress &ChatonGameActivity.PROGRESS_LEVEL2_TUTORIAL) == 0 && shownQuestions == 1) {
                                    posicionTaps(tutorialTap, !jlText.isErroneous() ? jl : !juanText.isErroneous() ?
                                            juan : paco, 0, -lowerFrameH);
                                    startTaps(tutorialTaps, tutorialTap, GameActivity.ANIMATION_DURATION);
                                }//si está mostrando el tutorial
                            }//run
                        }, (int) (GameActivity.ANIMATION_DURATION *1.5));
                        hintMsg.animate().setListener(null);
                    }//onAnimationEnd
                });
                hintMsg.animate().start();
            }//onAnimationEnd
        });
        hintMsg.setText(getReferenceText(currentCard));
        String aux;
        if(honestPos != honest && honestCharHonestPNum > (shownQuestions -1) /3f) {
            aux = result[honestPos];
            result[honestPos] = result[honest];
            result[honest] = aux;
            honestPos = honest;
        } else if(honestPos != honest) {
            honestCharHonestPNum++;
        }//evita que el honesto diga demasiadas mentiras
        jlText.setText(result[0]);
        juanText.setText(result[1]);
        pacoText.setText(result[2]);
        jlText.setErroneous(true);
        juanText.setErroneous(true);
        pacoText.setErroneous(true);
        if(honestPos == 0) {
            jlText.setErroneous(false);
        } else if(honestPos == 1) {
            juanText.setErroneous(false);
        } else {
            pacoText.setErroneous(false);
        }//indica el honesto
        stopTaps(taps, toTap);
        hintMsg.animate().start();
    }//nextQuestion

    /**
     * Dependiendo de la dificultad actual, selecciona la tarjeta que se le va a mostrar al jugador.
     * @return byte - El &iacute;ndice de la tarjeta que se le va a mostrar al jugador.
     * @since Module 2 3.0, november 2016
     */
    private byte cardSelector() {
        byte cardSelected;
        byte lowerLimit = 0;
        byte upperLimit = currentDifficulty == GameActivity.EASY ? Module2.EASY_CARD_NUMBER :
                currentDifficulty == GameActivity.NORMAL ? Module2.NORMAL_CARD_NUMBER :
                        Module2.HARD_CARD_NUMBER;
        byte sizeLimit = upperLimit;
        byte repeats = 0;
        do {
            cardSelected = (byte) (RANDOM.nextInt(upperLimit) +lowerLimit);
            if(cardShown[cardSelected]) {
                repeats++;
                if(repeats > sizeLimit) {
                    break;
                } if(lowerLimit == cardSelected) {
                    lowerLimit++;
                    upperLimit--;
                    if(lowerLimit > upperLimit) {
                        lowerLimit--;
                        upperLimit++;
                    }//si nos pasamos del límite superior
                } else if(upperLimit == cardSelected) {
                    upperLimit--;
                    if(upperLimit < lowerLimit) {
                        upperLimit++;
                    }//si nos pasamos del límite inferior
                }//verificamos que no nos pasamos de la raya
            }//si la tarjeta ya estaba seleccionada
        } while(cardShown[cardSelected]);
        cardShown[cardSelected] = true;
        return cardSelected;
    }//cardSelector

    /**
     * Devuelve una frace de la tarjeta seleccionada. Debe ser una tarjeta de alta baja.
     * @param currentCard - La tarjeta de dificultad baja seleccionada actualmente.
     * @param selected - El &iacute;ndice de la frace de la tarjeta que se desea obtener.
     * @return String - La frace deseada de la tarjeta seleccionada de dificultad baja.
     * @since Module 2 3.0, november 2016
     */
    private String getEasyCardPhrase(byte currentCard, byte selected) {
        switch(currentCard) {
            case 0 : // T1
                switch(selected) {
                    case 0 :
                        return getString(R.string.mod2_t1_ok);
                    case 1 :
                        return getString(R.string.mod2_t1_err1);
                    case 2 :
                        return getString(R.string.mod2_t1_err2);
                }//devuelve la cadena correspondiente
            case 1 : // T2
                switch(selected) {
                    case 0 :
                        return getString(R.string.mod2_t2_ok);
                    case 1 :
                        return getString(R.string.mod2_t2_err1);
                    case 2 :
                        return getString(R.string.mod2_t2_err2);
                }//devuelve la cadena correspondiente
            case 2 : // T5
                switch(selected) {
                    case 0 :
                        return getString(R.string.mod2_t5_ok);
                    case 1 :
                        return getString(R.string.mod2_t5_err1);
                    case 2 :
                        return getString(R.string.mod2_t5_err2);
                }//devuelve la cadena correspondiente
            case 3 : // T8
                switch(selected) {
                    case 0 :
                        return getString(R.string.mod2_t8_ok);
                    case 1 :
                        return getString(R.string.mod2_t8_err1);
                    case 2 :
                        return getString(R.string.mod2_t8_err2);
                }//devuelve la cadena correspondiente
            case 4 : // T9
                switch(selected) {
                    case 0 :
                        return getString(R.string.mod2_t9_ok);
                    case 1 :
                        return getString(R.string.mod2_t9_err1);
                    case 2 :
                        return getString(R.string.mod2_t9_err2);
                }//devuelve la cadena correspondiente
            case 5 : // T13
                switch(selected) {
                    case 0 :
                        return getString(R.string.mod2_t13_ok);
                    case 1 :
                        return getString(R.string.mod2_t13_err1);
                    case 2 :
                        return getString(R.string.mod2_t13_err2);
                }//devuelve la cadena correspondiente
            case 6 : // T18
                switch(selected) {
                    case 0 :
                        return getString(R.string.mod2_t18_ok);
                    case 1 :
                        return getString(R.string.mod2_t18_err1);
                    case 2 :
                        return getString(R.string.mod2_t18_err2);
                }//devuelve la cadena correspondiente
            case 7 : // T24
                switch(selected) {
                    case 0 :
                        return getString(R.string.mod2_t24_ok);
                    case 1 :
                        return getString(R.string.mod2_t24_err1);
                    case 2 :
                        return getString(R.string.mod2_t24_err2);
                }//devuelve la cadena correspondiente
            case 8 : // T32
                switch(selected) {
                    case 0 :
                        return getString(R.string.mod2_t32_ok);
                    case 1 :
                        return getString(R.string.mod2_t32_err1);
                    case 2 :
                        return getString(R.string.mod2_t32_err2);
                }//devuelve la cadena correspondiente
            case 9 : // T37
                switch(selected) {
                    case 0 :
                        return getString(R.string.mod2_t37_ok);
                    case 1 :
                        return getString(R.string.mod2_t37_err1);
                    case 2 :
                        return getString(R.string.mod2_t37_err2);
                }//devuelve la cadena correspondiente
            case 10 : // T43
                switch(selected) {
                    case 0 :
                        return getString(R.string.mod2_t43_ok);
                    case 1 :
                        return getString(R.string.mod2_t43_err1);
                    case 2 :
                        return getString(R.string.mod2_t43_err2);
                }//devuelve la cadena correspondiente
            case 11 : // T44
                switch(selected) {
                    case 0 :
                        return getString(R.string.mod2_t44_ok);
                    case 1 :
                        return getString(R.string.mod2_t44_err1);
                    case 2 :
                        return getString(R.string.mod2_t44_err2);
                }//devuelve la cadena correspondiente
            default:
                return "It's an error to everybody";
        }//devuelve la cadena de la tarjeta correspondente
    }//getEasyCardPhrase

    /**
     * Devuelve una frace de la tarjeta seleccionada. Debe ser una tarjeta de alta normal.
     * @param currentCard - La tarjeta de dificultad normal seleccionada actualmente.
     * @param selected - El &iacute;ndice de la frace de la tarjeta que se desea obtener.
     * @return String - La frace deseada de la tarjeta seleccionada de dificultad normal.
     * @since Module 2 3.0, november 2016
     */
    private String getNormalCardPhrase(byte currentCard, byte selected) {
        switch(currentCard) {
            case 0 : // T4
                switch(selected) {
                    case 0 :
                        return getString(R.string.mod2_t4_ok);
                    case 1 :
                        return getString(R.string.mod2_t4_err1);
                    case 2 :
                        return getString(R.string.mod2_t4_err2);
                }//devuelve la cadena correspondiente
            case 1 : // T6
                switch(selected) {
                    case 0 :
                        return getString(R.string.mod2_t6_ok);
                    case 1 :
                        return getString(R.string.mod2_t6_err1);
                    case 2 :
                        return getString(R.string.mod2_t6_err2);
                }//devuelve la cadena correspondiente
            case 2 : // T7
                switch(selected) {
                    case 0 :
                        return getString(R.string.mod2_t7_ok);
                    case 1 :
                        return getString(R.string.mod2_t7_err1);
                    case 2 :
                        return getString(R.string.mod2_t7_err2);
                }//devuelve la cadena correspondiente
            case 3 : // T10
                switch(selected) {
                    case 0 :
                        return getString(R.string.mod2_t10_ok);
                    case 1 :
                        return getString(R.string.mod2_t10_err1);
                    case 2 :
                        return getString(R.string.mod2_t10_err2);
                }//devuelve la cadena correspondiente
            case 4 : // T12
                switch(selected) {
                    case 0 :
                        return getString(R.string.mod2_t12_ok);
                    case 1 :
                        return getString(R.string.mod2_t12_err1);
                    case 2 :
                        return getString(R.string.mod2_t12_err2);
                }//devuelve la cadena correspondiente
            case 5 : // T14
                switch(selected) {
                    case 0 :
                        return getString(R.string.mod2_t14_ok);
                    case 1 :
                        return getString(R.string.mod2_t14_err1);
                    case 2 :
                        return getString(R.string.mod2_t14_err2);
                }//devuelve la cadena correspondiente
            case 6 : // T15
                switch(selected) {
                    case 0 :
                        return getString(R.string.mod2_t15_ok);
                    case 1 :
                        return getString(R.string.mod2_t15_err1);
                    case 2 :
                        return getString(R.string.mod2_t15_err2);
                }//devuelve la cadena correspondiente
            case 7 : // T17
                switch(selected) {
                    case 0 :
                        return getString(R.string.mod2_t17_ok);
                    case 1 :
                        return getString(R.string.mod2_t17_err1);
                    case 2 :
                        return getString(R.string.mod2_t17_err2);
                }//devuelve la cadena correspondiente
            case 8 : // T19
                switch(selected) {
                    case 0 :
                        return getString(R.string.mod2_t19_ok);
                    case 1 :
                        return getString(R.string.mod2_t19_err1);
                    case 2 :
                        return getString(R.string.mod2_t19_err2);
                }//devuelve la cadena correspondiente
            case 9 : // T20
                switch(selected) {
                    case 0 :
                        return getString(R.string.mod2_t20_ok);
                    case 1 :
                        return getString(R.string.mod2_t20_err1);
                    case 2 :
                        return getString(R.string.mod2_t20_err2);
                }//devuelve la cadena correspondiente
            case 10 : // T26
                switch(selected) {
                    case 0 :
                        return getString(R.string.mod2_t26_ok);
                    case 1 :
                        return getString(R.string.mod2_t26_err1);
                    case 2 :
                        return getString(R.string.mod2_t26_err2);
                }//devuelve la cadena correspondiente
            case 11 : // T27
                switch(selected) {
                    case 0 :
                        return getString(R.string.mod2_t27_ok);
                    case 1 :
                        return getString(R.string.mod2_t27_err1);
                    case 2 :
                        return getString(R.string.mod2_t27_err2);
                }//devuelve la cadena correspondiente
            case 12 : // T29
                switch(selected) {
                    case 0 :
                        return getString(R.string.mod2_t29_ok);
                    case 1 :
                        return getString(R.string.mod2_t29_err1);
                    case 2 :
                        return getString(R.string.mod2_t29_err2);
                }//devuelve la cadena correspondiente
            case 13 : // T33
                switch(selected) {
                    case 0 :
                        return getString(R.string.mod2_t33_ok);
                    case 1 :
                        return getString(R.string.mod2_t33_err1);
                    case 2 :
                        return getString(R.string.mod2_t33_err2);
                }//devuelve la cadena correspondiente
            case 14 : // T40
                switch(selected) {
                    case 0 :
                        return getString(R.string.mod2_t40_ok);
                    case 1 :
                        return getString(R.string.mod2_t40_err1);
                    case 2 :
                        return getString(R.string.mod2_t40_err2);
                }//devuelve la cadena correspondiente
            case 15 : // T41
                switch(selected) {
                    case 0 :
                        return getString(R.string.mod2_t41_ok);
                    case 1 :
                        return getString(R.string.mod2_t41_err1);
                    case 2 :
                        return getString(R.string.mod2_t41_err2);
                }//devuelve la cadena correspondiente
            case 16 : // T42
                switch(selected) {
                    case 0 :
                        return getString(R.string.mod2_t42_ok);
                    case 1 :
                        return getString(R.string.mod2_t42_err1);
                    case 2 :
                        return getString(R.string.mod2_t42_err2);
                }//devuelve la cadena correspondiente
            default :
                return "It's an error to everybody";
        }//devuelve la frace de la tarjeta correspondiente
    }//getNormalCardPhrase

    /**
     * Devuelve una frace de la tarjeta seleccionada. Debe ser una tarjeta de alta dificultad.
     * @param currentCard - La tarjeta de dificultad alta seleccionada actualmente.
     * @param selected - El &iacute;ndice de la frace de la tarjeta que se desea obtener.
     * @return String - La frace deseada de la tarjeta seleccionada de dificultad alta.
     * @since Module 2 3.0, november 2016
     */
    private String getHardCardPhrase(byte currentCard, byte selected) {
        switch(currentCard) {
            case 0 : // T3
                switch(selected) {
                    case 0 :
                        return getString(R.string.mod2_t3_ok);
                    case 1 :
                        return getString(R.string.mod2_t3_err1);
                    case 2 :
                        return getString(R.string.mod2_t3_err2);
                }//devuelve la cadena correspondiente
            case 1 : // T11
                switch(selected) {
                    case 0 :
                        return getString(R.string.mod2_t11_ok);
                    case 1 :
                        return getString(R.string.mod2_t11_err1);
                    case 2 :
                        return getString(R.string.mod2_t11_err2);
                }//devuelve la cadena correspondiente
            case 2 : // T16
                switch(selected) {
                    case 0 :
                        return getString(R.string.mod2_t16_ok);
                    case 1 :
                        return getString(R.string.mod2_t16_err1);
                    case 2 :
                        return getString(R.string.mod2_t16_err2);
                }//devuelve la cadena correspondiente
            case 3 : // T21
                switch(selected) {
                    case 0 :
                        return getString(R.string.mod2_t21_ok);
                    case 1 :
                        return getString(R.string.mod2_t21_err1);
                    case 2 :
                        return getString(R.string.mod2_t21_err2);
                }//devuelve la cadena correspondiente
            case 4 : // T22
                switch(selected) {
                    case 0 :
                        return getString(R.string.mod2_t22_ok);
                    case 1 :
                        return getString(R.string.mod2_t22_err1);
                    case 2 :
                        return getString(R.string.mod2_t22_err2);
                }//devuelve la cadena correspondiente
            case 5 : // T23
                switch(selected) {
                    case 0 :
                        return getString(R.string.mod2_t23_ok);
                    case 1 :
                        return getString(R.string.mod2_t23_err1);
                    case 2 :
                        return getString(R.string.mod2_t23_err2);
                }//devuelve la cadena correspondiente
            case 6 : // T25
                switch(selected) {
                    case 0 :
                        return getString(R.string.mod2_t25_ok);
                    case 1 :
                        return getString(R.string.mod2_t25_err1);
                    case 2 :
                        return getString(R.string.mod2_t25_err2);
                }//devuelve la cadena correspondiente
            case 7 : // T28
                switch(selected) {
                    case 0 :
                        return getString(R.string.mod2_t28_ok);
                    case 1 :
                        return getString(R.string.mod2_t28_err1);
                    case 2 :
                        return getString(R.string.mod2_t28_err2);
                }//devuelve la cadena correspondiente
            case 8 : // T30
                switch(selected) {
                    case 0 :
                        return getString(R.string.mod2_t30_ok);
                    case 1 :
                        return getString(R.string.mod2_t30_err1);
                    case 2 :
                        return getString(R.string.mod2_t30_err2);
                }//devuelve la cadena correspondiente
            case 9 : // T31
                switch(selected) {
                    case 0 :
                        return getString(R.string.mod2_t31_ok);
                    case 1 :
                        return getString(R.string.mod2_t31_err1);
                    case 2 :
                        return getString(R.string.mod2_t31_err2);
                }//devuelve la cadena correspondiente
            case 10 : // T34
                switch(selected) {
                    case 0 :
                        return getString(R.string.mod2_t34_ok);
                    case 1 :
                        return getString(R.string.mod2_t34_err1);
                    case 2 :
                        return getString(R.string.mod2_t34_err2);
                }//devuelve la cadena correspondiente
            case 11 : // T35
                switch(selected) {
                    case 0 :
                        return getString(R.string.mod2_t35_ok);
                    case 1 :
                        return getString(R.string.mod2_t35_err1);
                    case 2 :
                        return getString(R.string.mod2_t35_err2);
                }//devuelve la cadena correspondiente
            case 12 : // T36
                switch(selected) {
                    case 0 :
                        return getString(R.string.mod2_t36_ok);
                    case 1 :
                        return getString(R.string.mod2_t36_err1);
                    case 2 :
                        return getString(R.string.mod2_t36_err2);
                }//devuelve la cadena correspondiente
            case 13 : // T38
                switch(selected) {
                    case 0 :
                        return getString(R.string.mod2_t38_ok);
                    case 1 :
                        return getString(R.string.mod2_t38_err1);
                    case 2 :
                        return getString(R.string.mod2_t38_err2);
                }//devuelve la cadena correspondiente
            case 14 : // T39
                switch(selected) {
                    case 0 :
                        return getString(R.string.mod2_t39_ok);
                    case 1 :
                        return getString(R.string.mod2_t39_err1);
                    case 2 :
                        return getString(R.string.mod2_t39_err2);
                }//devuelve la cadena correspondiente
            default:
                return "It's an error to everybody";
        }//devuelve la frace de la tarjeta correspondiente
    }//getHardCardPhrase

    /**
     * Devuelve el texto de referencia de la tarjeta.
     * @param currentCard - La tarjeta seleccionada.
     * @return String - El texto de referencia de la tarjeta seleccionada.
     * @since Module 2 3.0, november 2016
     */
    private String getReferenceText(byte currentCard) {
        switch(currentDifficulty) {
            case GameActivity.EASY :
                switch(currentCard) {
                    case 0 : // T1
                        return getString(R.string.mod2_t1);
                    case 1 : // T2
                        return getString(R.string.mod2_t2);
                    case 2 : // T5
                        return getString(R.string.mod2_t5);
                    case 3 : // T8
                        return getString(R.string.mod2_t8);
                    case 4 : // T9
                        return getString(R.string.mod2_t9);
                    case 5 : // T13
                        return getString(R.string.mod2_t13);
                    case 6 : // T18
                        return getString(R.string.mod2_t18);
                    case 7 : // T24
                        return getString(R.string.mod2_t24);
                    case 8 : // T32
                        return getString(R.string.mod2_t32);
                    case 9 : // T37
                        return getString(R.string.mod2_t37);
                    case 10 : // T43
                        return getString(R.string.mod2_t43);
                    case 11 : // T44
                        return getString(R.string.mod2_t44);
                }//devuelve la frase correcta del la dificultad fácil
            case GameActivity.NORMAL :
                switch(currentCard) {
                    case 0 : // T4
                        return getString(R.string.mod2_t4);
                    case 1 : // T6
                        return getString(R.string.mod2_t6);
                    case 2 : // T7
                        return getString(R.string.mod2_t7);
                    case 3 : // T10
                        return getString(R.string.mod2_t10);
                    case 4 : // T12
                        return getString(R.string.mod2_t12);
                    case 5 : // T14
                        return getString(R.string.mod2_t14);
                    case 6 : // T15
                        return getString(R.string.mod2_t15);
                    case 7 : // T17
                        return getString(R.string.mod2_t17);
                    case 8 : // T19
                        return getString(R.string.mod2_t19);
                    case 9 : // T20
                        return getString(R.string.mod2_t20);
                    case 10 : // T26
                        return getString(R.string.mod2_t26);
                    case 11 : // T27
                        return getString(R.string.mod2_t27);
                    case 12 : // T29
                        return getString(R.string.mod2_t29);
                    case 13 : // T33
                        return getString(R.string.mod2_t33);
                    case 14 : // T40
                        return getString(R.string.mod2_t40);
                    case 15 : // T41
                        return getString(R.string.mod2_t41);
                    case 16 : // T42
                        return getString(R.string.mod2_t42);
                }//devuelve la frace correcta de la tarjeta en dificultad normal
            case GameActivity.HARD :
                switch(currentCard) {
                    case 0 : // T3
                        return getString(R.string.mod2_t3);
                    case 1 : // T11
                        return getString(R.string.mod2_t11);
                    case 2 : // T16
                        return getString(R.string.mod2_t16);
                    case 3 : // T21
                        return getString(R.string.mod2_t21);
                    case 4 : // T22
                        return getString(R.string.mod2_t22);
                    case 5 : // T23
                        return getString(R.string.mod2_t23);
                    case 6 : // T25
                        return getString(R.string.mod2_t25);
                    case 7 : // T28
                        return getString(R.string.mod2_t28);
                    case 8 : // T30
                        return getString(R.string.mod2_t30);
                    case 9 : // T31
                        return getString(R.string.mod2_t31);
                    case 10 : // T34
                        return getString(R.string.mod2_t34);
                    case 11 : // T35
                        return getString(R.string.mod2_t35);
                    case 12 : // T36
                        return getString(R.string.mod2_t36);
                    case 13 : // T38
                        return getString(R.string.mod2_t38);
                    case 14 : // T39
                        return getString(R.string.mod2_t39);
                }//devuelve la frace correcta de la tarjeta en dificultad difícil
            default:
                return "It's an error to everybody";
        }//devuelve la frace correcta de la tarjeta de acuerdo a la dificultad actual
    }//getReferenceString

    @Override
    public void showClues(View v) {
        super.showClues(v);
        pauseAnimatorThreads();
        pauseMusicHandlers();
        pause = true;
    }//showClues

    @Override
    protected void hideCluesBriefcase() {
        super.hideCluesBriefcase();
        resumeGame();
    }//hideCluesBriefcase

    // clases anidadas

    /**
     * Realiza la cuenta hacia 0 del tiempo que tiene el jugador para responder un nivel y
     * adem&aacute;s solicita la muestra de la pregunta por el honesto.
     * @author <a href="mailto:manuel_castillo_cc@ciencias.unam.mx" >Manuel "Nachintoch"
     * Castillo</a>
     * @version 1.0, november 2016
     * @since Module 2 3.0, november 2016
     */
    private class TimerCounter extends AnimatorThread {

        /**
         * Referencia al nivel 2.
         * @since Module 2 3.0, march 2017
         */
        Module2 parent;

        @Override
        public void run() {
            long elapsedT;
            boolean lastW = waiting;
            while(run) {
                elapsedT = System.currentTimeMillis();
                try {
                    Thread.sleep(ChatonGameActivity.ANIMATION_DELAY);
                } catch(InterruptedException e) {
                } finally {
                    elapsedT = System.currentTimeMillis() -elapsedT;
                }//intenta dormir
                if(pause || !waiting) continue;
                if(waiting != lastW) {
                    lastW = waiting;
                    done = 0;
                }//si ya no espera, entonces no ha acabado
                time -= elapsedT;
                if((ChatonGameActivity.TIMEOUT *2 /3) > time &&
                        time > (ChatonGameActivity.TIMEOUT /3) && state && done == 0) {
                    POSTER.state = Poster.CHATON_NERVOUS;
                    handler.post(POSTER);
                    if(soundFx == null) soundFx = new MusicHandler(parent);
                    else if(soundFx.isInitialized() && soundFx.isPlaying()) soundFx.pause();
                    soundFx.load(R.raw.hurry_up, false);
                    if(voiceOn) soundFx.play();
                    done = 1;
                } if(time < ChatonGameActivity.TIMEOUT /3 && time > 0 && state && done == 1) {
                    POSTER.state = Poster.CHATON_PANIC;
                    hardTimer.setVolume(MusicHandler.MAX_VOLUME);
                    if(voiceOn) hardTimer.play();
                    if(soundFx == null) soundFx = new MusicHandler(parent);
                    else if(soundFx.isInitialized() && soundFx.isPlaying()) soundFx.pause();
                    soundFx.load(R.raw.hurry_up, false);
                    if(voiceOn) soundFx.play();
                    handler.post(POSTER);
                    done = 2;
                } if(time <= 0 && (done == 2 || !state)) {
                    done = 3;
                    if(state) {
                        errorsCounter++;
                        shownQuestions++;
                        adjustDifficulty = true;
                        POSTER.state = Poster.TIMEOUT;
                        if(soundFx == null) soundFx = new MusicHandler(parent);
                        else if(soundFx.isInitialized() && soundFx.isPlaying()) soundFx.pause();
                        soundFx.load(R.raw.timeout_bell, false);
                        if(voiceOn) soundFx.play();
                    } else {
                        state = true;
                        POSTER.state = Poster.DISPLAY_CARD;
                        time = ChatonGameActivity.TIMEOUT;
                        absoluteBackground.post(new Runnable() {
                            @Override
                            public void run() {
                                terminateDialog(bubbleContainer, character, taps, toTap);
                            }//run
                        });
                    }//termina el tiepo de acuerdo al estado
                    waiting = false;
                    done = 0;
                    normalTimer.pause();
                    hardTimer.pause();
                    handler.post(POSTER);
                }//si se terminó el tiempo
            }//mientras deba ejecutarse
        }//run

    }//TimerCounter

    /**
     * Permite al temporizador realizar cabios sobre la interfaz gr&aacute;fica, a travez del
     * <tt>handler</tt> definido en el nivel por la <tt>Game Activity</tt>.
     * @author <a href="mailto:manuel_castillo_cc@ciencias.unam.mx" >Manuel "Nachintoch"
     * Castillo</a>
     * @version 1.0, november 2016
     * @since Module 2 3.0, november 2016
     */
    private class Poster implements Runnable {

        /**
         * El estado del POSTER.
         * @since Poster 1.0, november 2016
         */
        byte state;

        /**
         * Definici&oacute;n de todos los posibles estados del posteador.
         * @since Poster 1.0, november 2016
         */
        static final byte CHATON_NERVOUS = 1;
        static final byte CHATON_PANIC = 2;
        static final byte TIMEOUT = 3;
        static final byte DISPLAY_CARD = 4;
        static final byte NONE = -1;

        @Override
        public void run() {
            switch(state) {
                case Poster.CHATON_NERVOUS :
                    character.changeFrame(R.drawable.chaton_f2_454x571);
                    break;
                case Poster.CHATON_PANIC :
                    character.changeFrame(R.drawable.chaton_f3_454x571);
                    break;
                case Poster.TIMEOUT :
                    killChatonAnimThread();
                    jl.setClickable(false);
                    paco.setClickable(false);
                    juan.setClickable(false);
                    hintMsg.setText("");
                    if(tutorialTap != null && tutorialTaps != null)
                        stopTaps(tutorialTaps, tutorialTap);
                    if(errorsCounter >= ChatonGameActivity.ERROR_LIMIT) {
                        initDialogAfterAnim(bubbleContainer, typewriterView, R.string.mod_bad_end,
                                character, false, true, Module2.BAD_END, R.raw.mod_bad_end, false,
                                0, true, null, false);
                        gameOver = true;
                    } else {
                        initDialogAfterAnim(bubbleContainer, typewriterView, R.string.mod_timeout,
                                character, false, true, GameActivity.SET_READY, R.raw.mod_timeout,
                                false, 0, true, null, false);
                    }//si termina el juego
                    break;
                case Poster.DISPLAY_CARD :
                    if(!gameOver) nextCard();
                    if(tutorialTap != null) stopTaps(tutorialTaps, tutorialTap);
            }//realiza la acción adecuada
            state = NONE;
            handler.removeCallbacks(this);
        }//run

    }//Poster runnable

    /**
     * Reproduce los sonidos en el fondo.
     * @author <a href="mailto:manuel_castillo_cc@ciencias.unam.mx" >Manuel "Nachintoch"
     * Castillo</a>
     * @version 1.0, march 2017
     * @since Map 2.0, march 2017
     */
    private class BackgroundPlayer implements Runnable {

        @Override
        public void run() {
            handler.removeCallbacks(this);
            phone.rewind();
            if(voiceOn) phone.play();
            handler.postDelayed(this, randomizer.nextInt(phone.getTrackLength() +1) *5);
        }//run

    }//Background Player Runnable

}//Module2

