package mx.nachintoch.vg.chaton;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.RelativeLayout;

import java.util.Timer;
import java.util.TimerTask;

import mx.nachintoch.vg.android.CustomFontTextView;
import mx.nachintoch.vg.android.DrawView;
import mx.nachintoch.vg.android.GameActivity;
import mx.nachintoch.vg.android.HScroll2D;
import mx.nachintoch.vg.android.MusicHandler;
import mx.nachintoch.vg.android.SpriteView;
import mx.nachintoch.vg.android.Typewriter;
import mx.nachintoch.vg.android.VScroll2D;

/**
 * Imementa el m&oacute;dulo 1 del juego. Esta es la parte donde Chat&oacute;n lleva al jugador
 * al estudio de arte para analizar unas pistas que no puede entender.
 * @author <a href="mailto:manuel_castillo_cc@ciencias.unam.mx" >Manuel "Nachintoch" Castillo</a>
 * @author Ricardo L&aacute;zaro
 * @author Jonathan Guerrero P&eacute;rez
 * @version 3.0, october 2016
 * @since Misterios en Pregui 0.5, december 2015
 */
public class Module1 extends ChatonGameActivity {

    // atributos de clase

    /**
     * Referencia a la tarjeta con las pistas a estudiar.
     * @since Module 1 0.5, december 2015
     */
    private RelativeLayout cardView;

    /**
     *Referencia al lienzo sobre el que dibuja el jugador.
     * @since Module 1 0.5, december 2015
     */
    private DrawView canvas;

    /**
     * Referencia a la opci&oacute;n uno de la tarjeta.
     * @since Module 1 0.5, december 2015
     */
    private CardOptionView option1;

    /**
     * Referencia a la opci&oacute;n dos de la tarjeta.
     * @since Module 1 0.5, december 2015
     */
    private CardOptionView option2;

    /**
     * Referencia a la opci&oacute;n tres de la tarjeta.
     * @since Module 1 0.5, december 2015
     */
    private CardOptionView option3;

    /**
     * Indica si el jugador acaba de responder y si acerto o fallo.
     * @since Module 1 0.5, december 2015
     */
    private byte answered;

    /**
     * Indica cuales tarjetas se han mostrado y cuales no.
     * @since Module 1 1.0, january 2016
     */
    private boolean[] cardShown; //le pondría Shawn si no se viera raro --> Winchester!

    /**
     * Recuerda cu&aacute;l fue la tarjeta seleccionada actualmente.
     * @since Module 1 3.0, november 2016
     */
    private byte currentCard;

    /**
     * Recuerda si ya ha detectado pistas para no repetirlas.
     * @since Module 1 3.0, november 2016
     */
    private boolean clueDetected;

    /**
     * Referencia a los sprites del nivel.
     * @since Module 1 3.0, october 2016
     */
    private SpriteView poodleRoss;
    private SpriteView window;
    private SpriteView plane;
    private SpriteView beto;
    private SpriteView flag;
    private SpriteView tv;
    private SpriteView book;
    private SpriteView bear;

    /**
     * Referencia al hilo que anima el brillo de los objetos en pantalla.
     * @since Module 1 3.0 november 2016
     */
    private Glower glower;

    /**
     * Contador de tiempo.
     * @since Module 1 3.0, november 2016
     */
    private TimeCounter timeCounter;

    /**
     * Referencias a los receptores de eventos de las opciones de las tarjetas.
     * @since Module 1 3.0, november 2016
     */
    private Option1AnimationListener option1AnimListener;
    private Option2AnimationListener option2AnimListener;
    private Option3AnimationListener option3AnimListener;

    /**
     * Reproductores de sonido adcionales.
     * @since Module 1 3.0, march 2017
     */
    private MusicHandler wingFlaps;
    private MusicHandler iDidnt;

    /**
     * Indica si y cu&aacute;l de las pistas ocultas dif&iacute;ciles se asocia a la tarjeta que se
     * le muestra al usuario.
     */
    private byte hardHiddenClue;

    /**
     * Recuerda si se est&aacute; mostrando una tarjeta o no.
     * @since Module 1 3.0, march 2017
     */
    private boolean showingCarad;

    /**
     * Referencia a un objeto que contiene las m&eacute;tricas del display
     * @since Module 1 3.0, november 2016
     */
    private final DisplayMetrics DISPLAY_METRICS = new DisplayMetrics();

    /**
     * Indica que la siguiente escena es el segundo texto.
     * @since Module 1 0.5, december 2015
     */
    private static final byte TEXT2 = 1;
    private static final byte TEXT3 = 2;

    /**
     * Indica que la siguiente escena es la primera del final.
     * @since Module 1 3.0, november 2016.
     */
    private static final byte END_SEQUENCE1 = 20;
    private static final byte END_SEQUENCE2 = 21;
    private static final byte END_SEQUENCE3 = 22;
    private static final byte END_SEQUENCE4 = 23;
    private static final byte END_SEQUENCE5 = 24;
    private static final byte END_SEQUENCE6 = 25;

    /**
     * Indican que la siguiente escena es una de los hints.
     * @since Module 1 3.0, november 2016
     */
    private static final byte HINT1 = 10;
    private static final byte HINT3 = 12;
    private static final byte HINT4 = 13;

    /**
     * Indica que la siguiente escena es la mala transici&oacute;n al mapa.
     * @since Module 1 1.0, january 2016
     */
    private static final byte BAD_END = 30;

    /**
     * Indica que el jugador ha encontrado una pista.
     * @since Module 1 3.0, november 2016
     */
    private static final byte CLUE = 40;

    /**
     * Indica el n&uacute;mero total de tarjetas f&aacute;ciles disponibles.
     * @since Module 1 1.0, january 2015
     */
    private static final byte EASY_CARDS_NUMBER = 8;

    /**
     * Indica el n&uacute;mero total de tarjetas de dificultad normal disponibles.
     * @since Module 1 1.0, january 2015
     */
    private static final byte NORMAL_CARDS_NUMBER = 12;

    /**
     * Indica el n&uacute;mero total de tarjetas dif&iacute;ciles disponibles.
     * @since Module 1 2.0, october 2016
     */
    public static final byte HARD_CARDS_NUMBER = 9;

    /**
     * Indica el n&uacute;mero total de fraces dif&iacute;ciles disponibles.
     * @since Module 1 0.5, december 2015
     */
    public static final byte HARD_PHRASES_NUMBER = 5;

    /**
     * Indica el n&uacute;mero de opciones por tarjeta.
     * @since Module 1 0.5, december 2015
     */
    private static final byte OPTIONS_NUMBER = 3;

    /**
     * Indica que el jugador no ha respondido.
     * @since Module 1 0.5, december 2015
     */
    private static final byte NO_ANSWER = 0;

    /**
     * Indica que el jugador respondio correctamente.
     * @since Module 1 0.5, december 2015
     */
    private static final byte CORRECT = 1;

    /**
     * Indica que el jugador respondie incorrctamente.
     * @since Module 1 0.5, december 2015
     */
    private static final byte WRONG = 2;

    // métodos de implementación

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_module1);
        super.loadCommonGraphics(ChatonGameActivity.LEVEL1);
        cardView = (RelativeLayout) findViewById(R.id.card_container);
        canvas = (DrawView) findViewById(R.id.canvas);
        option1 = (CardOptionView) findViewById(R.id.card_option1);
        option2 = (CardOptionView) findViewById(R.id.card_option2);
        option3 = (CardOptionView) findViewById(R.id.card_option3);
        vScroll = (VScroll2D) findViewById(R.id.vScroll);
        hScroll = (HScroll2D) findViewById(R.id.hScroll);
        character = (SpriteView) findViewById(R.id.character);
        background = (RelativeLayout) findViewById(R.id.background);
        poodleRoss = (SpriteView) findViewById(R.id.poodle_ross);
        bear = (SpriteView) findViewById(R.id.banjo);
        book = (SpriteView) findViewById(R.id.book);
        tv = (SpriteView) findViewById(R.id.tv);
        flag = (SpriteView) findViewById(R.id.flag);
        beto = (SpriteView) findViewById(R.id.beto);
        plane = (SpriteView) findViewById(R.id.plane);
        window = (SpriteView) findViewById(R.id.window);
        loadCommonSounds();
        wingFlaps = new MusicHandler(this);
        wingFlaps.load(R.raw.wing_flapping, false);
        iDidnt = new MusicHandler(this);
        iDidnt.load(R.raw.i_didnt, false);
        soundsHandlers.add(wingFlaps);
        soundsHandlers.add(iDidnt);
        initSprites(poodleRoss);
        initSprites(bear);
        initSprites(book);
        initSprites(tv);
        initSprites(flag);
        initSprites(beto);
        initSprites(plane);
        initSprites(window);
        if(Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
            canvas.setHint(R.string.mod1_canvas_hint, 100, 300,
                    getResources().getDimensionPixelSize(R.dimen.card_text),
                    getColor(R.color.black), "Futura", getApplicationContext());
        } else {
            canvas.setHint(R.string.mod1_canvas_hint, 100, 300,
                    getResources().getDimensionPixelSize(R.dimen.card_text),
                    getResources().getColor(R.color.black), "Futura", getApplicationContext());
        }//asigna el hint del canvas de acuerdo a la versión de android
        character.loadFrame(R.drawable.chaton_f1_454x571, getApplicationContext());
        character.loadFrame(R.drawable.chaton_f2_454x571, getApplicationContext());
        character.loadFrame(R.drawable.chaton_f3_454x571, getApplicationContext());
        window.loadFrame(R.drawable.window_f1_727x505, getApplicationContext());
        window.loadFrame(R.drawable.window_f2_727x505, getApplicationContext());
        poodleRoss.loadFrame(R.drawable.casa_543x360, getApplicationContext());
        levelColor = R.color.level1_frame;
        getWindowManager().getDefaultDisplay().getMetrics(DISPLAY_METRICS);
        super.initAnimatorThreadsReference();
        glower = new Glower();
        glower.run = true;
        glower.start();
        glower.glowRoss = true;
        super.animatorThreads.put(Glower.class, glower);
        enableSprites(false);
        upperFrame = (CustomFontTextView) findViewById(R.id.score_text);
        upperFrame.setBackgroundResource(R.drawable.level1_top_frame_1824x94);
        upperFrame.setFont("GROBOLD.ttf");
        setLowerFrameDrawable(R.drawable.level1_lower_frame_1824x91);
        canvas.setShouldRespond(false);
        answered = Module1.NO_ANSWER;
        cardShown = new boolean[
                Module1.EASY_CARDS_NUMBER +Module1.NORMAL_CARDS_NUMBER +Module1.HARD_CARDS_NUMBER];
        cardView.animate().setDuration(GameActivity.ANIMATION_DURATION);
        canvas.animate().setDuration(GameActivity.ANIMATION_DURATION);
        option1.animate().setDuration(GameActivity.ANIMATION_DURATION);
        option2.animate().setDuration(GameActivity.ANIMATION_DURATION);
        option3.animate().setDuration(GameActivity.ANIMATION_DURATION);
        option1.setAlpha(0);
        option2.setAlpha(0);
        option3.setAlpha(0);
        option1AnimListener = new Option1AnimationListener();
        option2AnimListener = new Option2AnimationListener();
        option3AnimListener = new Option3AnimationListener();
        option3AnimListener.parent = this;
        timer = new Timer("mx.nachintoch.vg.chaton.Level_1_timer");
        cardView.bringToFront();
        if(tutorialTap != null) tutorialTap.bringToFront();
        bubbleContainer.bringToFront();
        jigsaw.bringToFront();
        ViewTreeObserver vto = absoluteBackground.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                absoluteBackground.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                cardView.setX(-cardView.getMeasuredWidth());
                canvas.setX(absoluteBackground.getMeasuredWidth() +50);
                canvas.setY(upperFrame.getHeight() +10);
                cardView.setY(upperFrame.getHeight() +10);
            }//onGlobalLayout
        });
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                translationAnim(character, 0, getResources().getDimension(R.dimen.chaton_margin),
                        null);
            }//run
        }, GameActivity.ANIMATION_DURATION);
        poodleRoss.setActive(true);
        poodleRoss.animate().start();
        shownQuestions = 1;
        lastAnsPos = -1;
        prepareCompleteDisplay(background, null);
        score = getIntent().getLongExtra(GameActivity.SCORE_ID, 0);
        displayScore();
        waiting = (gameProgress &ChatonGameActivity.PROGRESS_LEVEL1_TUTORIAL) == 0;
        activated = !waiting;
        if(!activated)
            initDialogAfterAnim(bubbleContainer, typewriterView, R.string.mod1_text1, character,
                    true, false, Module1.TEXT2, R.raw.mod1_text1, false, Math.round(
                            GameActivity.ANIMATION_DURATION *getFloatResource(R.dimen.map_presentaion_delay)), false, null,
                    true);
        fadeInAnim(fadeScreen).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                if(background.getScaleX() != 1) {
                    background.animate().scaleX(1);
                    background.animate().scaleY(1);
                    background.animate().x(0);
                    background.animate().y(0);
                    background.animate().setDuration(GameActivity.ANIMATION_DURATION);
                    background.animate().setListener(null);
                    background.animate().start();
                }//realiza la animación de introducción
            }//si hace falta animar el fondo
        }).start();
    }//onCreate

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if(dialogInProgress) {
            if(!gameOver && activated && shownQuestions > 3) {
                nextSequence = GameActivity.DO_NOTHING;
                super.terminateDialog(bubbleContainer, character, taps, toTap);
            }//salta más diálogos
        }//si hay un diálogo en progreso
        if(waiting) {
            switch (nextSequence) {
                case GameActivity.DO_NOTHING :
                    terminateDialog(bubbleContainer, character, taps, toTap);
            }//salta un diálogo
        }//termina un diálogo o sólo lo avanza
        return super.onTouchEvent(event);
    }//onTochEvent */

    @Override
    public void onPause() {
        if(timer != null) {
            timer.purge();
            timer.cancel();
        }//si hay un timer activo
        super.onPause();
    }//onPause

    @Override
    protected void animationSequence() {
        switch (nextSequence) {
            case Module1.TEXT2 :
                nextSequence = Module1.TEXT3;
                displayDialog(R.string.mod1_text2, bubbleContainer, taps, toTap, R.raw.mod1_text2);
                break;
            case Module1.TEXT3 :
                nextSequence = Module1.HINT1;
                displayDialog(R.string.mod1_text3, bubbleContainer, null, null, R.raw.mod1_text3);
                break;
            case Module1.HINT1 :
                nextSequence = Module1.DO_NOTHING;
                terminateDialog(bubbleContainer, character, taps, toTap);
                final AnimatorSet scrollAnim = scrollToParrot();
                scrollAnim.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        scrollAnim.removeListener(this);
                        initDialogAfterAnim(bubbleContainer, typewriterView, R.string.mod1_hint1,
                                character, true, false, GameActivity.DO_NOTHING, R.raw.mod1_hint1,
                                true, 0, false, null, false);
                        stopTaps(taps, toTap);
                        posicionTapsInDragScreen(tutorialTap, poodleRoss, 0, lowerFrameH *2);
                        startTaps(tutorialTaps, tutorialTap, GameActivity.ANIMATION_DURATION);
                    }//onAnimationEnd
                });
                scrollAnim.start();
                break;
            case Module1.HINT3 :
                nextSequence = GameActivity.DO_NOTHING;
                if(shownQuestions >= 2 ||
                        (gameProgress &ChatonGameActivity.PROGRESS_LEVEL1_TUTORIAL) != 0)
                    displayDialog(R.string.mod1_hint3, bubbleContainer, null, null,
                            R.raw.mod1_hint3);
                startTaps(drag, toDrag, 0);
                character.setClickable(true);
                break;
            case Module1.HINT4 :
                nextSequence = GameActivity.DO_NOTHING;
                if(shownQuestions <= 2) {
                    displayDialog(R.string.mod1_help1, bubbleContainer, taps, toTap,
                            R.raw.mod1_help1);
                } else {
                    terminateDialog(bubbleContainer, character, taps, toTap);
                }//si es la primera vez que se activa una pista
                break;
            case Module1.CLUE :
                nextSequence = GameActivity.DO_NOTHING;
                answered = Module1.NO_ANSWER;
                break;
            case Module1.END_SEQUENCE1 :
                terminateDialog(bubbleContainer, character, taps, toTap);
                fadeOutAnim(fadeScreen).setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        nextSequence = Module1.END_SEQUENCE2;
                        animationSequence();
                    }//onAnimationEnd
                }).start();
                nextSequence = GameActivity.DO_NOTHING;
                break;
            case Module1.END_SEQUENCE2 :
                bear.setVisibility(View.GONE);
                book.setVisibility(View.GONE);
                tv.setVisibility(View.GONE);
                flag.setVisibility(View.GONE);
                plane.setVisibility(View.GONE);
                window.setVisibility(View.GONE);
                glower.glowBeto = false;
                hScroll.scrollTo(0, 0);
                vScroll.scrollTo(0, 0);
                poodleRoss.setX(375);
                poodleRoss.setY(225);
                poodleRoss.setScaleX(1.67f);
                background.setBackgroundResource(0);
                if(Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP_MR1) {
                    background.setBackgroundColor(getResources().getColor(R.color.grass));
                } else {
                    background.setBackgroundColor(getColor(R.color.grass));
                }//si es una versión menor o igual a API 22
                poodleRoss.changeFrame(R.drawable.casa_543x360);
                beto.setVisibility(View.VISIBLE);
                beto.setX(DISPLAY_METRICS.widthPixels -(character.getMeasuredWidth() *1.5f));
                beto.setY(DISPLAY_METRICS.heightPixels);
                beto.bringToFront();
                beto.setAlpha(1f);
                fadeInAnim(fadeScreen).setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        nextSequence = Module1.END_SEQUENCE3;
                        animationSequence();
                    }//onAnimationEnd
                }).start();
                break;
            case Module1.END_SEQUENCE3 :
                initDialogAfterAnim(bubbleContainer, typewriterView, R.string.mod1_ending1,
                        character, true, false, Module1.END_SEQUENCE4, R.raw.mod1_ending1, false, 0,
                        true, null, false);
                break;
            case Module1.END_SEQUENCE4 :
                terminateDialog(bubbleContainer, character, taps, toTap);
                beto.animate().translationY(-8);
                beto.animate().setDuration(GameActivity.ANIMATION_DURATION);
                beto.animate().setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        bubbleContainer.setScaleX(-bubbleContainer.getScaleX());
                        bubbleContainer.setScaleY(-bubbleContainer.getScaleY());
                        initDialogAfterAnim(bubbleContainer, typewriterView, R.string.mod1_ending2,
                                beto, false, true, Module1.END_SEQUENCE5, R.raw.mod1_ending2, false,
                                0, true, null, false);
                    }//onAnimationEnd
                }).start();
                break;
            case Module1.END_SEQUENCE5 :
                terminateDialog(bubbleContainer, beto, taps, toTap);
                character.changeFrame(R.drawable.chaton_f3_454x571);
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        bubbleContainer.setScaleX(-bubbleContainer.getScaleX());
                        bubbleContainer.setScaleY(-bubbleContainer.getScaleY());
                        initDialogAfterAnim(bubbleContainer, typewriterView, R.string.mod1_ending3,
                                character, true, false, Module1.END_SEQUENCE6, R.raw.mod1_ending3,
                                false, 0, true, null, false);
                    }//run
                }, GameActivity.ANIMATION_DURATION);
                if(soundFx == null) soundFx = new MusicHandler(this);
                else if(soundFx.isInitialized() && soundFx.isPlaying()) soundFx.pause();
                soundFx.load(R.raw.chaton_bell, false);
                if(voiceOn) soundFx.play();
                break;
            case Module1.END_SEQUENCE6 :
                nextSequence = Module1.END_LEVEL;
                character.changeFrame(R.drawable.chaton_f1_454x571);
                displayDialog(R.string.mod1_ending4, bubbleContainer, null, null,
                        R.raw.mod1_ending4);
                break;
            case GameActivity.END_LEVEL :
                soundFx.load(R.raw.good_end, false);
                if(voiceOn) soundFx.play();
            case Module1.BAD_END :
                terminateDialog(bubbleContainer, character, taps, toTap);
                timer = new Timer("mx.nachintoch.vg.chaton.Level_1_timer");
                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        Intent result = new Intent();
                        result.putExtra(GameActivity.SCORE_ID, score);
                        setResult(Activity.RESULT_OK, result);
                        finish();
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                    }
                }, GameActivity.ANIMATION_DURATION);
                break;
            case GameActivity.DO_NOTHING :
                waiting = false;
                terminateDialog(bubbleContainer, character, taps, toTap);
                startTaps(drag, toDrag, GameActivity.CLICK_INDICATOR_DURATION);
        }//muestra el diálogo adecuado
    }//animationSequence

    /**
     * Responde al toque sobre una opci&oacute;n de la tarjeta.
     * @param option - Referencia a la opci&oacute;n seleccionada.
     * @since Module 1 0.5, december 2015
     */
    public void optionClicked(View option) {
        timer.cancel();
        option1.setClickable(false);
        option2.setClickable(false);
        option3.setClickable(false);
        pauseAllowed = true;
        canvas.setShouldRespond(false);
        if(tutorialTap != null && tutorialTaps != null) stopTaps(tutorialTaps, tutorialTap);
        hideClues();
        boolean gotRight;
        switch(option.getId()) {
            case R.id.card_option1 :
                gotRight = option1.isErroneous();
                break;
            case R.id.card_option2 :
                gotRight = option2.isErroneous();
                break;
            case R.id.card_option3 :
                gotRight = option3.isErroneous();
                break;
            default :
                gotRight = false;
                Log.e(GameActivity.LOGGER_ID, "An unexisting card option was selected...!?");
        }//actua dependiendo de la tarjeta seleccionada
        answered = gotRight ? Module1.CORRECT :  Module1.WRONG;
        normalTimer.pause();
        hardTimer.pause();
        adjustDifficulty = false;
        if(answered == Module1.CORRECT && shownQuestions < Module1.QUESTION_LIMIT) {
            score += ChatonGameActivity.QUESTION_VALUE
                    *(getDifficultyMultiplier() +Module1.LEVEL1_MULTIPLIER);
            correctQuestions++;
            // aquí estaba el initDialog..
            if(correctQuestions > ChatonGameActivity.CLUE_GET_THRESHOLD && clueAwarded == 0) {
                clueAwarded = 1;
                if (saveNormalProgress(ChatonGameActivity.LEVEL1)) {
                    clueAnimation();
                    initDialogAfterAnim(bubbleContainer, typewriterView, R.string.mod_clue_award,
                            character, true, false, GameActivity.DO_NOTHING, R.raw.mod_clue_award,
                            true, 0, true, null, true);
                } else {
                    if(soundFx == null) soundFx = new MusicHandler(this);
                    else if(soundFx.isInitialized() && soundFx.isPlaying()) soundFx.pause();
                    soundFx.load(R.raw.correct_bell, false);
                    if(voiceOn) soundFx.play();
                    initDialogAfterAnim(bubbleContainer, typewriterView, R.string.mod1_well_done,
                            character, true, false, Module1.HINT4, R.raw.mod1_well_done, true, 0,
                            true, null, true);
                }//si se obtuvo una pista completamente nueva
            } else {
                if(soundFx == null) soundFx = new MusicHandler(this);
                else if(soundFx.isInitialized() && soundFx.isPlaying()) soundFx.pause();
                soundFx.load(R.raw.correct_bell, false);
                if(voiceOn) soundFx.play();
                initDialogAfterAnim(bubbleContainer, typewriterView, R.string.mod1_well_done,
                        character, true, false, Module1.HINT4, R.raw.mod1_well_done, true, 0, true,
                        null, true);
            }//se le da al jugador la pista
        } else if(answered != Module1.CORRECT && errorsCounter +1 < ERROR_LIMIT){
            errorsCounter++;
            adjustDifficulty = true;
            if(soundFx == null) soundFx = new MusicHandler(this);
            else if(soundFx.isInitialized() && soundFx.isPlaying()) soundFx.pause();
            soundFx.load(R.raw.chaton_bell, false);
            if(voiceOn) soundFx.play();
            initDialogAfterAnim(bubbleContainer, typewriterView, R.string.mod1_error, character,
                    true, false, Module1.DO_NOTHING, R.raw.mod1_error, true, 0, true, null, true);
        }//actúa dependiendo el tipo de respuesta
        displayScore();
        if(shownQuestions >= Module1.QUESTION_LIMIT) {
            gameOver = true;
            clueAnimation();
            saveNormalProgress(ChatonGameActivity.LEVEL1);
            initDialogAfterAnim(bubbleContainer, typewriterView, R.string.mod_end, character, true,
                    false, Module1.END_SEQUENCE1, R.raw.mod_end, false, 0, true, null, true);
        } else if(errorsCounter >= ERROR_LIMIT) {
            gameOver = true;
            initDialogAfterAnim(bubbleContainer, typewriterView, R.string.mod_bad_end, character,
                    true, false, Module1.BAD_END, R.raw.mod_bad_end, false, 0, true, null, true);
        }//termina el nivel o muestra la siguiente tarjeta
        shownQuestions++;
        endQuestion();
    }//option1Clicked

    /**
     * Responde al toque sobre Poodle Ross.
     * @param ross - Referencia a Ross.
     * @since Module 1 0.5, december 2015
     */
    public void rossClicked(View ross) {
        if(gameOver) return;
        stopTaps(taps, toTap);
        stopTaps(drag, toDrag);
        if(tutorialTap != null && tutorialTaps != null) stopTaps(tutorialTaps, tutorialTap);
        if(showingCarad) {
            if(iDidnt.isPlaying()) return;
            iDidnt.rewind();
            if(voiceOn) iDidnt.play();
            canvas.deleteDraw();
            return;
        } if(pauseRequested) {
            return;
        }//si el juego debería estar pausado
        if(background.isClickable()) {
            animationSequence();
            return;
        }//si se debe hacer click sobre el fondo y no sobre ross
        pauseAllowed = false;
        poodleRoss.animate().cancel();
        poodleRoss.setScaleX(1);
        poodleRoss.setScaleY(1);
        createCard();
        appearClues();
        if(shownQuestions == 2 || shownQuestions == 3) {
            super.initDialogAfterAnim(bubbleContainer, typewriterView, R.string.mod1_hint2,
                    character, true, false, null, null, Module1.HINT3, R.raw.mod1_hint2, false,
                    Typewriter.TEXT_DELAY, Math.round(
                            GameActivity.ANIMATION_DURATION * getFloatResource(
                                    R.dimen.text_intro_delay)), 0, true, null);
        }// sólo muestra el mensaje de ayuda una sola vez
        enableSprites(false);
        answered = Module1.NO_ANSWER;
        canvas.deleteDraw();
        glower.glowRoss = false;
        activated = false;
        chatAnim = new ChatonAnimator(character.getX(), character.getY());
        chatAnim.run = true;
        chatAnim.start();
        super.animatorThreads.put(ChatonAnimator.class, chatAnim);
        scrollToParrot().start();
    }//rossClicked

    /**
     * Responde a un toque sobre una pista.
     * @param clue - La pista seleccionada.
     * @since Module1 3.0, october 2016
     */
    public void clueClicked(View clue) {
        enableSprites(false);
        if(tutorialTap != null && tutorialTaps != null) {
            stopTaps(tutorialTaps, tutorialTap);
            tutorialTap.setVisibility(View.GONE);
            tutorialTaps = null;
            tutorialTap = null;
        }//si es posible eliminar los taps
        correctQuestions++;
        shownQuestions++;
        activated = true;
        score += ChatonGameActivity.QUESTION_VALUE
                *(ChatonGameActivity.HARD_MULTIPLIER +ChatonGameActivity.LEVEL1_MULTIPLIER);
        displayScore();
        boolean newClue = saveSpecialProgress(ChatonGameActivity.LEVEL1);
        if(dialogInProgress) {
            if(newClue) {
                clueAnimation();
                displayDialog(R.string.mod_clue_award, bubbleContainer, taps, toTap,
                        R.raw.mod_clue_award);
            } else {
                soundFx.pause();
                soundFx.load(R.raw.hiden_clue, false);
                if(voiceOn) soundFx.play();
                displayDialog(R.string.mod1_clue, bubbleContainer, taps, toTap, R.raw.mod1_clue);
            }//muestra un mensaje dependiendo de si se encontró una pista nueva o no
        } else if(newClue) {
            clueAnimation();
            initDialogAfterAnim(bubbleContainer, typewriterView, R.string.mod_clue_award, character,
                    true, false, GameActivity.DO_NOTHING, R.raw.mod_clue_award, true, 0, true, null,
                    true);
        } else {
            soundFx.pause();
            soundFx.load(R.raw.hiden_clue, false);
            if(voiceOn) soundFx.play();
            initDialogAfterAnim(bubbleContainer, typewriterView, R.string.mod1_clue,
                    character, true, false, Module1.DO_NOTHING, R.raw.mod1_clue, true, 0, true,
                    null, true);
        }//reacciona adecuadamente
        endQuestion();
    }//clueClicked

    @Override
    protected void transitionControl(int next) {}

    // métodos auxiliares

    /**
     * Crea una tarjeta.
     * clasificadas por dificultad y debería usarse el progreso guargado para saber qué dificultad
     * se ha alcanzado y poder generar sólo tarjetas de esa dificultad. Por lo pronto, genera a
     * lo bruto cualquier tarjeta.
     * @since Module 1 0.5, december 2015
     */
    private void createCard() {
        String text1, text2, text3;
        option1.setText("");
        option2.setText("");
        option3.setText("");
        option1.setErroneous(false);
        option2.setErroneous(false);
        option3.setErroneous(false);
        float percentage = correctQuestions /(float) shownQuestions;
        if(percentage <= 0.5 && adjustDifficulty) {
            currentDifficulty = GameActivity.EASY;
        } else if(percentage > 0.5 && percentage <= 0.8) {
            currentDifficulty = GameActivity.NORMAL;
        } else if(percentage > 0.8) {
            currentDifficulty = GameActivity.HARD;
        }//decide cual es la deificultad actual
        hardHiddenClue = -1;
        switch(currentDifficulty) {
            case GameActivity.EASY :
                switch(cardSelector(Module1.EASY_CARDS_NUMBER)) {
                    case 1 :
                        text1 = getString(R.string.mod1_t2_ok1);
                        text2 = getString(R.string.mod1_t2_ok2);
                        text3 = getString(R.string.mod1_t2_err);
                        break;
                    case 2 :
                        text1 = getString(R.string.mod1_t14_ok1);
                        text2 = getString(R.string.mod1_t14_ok2);
                        text3 = getString(R.string.mod1_t14_err);
                        break;
                    case 3 :
                        text1 = getString(R.string.mod1_t13_ok1);
                        text2 = getString(R.string.mod1_t13_ok2);
                        text3 = getString(R.string.mod1_t13_err);
                        break;
                    case 4 :
                        text1 = getString(R.string.mod1_t9_ok1);
                        text2 = getString(R.string.mod1_t9_ok2);
                        text3 = getString(R.string.mod1_t9_err);
                        break;
                    case 5 :
                        text1 = getString(R.string.mod1_t3_ok1);
                        text2 = getString(R.string.mod1_t3_ok2);
                        text3 = getString(R.string.mod1_t3_err);
                        break;
                    case 6 :
                        text1 = getString(R.string.mod1_t8_ok1);
                        text2 = getString(R.string.mod1_t8_ok2);
                        text3 = getString(R.string.mod1_t8_err);
                        break;
                    case 7 :
                        text1 = getString(R.string.mod1_t4_ok1);
                        text2 = getString(R.string.mod1_t4_ok2);
                        text3 = getString(R.string.mod1_t4_err);
                        break;
                    case 8 :
                    default :
                        text1 = getString(R.string.mod1_t6_ok1);
                        text2 = getString(R.string.mod1_t6_ok2);
                        text3 = getString(R.string.mod1_t6_err);
                        break;
                }//asigna las tarjetas fáciles
                break;
            case GameActivity.NORMAL :
                switch(cardSelector(Module1.NORMAL_CARDS_NUMBER)) {
                    case 1 :
                        text1 = getString(R.string.mod1_t7_ok1);
                        text2 = getString(R.string.mod1_t7_ok2);
                        text3 = getString(R.string.mod1_t7_err);
                        break;
                    case 2 :
                        text1 = getString(R.string.mod1_t1_ok1);
                        text2 = getString(R.string.mod1_t1_ok2);
                        text3 = getString(R.string.mod1_t1_err);
                        break;
                    case 3 :
                        text1 = getString(R.string.mod1_t19_ok1);
                        text2 = getString(R.string.mod1_t19_ok2);
                        text3 = getString(R.string.mod1_t19_err);
                        break;
                    case 4 :
                        text1 = getString(R.string.mod1_t10_ok1);
                        text2 = getString(R.string.mod1_t10_ok2);
                        text3 = getString(R.string.mod1_t10_err);
                        break;
                    case 5 :
                        text1 = getString(R.string.mod1_t12_ok1);
                        text2 = getString(R.string.mod1_t12_ok2);
                        text3 = getString(R.string.mod1_t12_err);
                        break;
                    case 6 :
                        text1 = getString(R.string.mod1_t5_ok1);
                        text2 = getString(R.string.mod1_t5_ok2);
                        text3 = getString(R.string.mod1_t5_err);
                        break;
                    case 7 :
                        text1 = getString(R.string.mod1_t17_ok1);
                        text2 = getString(R.string.mod1_t17_ok2);
                        text3 = getString(R.string.mod1_t17_err);
                        break;
                    case 8 :
                        text1 = getString(R.string.mod1_t16_ok1);
                        text2 = getString(R.string.mod1_t16_ok2);
                        text3 = getString(R.string.mod1_t16_err);
                        break;
                    case 9 :
                        text1 = getString(R.string.mod1_t15_ok1);
                        text2 = getString(R.string.mod1_t15_ok2);
                        text3 = getString(R.string.mod1_t15_err);
                        break;
                    case 10 :
                        text1 = getString(R.string.mod1_t18_ok1);
                        text2 = getString(R.string.mod1_t18_ok2);
                        text3 = getString(R.string.mod1_t18_err);
                        break;
                    case 11 :
                        text1 = getString(R.string.mod1_t11_ok1);
                        text2 = getString(R.string.mod1_t11_ok2);
                        text3 = getString(R.string.mod1_t11_err);
                        break;
                    case 12 :
                    default :
                        text1 = getString(R.string.mod1_t20_ok1);
                        text2 = getString(R.string.mod1_t20_ok2);
                        text3 = getString(R.string.mod1_t20_err);
                }//asigna las tarjetas de dificultad media
                break;
            case GameActivity.HARD :
            default :
                switch(cardSelector(Module1.HARD_CARDS_NUMBER)) {
                    case 1 :
                        text3 = getString(R.string.mod1_t21_err);
                        break;
                    case 2 :
                        text3 = getString(R.string.mod1_t22_err);
                        hardHiddenClue = 1;
                        break;
                    case 3 :
                        text3 = getString(R.string.mod1_t23_err);
                        break;
                    case 4 :
                        text3 = getString(R.string.mod1_t24_err);
                        break;
                    case 5 :
                        text3 = getString(R.string.mod1_t25_err);
                        break;
                    case 6 :
                        text3 = getString(R.string.mod1_t26_err);
                        break;
                    case 7 :
                        text3 = getString(R.string.mod1_t27_err);
                        break;
                    case 8 :
                        text3 = getString(R.string.mod1_t28_err);
                        break;
                    case 9 :
                    default :
                        text3 = getString(R.string.mod1_t29_err);
                        hardHiddenClue = 1;
                }//aigna las tarjetas de dificultad alta
                int miscId = miscCardChooser(-1);
                text1 = getString(miscId);
                text2 = getString(miscCardChooser(miscId));
        }//asigna las tarjetas de acuerdo al grado de dificultad
        int assignedTo = randomizer.nextInt(Module1.OPTIONS_NUMBER) +1;
        switch (assignedTo) {
            case 1 :
                option1.setText(text1);
                break;
            case 2 :
                option2.setText(text1);
                break;
            case 3 :
            default :
                option3.setText(text1);
                break;
        }//ahora crea opción 1
        int newAssignment = randomizer.nextInt(Module1.OPTIONS_NUMBER) +1;
        if(newAssignment == assignedTo) {
            newAssignment = (newAssignment +1) %Module1.OPTIONS_NUMBER;
            if(newAssignment == 0) {
                newAssignment++;
            }//si se hace 0
        }//si escogió el mismo
        switch (newAssignment) {
            case 1 :
                option1.setText(text2);
                break;
            case 2 :
                option2.setText(text2);
                break;
            case 3 :
                option3.setText(text2);
                break;
        }//ahora crea opción 2
        if(option1.getText().equals("")) {
            if(lastAnsPos != 0) {
                option1.setText(text3);
                option1.setErroneous(true);
                lastAnsPos = 0;
            } else {
                option1.setText(option2.getText());
                option2.setText(text3);
                option2.setErroneous(true);
                lastAnsPos = 1;
            }//acomoda la frase donde sea adecuado
        } else if(option2.getText().equals("")) {
            if(lastAnsPos != 1) {
                option2.setText(text3);
                option2.setErroneous(true);
                lastAnsPos = 1;
            } else {
                option2.setText(option3.getText());
                option3.setText(text3);
                option3.setErroneous(true);
                lastAnsPos = 2;
            }//acomoda la frase donde sea adecuado
        } else if(option3.getText().equals("")) {
            if(lastAnsPos != 2) {
                option3.setText(text3);
                option3.setErroneous(true);
                lastAnsPos = 2;
            } else {
                option3.setText(option1.getText());
                option1.setText(text3);
                option1.setErroneous(true);
                lastAnsPos = 0;
            }//acomoda la frase donde sea adecuado
        }//finalmente crea la opción 3
        option1.setClickable(true);
        option2.setClickable(true);
        option3.setClickable(true);
        clueDetected = false;
    }//createCard

    /**
     * Realiza una especie de b&uacute;squeda binaria para encontrar tarjetas no utilizadas en el
     * m&oacute;dulo. El funcionamiento consiste en originalmente un sólo selector que intenta
     * adivinar una carta disponible para todo el rango de tarjetas. Cuando descubre que una ya
     * est&aacute; seleccionada, crea un nuevo selector y se dividen el rango omitiendo la
     * descubierta
     * @param currentDifficultyCardsNo - La configuraci&oacute;n de dificulad actual.
     * @return int - El identificador de la tarjeta seleccionada.
     * @see Module1#EASY_CARDS_NUMBER
     * @see Module1#NORMAL_CARDS_NUMBER
     * @see Module1#HARD_CARDS_NUMBER
     * @since Module 1 1.0, january 2016
     */
    private byte cardSelector(byte currentDifficultyCardsNo) {
        byte cardSelected;
        byte lowerLimit = 0;
        byte upperLimit = currentDifficultyCardsNo;
        byte repeats = 0;
        do {
            if((gameProgress &ChatonGameActivity.PROGRESS_LEVEL1_TUTORIAL) == 0) {
                // las siguientes tarjetas abren la cortina: T3, T8, T13
                switch (randomizer.nextInt(3)) {
                    case 0:
                        cardSelected = 5;
                        break;
                    case 1:
                        cardSelected = 6;
                        break;
                    default:
                    case 2:
                        cardSelected = 7;
                }//esoge la frase que habre la cortina
            } else {
                cardSelected = (byte) (randomizer.nextInt(upperLimit) +lowerLimit +1);
            }//si es la primer tarjeta, debe escoger alguna de las frases que abren la cortina
            if(cardShown[cardSelected]) {
                repeats++;
                if(repeats > currentDifficultyCardsNo) {
                    break;
                }//solo permite rngos sensato
                if(lowerLimit == cardSelected) {
                    upperLimit--;
                    lowerLimit++;
                    if(lowerLimit > upperLimit) {
                        lowerLimit--;
                        upperLimit++;
                    }//si el límite inferior se hace menor que 0
                } else if(upperLimit == cardSelected) {
                    upperLimit--;
                    if(lowerLimit > upperLimit) {
                        upperLimit++;
                    }//si el límite inferior se hace menor que 0
                }//si hay que reducir (imponer) límites
            }//si la tarjeta ya estaba seleccionada
        } while(cardShown[cardSelected]);
        cardShown[cardSelected] = true;
        this.currentCard = cardSelected;
        return cardSelected;
    }//cardSelector

    /**
     * Activa las pistas ocultas indicadas por la tarjeta seleccionada actualmente.
     * @param currentCard - El identificador de la tarjeta actual.
     * @since Module 1 3.0, novemeber 2016
     */
    private void detectHiddenClues(int currentCard) {
        switch (currentDifficulty) {
            case GameActivity.EASY :
                boolean tutorial = (gameProgress &ChatonGameActivity.PROGRESS_LEVEL1_TUTORIAL) == 0;
                if(tutorial) saveGame(ChatonGameActivity.PROGRESS_LEVEL1_TUTORIAL);
                switch(currentCard) {
                    case 5 : // T3 avión - TUTORIAL
                        window.changeFrame(R.drawable.window_f2_727x505);
                        plane.setVisibility(View.VISIBLE);
                        beto.setVisibility(View.VISIBLE);
                        plane.setActive(true);
                        if(tutorial) posicionTapsInDragScreen(tutorialTap, plane, 0,
                                upperFrame.getHeight());
                        glower.glowPlane = true;
                        plane.animate().start();
                        break;
                    case 6 : // T8 beto - TUTORIAL
                        window.changeFrame(R.drawable.window_f2_727x505);
                        plane.setVisibility(View.VISIBLE);
                        beto.setVisibility(View.VISIBLE);
                        beto.setActive(true);
                        if(tutorial) posicionTapsInDragScreen(tutorialTap, beto, 0, lowerFrameH);
                        glower.glowBeto = true;
                        beto.animate().start();
                        break;
                    case 7 : // T13 cortina - TUTORIAL
                        window.setActive(true);
                        plane.setVisibility(View.VISIBLE);
                        beto.setVisibility(View.VISIBLE);
                        window.changeFrame(R.drawable.window_f2_727x505);
                        if(tutorial) posicionTapsInDragScreen(tutorialTap, window, 0,
                                upperFrame.getHeight());
                        window.animate().start();
                }//detecta pistas ocultas en las tarjetas de dificultad fácil
                if(tutorialTaps != null && tutorialTap != null)
                    startTaps(tutorialTaps, tutorialTap, 0);
                break;
            case GameActivity.NORMAL :
                switch(currentCard) {
                    case 1 : // T7 fuera de la casa
                        window.changeFrame(R.drawable.window_f2_727x505);
                        plane.setVisibility(View.VISIBLE);
                        beto.setVisibility(View.VISIBLE);
                        window.setActive(true);
                        window.animate().start();
                        break;
                    case 2: // T1 oso
                        glower.glowBear = true;
                        bear.setActive(true);
                        bear.animate().start();
                        break;
                    case 4 : // T10 libro
                        glower.glowBook = true;
                        book.setActive(true);
                        book.animate().start();
                        break;
                    case 10 : // T18 beto
                        window.changeFrame(R.drawable.window_f2_727x505);
                        plane.setVisibility(View.VISIBLE);
                        beto.setVisibility(View.VISIBLE);
                        glower.glowBeto = true;
                        beto.setActive(true);
                        beto.animate().start();
                        break;
                    case 11 : // T11 bandera
                        glower.glowFlag = true;
                        flag.setActive(true);
                        flag.animate().start();
                        break;
                    case 12 : // T20 tv
                        glower.glowTv = true;
                        tv.animate().start();
                        tv.setActive(true);
                }//detecta pistas ocultas en las tarjetas de dificultad normal
            case GameActivity.HARD :
                switch (hardHiddenClue) {
                    case 1 :
                        window.changeFrame(R.drawable.window_f2_727x505);
                        plane.setVisibility(View.VISIBLE);
                        beto.setVisibility(View.VISIBLE);
                        window.setActive(true);
                        window.animate().start();
                        break;
                    case 2:
                        glower.glowBook = true;
                        book.setActive(true);
                        book.animate().start();
                        break;
                    case 3:
                        window.changeFrame(R.drawable.window_f2_727x505);
                        plane.setVisibility(View.VISIBLE);
                        beto.setVisibility(View.VISIBLE);
                        glower.glowBeto = true;
                        beto.setActive(true);
                        beto.animate().start();
                }//detecta pistas ocultas para las tarjetas difíciles
        }//detecta pistas ocultas de acuerdo a la dificultad
        clueDetected = true;
    }//detectHiddenClues

    /**
     * Escoge las fraces misceneas a mostrar con la tarjeta de dificultad alta actual.
     * @param lastId - El identificador de recursos de la &uacute;ltima frace seleccionada.
     * @return int - El identificador de recursos de la frace miscelanea a utilizar.
     * @since Module 1 3.0, november 2016
     */
    private int miscCardChooser(int lastId) {
        byte next;
        boolean repeated;
        int selected;
        do {
            next = (byte) randomizer.nextInt(Module1.HARD_PHRASES_NUMBER);
            switch(next) {
                case 0 :
                    selected = R.string.mod1_misc_ok1;
                    hardHiddenClue = 2;
                    break;
                case 1 :
                    selected = R.string.mod1_misc_ok2;
                    hardHiddenClue = 3;
                    break;
                case 2 :
                    selected = R.string.mod1_misc_ok3;
                    break;
                case 3 :
                    selected = R.string.mod1_misc_ok4;
                    break;
                case 4 :
                default:
                    selected = R.string.mod1_misc_ok5;
            }//devuelve la frace adecuada
            repeated = selected == lastId;
        } while(repeated);
        return selected;
    }//miscCardChooser

    /**
     * Habilita o deshabilita los sprites en escena.
     * @param enable - <tt>true</tt> si los sprites deber&aacute;n estar habilitados, <tt>false</tt>
     * en otro caso.
     * @since Module 1 3.0, ocotber 2016
     */
    private void enableSprites(boolean enable) {
        enableSprites(enable, !enable, bear, book, tv, flag, window, beto, plane);
        glower.glowBear = glower.glowBook = glower.glowTv = glower.glowFlag = glower.glowBeto
                = glower.glowPlane = enable;
        if(enable) {
            window.changeFrame(R.drawable.window_f2_727x505);
            plane.setVisibility(View.VISIBLE);
            beto.setVisibility(View.VISIBLE);
        } else {
            window.changeFrame(R.drawable.window_f1_727x505);
            plane.setVisibility(View.INVISIBLE);
            beto.setVisibility(View.INVISIBLE);
            glower.resetSprites();
        }//si debe abrir o cerrar las cortinas
    }//disableSprites

    /**
     * Aparece la libreta de pistas y las pistas.
     * @since Module 1 3.0, november 2016
     */
    private void appearClues() {
        canvas.setVisibility(View.VISIBLE);
        cardView.setVisibility(View.VISIBLE);
        canvas.animate().x(DISPLAY_METRICS.widthPixels -canvas.getWidth());
        canvas.animate().setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                canvas.setShouldRespond(true);
            }//onAnimationEnd
        });
        cardView.animate().x(absoluteBackground.getWidth() -DISPLAY_METRICS.widthPixels);
        option1.animate().alpha(1);
        option2.animate().alpha(1);
        option3.animate().alpha(1);
        cardView.animate().setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                option1.animate().setListener(option1AnimListener);
                option1.animate().start();
            }//onAnimationEnd
        });
        cardView.animate().start();
        canvas.setShouldRespond(true);
        canvas.animate().start();
        showingCarad = true;
    }//appearCluesNotebook

    /**
     * Esconde la libreta de pistas y las pistas.
     * @since Module 1 3.0, november 2016
     */
    private void hideClues() {
        canvas.animate().x(DISPLAY_METRICS.widthPixels);
        canvas.animate().setListener(null);
        cardView.animate().x(-cardView.getWidth());
        cardView.animate().setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                option1.setAlpha(0);
                option2.setAlpha(0);
                option3.setAlpha(0);
            }//onAnimationEnd
        });
        cardView.animate().start();
        canvas.animate().start();
    }//hideCluesNotebook

    /**
     * Termina con la pregunta actual.
     * @since Module 1 3.0, november 2016
     */
    private void endQuestion() {
        super.killChatonAnimThread();
        if(timer != null) timer.purge();
        showingCarad = false;
        option1.setText("");
        option2.setText("");
        option3.setText("");
        canvas.setShouldRespond(false);
        canvas.deleteDraw();
        character.changeFrame(R.drawable.chaton_f1_454x571);
        normalTimer.pause();
        hardTimer.pause();
        if(!gameOver) {
            poodleRoss.setClickable(true);
            poodleRoss.setActive(true);
            poodleRoss.animate().start();
            glower.glowRoss = true;
            pauseAllowed = true;
            activated = true;
            if ((gameProgress &ChatonGameActivity.PROGRESS_LEVEL1_TUTORIAL) == 0 &&
                    shownQuestions <= 2 && currentDifficulty == GameActivity.EASY) {
                AnimatorSet anim = this.getScrollAnimation(Math.round(window.getX() - 50),
                        Math.round(window.getY() + 50), GameActivity.ANIMATION_DURATION);
                anim.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        detectHiddenClues(currentCard);
                    }//onAnimationEnd
                });
                anim.start();
            } else if (!this.clueDetected) {
                detectHiddenClues(this.currentCard);
            }//prepara el juego para detectar pistas y enseñarle al jugador la mecánica
        }//si el juego no ha terminado
    }//endQuestion

    /**
     * Mueve la pantalla haciea el perico.
     * @return AnimatorSer - Conjunto de animaciones que agrupa las animaciones del scroll
     * horizontal y el vertical de las vistas subyacentes.
     * @see mx.nachintoch.vg.android.DragScreen
     * @since Module 1 3.0, november 2016
     */
    private AnimatorSet scrollToParrot() {
        return getScrollAnimation(absoluteBackground.getMeasuredWidth(),
                absoluteBackground.getMeasuredHeight() -poodleRoss.getMeasuredHeight() /2,
                GameActivity.ANIMATION_DURATION);
    }//scrollToParrot

    /**
     * Inicializa los sprites para adem&aacute;s de brillar, hacerse grandes y pequeños
     * ritmicamente.
     * @param view - Referencia a la vista del sprite.
     * @since Module 1 3.0, november 2016
     */
    private void initSprites(final SpriteView view) {
        view.animate().setDuration(GameActivity.ANIMATION_DURATION);
        view.animate().setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                if(view.getScaleX() > 1) {
                    view.animate().scaleX(0.875f);
                    view.animate().scaleY(0.875f);
                } else if(view.getScaleX() <= 1) {
                    view.animate().scaleX(1.125f);
                    view.animate().scaleY(1.125f);
                }//escala el sprite adecuadamente
            }//onAnimationEnd
        });
    }//initSprites

    @Override
    public void characterClicked(View v) {
        if(!pauseAllowed && canvas.shouldRespond() && !dialogInProgress) {
            character.setClickable(false);
            initDialogAfterAnim(bubbleContainer, typewriterView, R.string.mod1_hint3,
                    character, true, false, null, null, GameActivity.DO_NOTHING,
                    R.raw.mod1_hint3, false, Typewriter.TEXT_DELAY, Math.round(
                            GameActivity.ANIMATION_DURATION * getFloatResource(
                                    R.dimen.text_intro_delay)), 0, true, new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            character.setClickable(true);
                        }//onAnimationEnd
                    });
        } else {
            super.characterClicked(v);
        }//si debería responder
    }//characterClicked

    @Override
    protected void resumeGame() {}

    // clases anidadas

    // ene-16 feliz año wi!!! Seguiré haciendo lo mismo todos los días que el año pasado, wi!!!!
    // :(

    // oct-16 lol, como cambian las cosas de un año al otro... Que bueno que dejé de hacer esas
    // cosas...

    /**
     * Hace brillar los sprites que se le indiquen.
     * @author <a href="mailto:manuel_castillo_cc@ciencias.unam.mx" >Manuel "Nachintoch"
     * Castillo</a>
     * @version 1.0, november 2016
     * @since Module 1 3.0, november 2016
     */
    private class Glower extends AnimatorThread {

        // atributos de clase

        /**
         * Indican cuales de los sprites deben brillar.
         * @since Glower 1.0, november 2016
         */
        boolean glowRoss;
        boolean glowBear;
        boolean glowBook;
        boolean glowTv;
        boolean glowFlag;
        boolean glowBeto;
        boolean glowPlane;

        /**
         * Indica el estado de brillo del sprite. Si va en incremento o decremento.
         * @since Glower 1.0, november 2016
         */
        boolean rossState;
        boolean bearState;
        boolean bookState;
        boolean tvState;
        boolean flagState;
        boolean betoState;
        boolean planeState;

        /**
         * Referencia a las capas del sprite a animar.
         * @since Glower 1.0, november 2016
         */
        Drawable rossFrame;
        Drawable bearFrame;
        Drawable bookFrame;
        Drawable tvFrame;
        Drawable flagFrame;
        Drawable betoFrame;
        Drawable planeFrame;

        /**
         * Referencia al posteador de eventos en el hilo gr&aacute;fico.
         * @since Glower 1.0, november 2016
         */
        Poster poster;

        /**
         * Indica el nombre de este hilo.
         * @since Glower 1.0, november 2016
         */
        static final String THREAD_NAME = "mx.nachintoch.vg.android.LEVEL1$GLOWER";

        // métodos constructores

        Glower() {
            super(THREAD_NAME);
            rossFrame = ((LayerDrawable) poodleRoss.getDrawable())
                    .findDrawableByLayerId(R.id.parrot_f);
            bearFrame = ((LayerDrawable) bear.getDrawable()).findDrawableByLayerId(R.id.bear_f);
            bookFrame = ((LayerDrawable) book.getDrawable()).findDrawableByLayerId(R.id.book_f);
            tvFrame = ((LayerDrawable) tv.getDrawable()).findDrawableByLayerId(R.id.tv_f);
            flagFrame = ((LayerDrawable) flag.getDrawable()).findDrawableByLayerId(R.id.flag_f);
            betoFrame = ((LayerDrawable) beto.getDrawable()).findDrawableByLayerId(R.id.beto_f);
            planeFrame = ((LayerDrawable) plane.getDrawable()).findDrawableByLayerId(R.id.plane_f);
            poster = new Poster();
            run = true;
        }//constructor

        // métodos de implementación

        @Override
        public void run() {
            while(run) {
                if(pause) {
                    pause();
                    continue;
                }//si debe pausar el hilo
                rossState = glowRoss && glowSprite(rossFrame, rossState);
                bearState = glowBear && glowSprite(bearFrame, bearState);
                bookState = glowBook && glowSprite(bookFrame, bookState);
                tvState = glowTv && glowSprite(tvFrame, tvState);
                flagState = glowFlag && glowSprite(flagFrame, flagState);
                betoState = glowBeto && glowSprite(betoFrame, betoState);
                planeState = glowPlane && glowSprite(planeFrame, planeState);
                try {
                    Thread.sleep(ChatonGameActivity.ANIMATION_DELAY);
                } catch(InterruptedException e) {
                    Log.d(GameActivity.LOGGER_ID, "Exception when glowing", e);
                }//intenta dar un tiempo entre cuadros
            }//mientras deba ejecutarse
        }//run

        /**
         * Hace brillar al sprite indicado.
         * @param sprite - El sprite que se desea hacer brillar.
         * @param state - El estado del sprite que se est&aacute; haciendo brillar, para distinguir
         * si el brillo sube o baja.
         * @return boolean - El estado resultante del sprite.
         * @since Glower 1.0, november 2016
         */
        boolean glowSprite(Drawable sprite, boolean state) {
            if(sprite.getAlpha() == 0) {
                state = false;
            } else if(sprite.getAlpha() == 255) {
                state = true;
            }//cambia el estado de brillo
            poster.sprite = sprite;
            if(state) {
                poster.alpha = Math.round(sprite.getAlpha() -25.5f);
                if(poster.alpha < 0) {
                    poster.alpha = 0;
                }//evita que se pase de 0
            } else {
                poster.alpha = Math.round(sprite.getAlpha() +25.5f);
                if(poster.alpha > 255) {
                    poster.alpha = 255;
                }//evita que se pase de 255
            }//hace brillar al sprite
            handler.post(poster);
            return state;
        }//glowSprite

        /**
         * Resetea las capas de los sprites
         * @since Glower 1.1, march 2017
         */
        void resetSprites() {
            rossFrame.setAlpha(0);
            bearFrame.setAlpha(0);
            bookFrame.setAlpha(0);
            tvFrame.setAlpha(0);
            flagFrame.setAlpha(0);
            betoFrame.setAlpha(0);
            planeFrame.setAlpha(0);
        }//resetSprites

        /**
         * Se encarga de cambiar el alpha a los sprites.
         * @author <a href="mailto:manuel_castillo_cc@ciencias.unam.mx" >Manuel "Nachintoch"
         * Castillo</a>
         * @version 1.0, october 2016
         * @since Glower Thread 1.0, november 2016
         */
        private class Poster implements Runnable {

            // atributos de clase

            /**
             * Alpha del sprite que est&aacute; siendo animado.
             * @since Poster 1.0, november 2016
             */
            int alpha;

            /**
             * Referencia al sprite que est&aacute; siendo animado.
             * @since Poster 1.0, november 2016
             */
            Drawable sprite;

            // métodos de implementación

            @Override
            public void run() {
                sprite.setAlpha(alpha);
            }//run

        }//Poster Runnable

    }//Glower Thread

    /**
     * Tarea que se encarga de contar el tiempo y cambiar el &aacute;nimo de Chat&oacute;n.
     * @author <a href="mailto:manuel_castillo_cc@ciencias.unam.mx" >Manuel "Nachintoch"
     * Castillo</a>
     * @version 1.0, november 2016
     * @since Module 1 3.0, november 2016
     */
    private class TimeCounter extends TimerTask {

        /**
         * Referencia al publicador en el hilo gráfico.
         * @since Timer Counter 1.0, november 2016
         */
        final Poster POSTER = new Poster();

        /**
         * Referencia al nivel 1.
         * @since Time Counter 1.0, march 2017
         */
        Module1 parent;

        /**
         * Recuerda si ya se ha hecho el sonido de cambio del tiempo
         * @since Time Counter 1.0, march 2017
         */
        byte timerChange;

        @Override
        public void run() {
            time -= TIME_STEP;
            if(normalTimer != null && !normalTimer.isPlaying()) {
                if(voiceOn) normalTimer.play();
            }//reproduce el tiemporizador normal
            if((TIMEOUT *2/3) >= time && time > TIME_STEP) {
                POSTER.state = Poster.NERVOUS;
                handler.post(POSTER);
                if(timerChange == 0) {
                    timerChange = 1;
                    if(soundFx == null) soundFx = new MusicHandler(parent);
                    else if(soundFx.isInitialized() && soundFx.isPlaying()) soundFx.pause();
                    soundFx.load(R.raw.hurry_up, false);
                    if(voiceOn) soundFx.play();
                }//efectúa el sonido para presionar al jugador
            } if(time < TIME_STEP) {
                POSTER.state = Poster.CRISIS;
                handler.post(POSTER);
                if(timerChange == 1) {
                    timerChange = 2;
                    soundFx.pause();
                    soundFx.load(R.raw.hurry_up, false);
                    if(voiceOn) soundFx.play();
                    if(voiceOn) hardTimer.play();
                }//reproduce el sonido del segundo temporizador
            } if(time <= 0) {
                POSTER.state = Poster.TIMED_OUT;
                handler.post(POSTER);
                normalTimer.pause();
                hardTimer.pause();
                soundFx.pause();
                soundFx.load(R.raw.timeout_bell, false);
                if(voiceOn) soundFx.play();
                timer.cancel();
            }// cambia el estado de ánimo de chaton o finaliza la pregunta
        }//run

    }//Time Counter Task

    /**
     * Solicita el cambio del cuadro de chat&oacute;n.
     * @author <a href="mailto:manuel_castillo_cc@ciencias.unam.mx" >Manuel "Nachintoch" Castillo</a>
     * @version 1.0, november 2016
     * @since Module 1 3.0, november 2016
     */
    private class Poster implements Runnable {

        /**
         * Indica que estado de chat&oacute;n debe colocar.
         * @since Poster 1.0, november 2016
         */
        byte state;

        static final byte OK = 0;
        static final byte NERVOUS = 1;
        static final byte CRISIS = 2;
        static final byte TIMED_OUT = 3;

        @Override
        public void run() {
            switch(state) {
                case OK :
                    character.changeFrame(R.drawable.chaton_f1_454x571);
                    break;
                case NERVOUS :
                    character.changeFrame(R.drawable.chaton_f2_454x571);
                    break;
                case CRISIS :
                    character.changeFrame(R.drawable.chaton_f3_454x571);
                    break;
                case TIMED_OUT :
                    if(tutorialTap != null && tutorialTaps != null)
                        stopTaps(tutorialTaps, tutorialTap);
                    hideClues();
                    errorsCounter++;
                    if(shownQuestions >= Module1.QUESTION_LIMIT) {
                        gameOver = true;
                        clueAnimation();
                        saveNormalProgress(ChatonGameActivity.LEVEL1);
                        initDialogAfterAnim(bubbleContainer, typewriterView, R.string.mod_end, character, true,
                                false, Module1.END_SEQUENCE1, R.raw.mod_end, false, 0, true, null, true);
                    } else if(errorsCounter >= ERROR_LIMIT) {
                        gameOver = true;
                        initDialogAfterAnim(bubbleContainer, typewriterView, R.string.mod_bad_end, character,
                                true, false, Module1.BAD_END, R.raw.mod_bad_end, false, 0, true, null, true);
                    } else {
                        shownQuestions++;
                        adjustDifficulty = true;
                        initDialogAfterAnim(bubbleContainer, typewriterView, R.string.mod_timeout,
                                character, true, false, Module1.HINT4, R.raw.mod_timeout, true, 0,
                                true, null, true);
                        endQuestion();
                    }//termina el nivel o muestra la siguiente tarjeta
            }//depende del estado del poteador
            handler.removeCallbacks(this);
        }//run

    }//Poster Runnable

    /**
     * Pone en acci&oacute;n la animaci&oacute;n de la segunda opci&oacute;n de la tarjeta.
     * @author <a href="mailto:manuel_castillo_cc@ciencias.unam.mx" >Manuel "Nachintoch"
     * Castillo</a>
     * @version 1.0, november 2016
     * @since Module 1 3.0, november 2016
     */
    private class Option1AnimationListener extends AnimatorListenerAdapter {

        @Override
        public void onAnimationEnd(Animator animation) {
            option1.animate().setListener(null);
            option2.animate().setListener(option2AnimListener);
            option2.animate().start();
        }//onAnimationEnd

    }//Option 1 Animation Listener

    /**
     * Pone en acci&oacute;n la animaci&oacute;n de la tercera opci&oacute;n de la tarjeta.
     * @author <a href="mailto:manuel_castillo_cc@ciencias.unam.mx" >Manuel "Nachintoch"
     * Castillo</a>
     * @version 1.0, november 2016
     * @since Module 1 3.0, november 2016
     */
    private class Option2AnimationListener extends AnimatorListenerAdapter {

        @Override
        public void onAnimationEnd(Animator animation) {
            option2.animate().setListener(null);
            option3.animate().setListener(option3AnimListener);
            option3.animate().start();
        }//onAnimationEnd

    }//Option 2 Animator Listener

    /**
     * Pone en acci&oacute;n el temporizador de la pregunta.
     * @author <a href="mailto:manuel_castillo_cc@ciencias.unam.mx" >Manuel "Nachintoch"
     * Castillo</a>
     * @version 1.0, november 2016
     * @since Module 1 3.0, november 2016
     */
    private class Option3AnimationListener extends AnimatorListenerAdapter {

        /**
         * Referencia al nivel 1.
         * @since Time Counter 1.0, march 2017
         */
        Module1 parent;

        @Override
        public void onAnimationEnd(Animator animation) {
            time = Module1.TIMEOUT;
            timer = new Timer("mx.nachintoch.vg.chaton.Level_1_timer");
            try {
                if(timeCounter != null) timeCounter.cancel();
                timeCounter = new TimeCounter();
                timeCounter.parent = parent;
                timer.schedule(timeCounter, 0, TIME_STEP);
            } catch(IllegalStateException e) {
                Log.v(GameActivity.LOGGER_ID, "Exception when scheduling task", e);
            }//intenta dejar pendiente la activación del contador
            if((gameProgress &ChatonGameActivity.PROGRESS_LEVEL1_TUTORIAL) == 0) {
                posicionTaps(tutorialTap, option1.isErroneous() ? option1 :
                        option2.isErroneous() ? option2 : option3, 0, option3.getHeight() /2);
                startTaps(tutorialTaps, tutorialTap, 0);
            }//si no se ha mostrado el tutorial
            option3.animate().setListener(null);
        }//onAnimationEnd

    }//Option 3 Animation Listener

}//Module1
