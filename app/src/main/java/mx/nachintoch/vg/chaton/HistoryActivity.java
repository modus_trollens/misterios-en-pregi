package mx.nachintoch.vg.chaton;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import mx.nachintoch.vg.android.GameActivity;
import mx.nachintoch.vg.android.MusicHandler;

/**
 * Actividad que muestra la historia del juego.
 * @author <a href="mailto:manuel_castillo_cc@ciencias.unam.mx" >Manuel "Nachintoch" Castillo</a>
 * @version 0.5, december 2015
 * @since Misterios en Pregui 0.5, december 2015
 */
public class HistoryActivity extends ChatonGameActivity {

    // atributos de clase

    /**
     * Referencia al cuadro donde se muestra el ring 1.
     * @since History Activity 0.5, december 2015
     */
    private ImageView ring1;

    /**
     * Referencia al cuadro donde se muestra el ring 2.
     * @since History Activity 0.5, december 2015
     */
    private ImageView ring2;

    /**
     * Indica que deben ejecutarse los rings.
     * @since History Activity 0.5, december 2015
     */
    private byte run;

    /**
     * Referencia al animador del tel&eacute;fono.
     * @since History Activity 0.5, december 2015
     */
    private Ringer ringer;

    /**
     * Indica que continua a la secuencia de texto 2.
     * @since History Activity 0.5, december 2015
     */
    private static final byte TEXT2 = 1;

    /**
     * Indica que continua a la secuencia de texto 3
     * @since History Activity 0.5, december 2015
     */
    private static final byte TEXT3 = 2;

    /**
     * Indica que continua a la seciencia de texto 4
     * @since History Activity 0.5, december 2015
     */
    private static final byte TEXT4 = 3;

    /**
     * Indica que continua a la secuencia de desaparecer la oficina de chat&oacute;n y aparecer a
     * los testigos
     */
    private static final byte ANIMATION = 4;

    /**
     * Indica que debe terminar la secuencia.
     */
    private static final byte TERMINATE_SEQUENCE = 10;

    // métodos de implementación

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);
        run = -1;
        //prepareDialog(R.id.dialog_box);
        ring1 = (ImageView) findViewById(R.id.ring1);
        ring2 = (ImageView) findViewById(R.id.ring2);
        nextSequence = TEXT2;
        ringer = new Ringer();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //displayDialog(R.string.history_text1, R.drawable.chaton);
            }//run
        }, GameActivity.ANIMATION_DURATION);
    }//onCreate

    @Override
    protected void animationSequence() {
        beginPlayer.play();
        if(nextSequence == HistoryActivity.TERMINATE_SEQUENCE) {
            run = -1;
            handler.removeCallbacks(ringer);
            /*taps.stop();
            toTap.setVisibility(View.GONE);*/
            typewriterView.setVisibility(View.GONE);
            character.setVisibility(View.GONE);
            super.saveGame(ChatonGameActivity.PROGRESS_HISTORY);
            Intent intent = new Intent(this, Map.class);
            intent.putExtra(GameActivity.CURRENT_PARTY_ID, super.currentParty);
            intent.putExtra(GameActivity.GAME_PROGRESS_ID, super.gameProgress);
            startActivity(intent);
            loopPlayer.fadeRelease(MusicHandler.DEFAULT_DELAY);
            soundFx.fadeRelease(MusicHandler.DEFAULT_DELAY);
            finish();
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            return;
        }//si la secuencia debe teminar
        //terminateDialog(R.id.dialog_box);
        int animDuration;
        switch(nextSequence) {
            case HistoryActivity.TEXT2 :
                nextSequence = HistoryActivity.ANIMATION;
//                displayDialog(R.string.history_text2, R.drawable.chaton);
                break;
            case HistoryActivity.ANIMATION :
                animDuration = getResources().getInteger(android.R.integer.config_longAnimTime);
                super.viewTransition(findViewById(R.id.history), animDuration,
                        GameActivity.DO_NOTHING, GameActivity.TRANSITION_DISAPPEAR);
                super.viewTransition(findViewById(R.id.history2), animDuration, 0,
                        GameActivity.TRANSITION_APPEAR);
                break;
            case HistoryActivity.TEXT3 :
                nextSequence = HistoryActivity.TEXT4;
                //displayDialog(R.string.history_text3, R.drawable.laura); // TODO remove
                break;
            case HistoryActivity.TEXT4 :
                nextSequence = HistoryActivity.TERMINATE_SEQUENCE;
                //displayDialog(R.string.history_text4, R.drawable.chaton);
        }//progresa en las secuencias de la historia
    }//animationSquence

    @Override
    protected void transitionControl(int next) {
        nextSequence = HistoryActivity.TEXT3;
        animationSequence();
    }//transitionControl

    /**
     * M&eacute;todo invocado cuando se da click al bot&ocute;n invisible que hace sonar el
     * tel&eacute;fono.
     * @param phoneButton - El bot&oacute;n del tel&eacute;fono.
     * @since History Activity 1.0, january 2016
     */
    public void ringPhone(View phoneButton) {
        if(run != -1) {
            return;
        }//si no está listo
        run = 0;
        handler.post(ringer);
    }//ringPhone

    @Override
    protected void resumeGame() {}

    // clases anidadas

    /**
     * Intercambia los rings del tel&eacute;fono para indicar que suena.
     * @author <a href="mailto:manuel_castillo_cc@ciencias.unam.mx" >Manuel "Nachintich"
     * Castillo</a>
     * @version 1.0, december 2015
     * @since History Activity 0.5, december 2015
     */
    private class Ringer implements Runnable {

        // métodos de implementación

        @Override
        public void run() {
            if (run < 2) { // FIXME los códigos de run están harcodeados!
                run++;
                if (ring1.getVisibility() == View.VISIBLE) {
                    ring1.setVisibility(View.INVISIBLE);
                    ring2.setVisibility(View.VISIBLE);
                } else {
                    ring1.setVisibility(View.VISIBLE);
                    ring2.setVisibility(View.INVISIBLE);
                }//actúa dependiendo si ring1 o ring2 es visibl
                soundFx.play();
                handler.postDelayed(this, soundFx.getTrackLength());
            } else {
                handler.removeCallbacks(this);
                ring1.setVisibility(View.INVISIBLE);
                ring2.setVisibility(View.INVISIBLE);
                run = -1;
            }//mientras deba ejecutarse
        }//run

    }//Ringer

}//HistoryActivity
