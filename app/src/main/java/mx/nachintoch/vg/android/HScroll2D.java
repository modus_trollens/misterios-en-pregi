package mx.nachintoch.vg.android;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.HorizontalScrollView;

/**
 * Scroll horizontal personalizado para usar con pantallas que permiten el desplazamiento total en
 * 2D.
 * @author <a href="http://stackoverflow.com/users/451013/mahdi-hijazi" >Mahdi Hijazi</a>
 * @author <a href="mailto:contact@nachintoch.mx" >Manuel "Nachintoch" Castillo</a>
 * @version 1.0, october 2016
 * @since Nachintoch Android Library for Videogames 1.0, october 2016
 */
public class HScroll2D extends HorizontalScrollView {

    // métodos constructores

    /**
     * Construye un scroll horizontal.
     * @param context - El contexto de la aplicaci&oacute;n.
     * @param attrs - Conjunto de atributos.
     * @param defStyle - ?
     * @since HScroll2D 1.0, october 2016
     */
    public HScroll2D(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }//constructor con tdo

    /**
     * Construye un scroll horizontal.
     * @param context - El contexto de la aplicaci&oacute;n.
     * @param attrs - Conjunto de atributos.
     * @since HScroll2D 1.0, october 2016
     */
    public HScroll2D(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    /**
     * Construye un scroll horizontal.
     * @param context - El contexto de la aplicaci&oacute;n.
     * @since HScroll2D 1.0, october 2016
     */
    public HScroll2D(Context context) {
        super(context);
    }

    // métodos de implementación

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        return false;
    }

    /**
     * Devuelve una animaci&oacute;n sobre X de este scroll.
     * @param scrollTo - La posici&oacute;n en X a donde se quiere scrollear
     * @return ObjectAnimator - El animador de este scroll.
     * @since Vertical Scroll 2D 1.0, november 2016
     */
    public ObjectAnimator getAnimateScroll(int scrollTo) {
        return ObjectAnimator.ofInt(this, "scrollX", scrollTo);
    }//animateScroll

}//HScroll2D
