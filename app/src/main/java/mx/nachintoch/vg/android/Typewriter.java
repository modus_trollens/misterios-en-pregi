package mx.nachintoch.vg.android;

import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

/**
 * Muestra una animaci&oacute;n de texto que aparece caracter por caracter.
 * @author <a href="http://stackoverflow.com/users/246461/devunwired" >Devunwired</a>
 * @author <a href="mailto:contact@nachintoch.mx" >Manuel "Nachintoch" Castillo</a>
 * @version 2.0, october 2015
 * @since Misterios en Pregui 0.5, december 2015
 */
public class Typewriter extends CustomFontTextView {

    // atributos de clase

    /**
     * Texto a mostrar en el campo de texto.
     * @since Typewriter 1.0, july 2011
     */
    private CharSequence text;

    /**
     * &Uacute;ltimo &iacute;ndice mostrado del texto.
     * @since Typewriter 1.0, july 2011.
     */
    private int textIndex;

    /**
     * Retraso entre cada caracter a mostrar.
     * @since Typewriter 1.0, july 2011.
     */
    private long animationDelay;

    /**
     * Retraso especial para la acci&oacute;n terminal.
     * @since Typewriter 1.1, december 2015
     */
    private long finalDelay;

    /**
     * Referencia a la actividad que mantiene esta vista.
     * @since Typewriter 2.0, october 2016
     */
    private GameActivity activity;

    /**
     * Recuerda la vista que contiene el globo de texto.
     * @since Typewriter 2.0, october 2016
     */
    private View bubble;

    /**
     * Referencia al personaje que habla.
     * @since Typewriter 2.0, novbember 2016
     */
    private View character;

    /**
     * Referencica al animador del texto.
     * @since Typewriter 2.0, october 2016
     */
    private Animator animator;

    /**
     * Referencia a la animaci&oacute;n que invita al jugador a tocar la pantalla.
     * @since Typewriter 2.0, december 2016
     */
    private AnimationDrawable taps;

    /**
     * Referencia al contenedor de la animaci&oacute;n que invita al jugador a tocar la pantalla.
     * @since Typewriter 2.0, december 2016
     */
    private ImageView toTap;

    /**
     * Retraso por defecto del texto.
     * @since Typewriter 1.1, december 2015
     */
    public static final int TEXT_DELAY = 30;

    // métodos constructores

    /**
     * Crea el campo de texto animado.
     * @param context - El contexto de la aplicaci&oacute;n.
     * @since Typewriter 1.1, december 2015
     */
    public Typewriter(Context context) {
        super(context);
        this.animationDelay = Typewriter.TEXT_DELAY;
        this.finalDelay = Typewriter.TEXT_DELAY;
    }//constructor

    /**
     * Crea el campo de texto animado.
     * @param context - El contexto de la aplicaci&oacute;n.
     * @param attrs - Los aributos a aplicar al campo de texto.
     * @since Typewriter 1.1, december 2015
     */
    public Typewriter(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.animationDelay = Typewriter.TEXT_DELAY;
        this.finalDelay = Typewriter.TEXT_DELAY;
    }//constructor

    // métodos de modificación

    /**
     * Cambia el retraso de la animaci&oacute;n.
     * @param delay - El retraso de la animaci&oacute;n.
     * @since Typewriter 1.0, july 2011
     */
    public void setAnimationDelay(long delay) {
        this.animationDelay = delay;
    }//setAnimationDelay

    /**
     * Cambia el retraso de la aniamaci&oacute;n final.
     * @param delay - El retraso de la animaci&oacute;n final.
     * @since Typewriter 1.1, december 2015
     */
    public void setFinalDelay(long delay) {
        this.finalDelay = delay;
    }//setFinal Delay

    /**
     * Cambia la referencia de la actividad que mantiene esta vista de texto.
     * @param act - La actividad padre.
     * @since Typewriter 2.0, october 2016
     */
    public void setActivity(GameActivity act) {
        this.activity = act;
    }//setActivity

    /**
     * Asigna el identificador de la burbuja en la que est&aacute; el texto.
     * @param bubble - La burbuja en la que est&aacute; el texto.
     * @since Typewriter 2.0, october 2016
     */
    public void setBubble(View bubble) {
        this.bubble = bubble;
    }//setBubbleId

    /**
     * Asigna el personaje que est&aacute; hablando.
     * @param character - El personaje que habla.
     * @since Typewriter 2.0, october 2016
     */
    public void setCharacter(View character) {
        this.character = character;
    }//setCharacter

    /**
     * Sobreescribe el texto de la animaci&oacute;n actual, a menos de que no haya una animación que
     * sobreescribir.
     * @param text - El texto que se quiere mostrar de ahora en adelante.
     * @return boolean - Si pudo sustituir alguna animaci&oacute;n o no.
     * @since Typewriter 2.0, february 2017
     */
    public boolean overwriteText(CharSequence text) {
        this.text = text;
        textIndex = 0;
        animator.run = true;
        return true;
    }//overwriteText

    /**
     * Asigna la referncia a la vista animada que invita al jugador a tocar la pantalla.
     * @param taps - La vista animada.
     * @since Typewriter 2.0, december 2016
     */
    public void setTaps(AnimationDrawable taps) {
        this.taps = taps;
    }//setTaps

    /**
     * Asigna el contenedor de la animaci&oacute;n que invita al jugador a tocar la pantalla.
     * @param toTap - El contenedor de la animaci&oacute;n.
     * @since Typewriter 2.0, december 2016
     */
    public void setTapsContainer(ImageView toTap) {
        this.toTap = toTap;
    }//setTapsContainer

    // métodos de implementación

    /**
     * Realiza la animaci&oacute;n del texto dado.
     * @param toAnimate - El texto a animar.
     * @since Typewriter 1.0, july 2011
     */
    public void animateText(CharSequence toAnimate) {
        this.text = toAnimate;
        terminateDialog();
        animator = new Animator();
        animator.run = true;
        animator.start();
        //handler.postDelayed(animator, animationDelay);
    }//animateText

    /**
     * Termina el di&aacute;logo en curso.
     * @since Typewriter 2.0, ocotber 2016
     */
    public void terminateDialog() {
        if(animator != null) {
            animator.run = false;
            animator = null;
        }//si hay un animador que destruir
        this.textIndex = 0;
        post(new Runnable() {
            @Override
            public void run() {
                setText("");
            }//run
        });
    }//terminateDialog

    /**
     * brinca el diálogo abruptamente
     * @since Typewriter 2.0, decmeber 2016
     */
    public void jumpDialog() {
        textIndex = text.length() +5;
    }//jumpDialog

    // clases anidadas

    /**
     * Hilo que realiza la animaci&oacute;n de hacer aparecer cada caracter del texto, uno por uno
     * con el retraso dado.
     * @author <a href="mailto:manuel_castillo_cc@ciencias.unam.mx" >Manuel "Nachintoch"
     * Castillo</a>
     * @version 2.0, october 2016
     * @since Misterios en Pregui 0.5, december 2015
     */
    private class Animator extends Thread {

        // atributos de clase

        /**
         * Indica si debe seguirse ejecutando.
         * @since Animator 2.0, october 2016
         */
        boolean run;

        /**
         * Especifica el nombre del hilo.
         * @since Animator 2.0, october 2016
         */
        static final String THREAD_NAME = "mx.nachintoch.vg.android.TYPEWRITER$ANIMATOR";

        // métodos constructores

        Animator() {
            super(THREAD_NAME);
        }//constructor

        /**
         * Referencia al posteador de mensajes en el hilo gr&aacute;fico.
         * @since Animator 2.0, october 2016
         */
        Poster poster = new Poster();

        @Override
        public void run() {
            while (run) {
                if (textIndex + 1 > text.length()) {
                    try {
                        Thread.sleep(finalDelay);
                    } catch (InterruptedException e) {
                        Log.d(GameActivity.LOGGER_ID, "Exception in final typewriter delay", e);
                    }//retraso final para la actividad
                    if(run){
                        if (activity.nextSequence == GameActivity.DO_NOTHING) {
                            post(new Runnable() {
                                @Override
                                public void run() {
                                    activity.terminateDialog(bubble, character, taps, toTap);
                                }//run
                            });
                            run = false;
                            continue;
                        }//si terminó de mecanografear todo el texto
                        post(new Runnable() {
                            @Override
                            public void run() {
                                activity.animationSequence();
                            }//run
                        });
                        run = false;
                        continue;
                    }//si el hilo sigue activo
                }//si terminó de mecanografear este trozo de texto
                if(run) post(poster);
                try {
                    Thread.sleep(animationDelay);
                } catch (InterruptedException e) {
                    Log.d(GameActivity.LOGGER_ID, "Exception when sleeping typewriter", e);
                }// intenta dormir al hilo
            }//mientras deba ejecutarse
            run = false;
        }//run

    }//Animator Thread

    /**
     * Se encarga de poner el texto en la vista.
     * @author <a href="mailto:manuel_castillo_cc@ciencias.unam.mx" >Manuel "Nachintoch"
     * Castillo</a>
     * @version 1.0, october 2016
     * @since Typewriter 2.0, october 2016
     */
    private class Poster implements Runnable {

        @Override
        public void run() {
            try {
                setText(text.subSequence(0, ++textIndex));
            } catch(Exception e) {
                Log.d(GameActivity.LOGGER_ID, "Whoopsie", e);
            }//lol
        }//run

    }//Poster Runnable

}//Typewriter textview
