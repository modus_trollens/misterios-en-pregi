package mx.nachintoch.vg.android;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.graphics.Point;
import android.graphics.drawable.AnimationDrawable;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ImageView;

/**
 * Define una pantalla con la capacidad de realizar desplazamientos bidireccionales e incluso
 * diagonales; con todas sus respectivas combinaciones.
 * @author <a href="http://stackoverflow.com/users/451013/mahdi-hijazi" >Mahdi Hijazi</a>
 * @author <a href="mailto:contact@nachintoch.mx" >Manuel "Nachintoch" Castillo</a>
 * @version 1.0, october 2016
 * @since Nachintoch Android Library for Videogames 1.0, october 2016
 */
public abstract class DragScreen extends GameActivity {

    // atributos de clase

    /**
     * Recuerda donde ocurri&oacute; el &uacute;ltimo toque.
     * @since Drag Screen 1.0, october 2016
     */
    protected Point lastTouch = new Point();

    /**
     * Indica si la detecci&oacute;n de arrastre debe estar activada o no.
     * @since Drag Screen 1.0, october 2016
     */
    protected boolean activated = false;

    /**
     * Referencia a una vista de desplazamiento vertical.
     * @since Drag Screen 1.0, october 2016
     */
    protected VScroll2D vScroll;

    /**
     * Referencia a una vista de desplazamiento horizontal.
     * @since Drag Screen 1.0, october 2016
     */
    protected HScroll2D hScroll;

    /**
     * Indica si el desplazamiento est&aacute; sinedo suave para evitar teletransportaciones.
     * @since Drag Screen 1.0, ocotber 2016
     */
    protected boolean smoothie = false;

    /**
     * Animaci&oacute;n que indica arrastrar el dedo por la pantalla.
     * @since Drag Screen 1.0, february 2017
     */
    protected AnimationDrawable drag;

    /**
     * Contenedor de la im&aacute;gen que indica arrastrar el dedo.
     * @since Drag Screen 1.0, february 2017
     */
    protected ImageView toDrag;

    // mátodos de implementación

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        float curX, curY;
        switch(event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                lastTouch = new Point(Math.round(event.getX()), Math.round(event.getY()));
                smoothie = true;
                break;
            case MotionEvent.ACTION_MOVE:
                if(!activated) return super.onTouchEvent(event);
                if(!smoothie || vScroll == null || hScroll == null) {
                    break;
                }//si el desplazamiento es está siendo adecuado
                curX = event.getX();
                curY = event.getY();
                vScroll.setScrollY(vScroll.getScrollY() +lastTouch.y -Math.round(curY));
                hScroll.setScrollX(hScroll.getScrollX() +lastTouch.x -Math.round(curX));
                lastTouch.x = Math.round(curX);
                lastTouch.y = Math.round(curY);
                stopTaps(drag, toDrag);
                break;
            case MotionEvent.ACTION_UP :
                smoothie = false;
        }//responde al arrastre sobre la pantalla
        return super.onTouchEvent(event);
    }//onTouchEvent

    /**
     * Prepara una animaci&oacute;n de scroll.
     * @param scrollToX - La posici&oacute;n en X a la que se quiere hacer scroll.
     * @param scrollToY - La posici&oacute;n en Y a la que se quiere hacer scroll.
     * @param duration - La duraci&oacute;n de la animaci&oacute;n del scroll.
     * @return AnimatorSet - La animaci&oacute;n de movimiento lista para ejecutarse.
     * @since Drag Screen 1.0, november 2016
     */
    public AnimatorSet getScrollAnimation(int scrollToX, int scrollToY, long duration) {
        AnimatorSet animation = new AnimatorSet();
        animation.setDuration(duration);
        animation.playTogether(hScroll.getAnimateScroll(scrollToX),
                vScroll.getAnimateScroll(scrollToY));
        return animation;
    }//getScrollAnimation

    /**
     * Posiciona un elemento en referncia a otro tomando en cuenta el tama&ntilde;o del fondo.
     * @param taps - El elemento que se quiere posicionar.
     * @param reference - La vista cuya posici&oacute;n servir&aacute; de referencia para la
     * posici&oacute;n.
     * @param offsetX - Umbral de desplazamiento en X.
     * @param offsetY - Umbral de desplazamiento en Y.
     * @since Drag Screen 1.1, april 2017
     */
    protected void posicionTapsInDragScreen(View taps, View reference, int offsetX, int offsetY) {
        taps.setX(reference.getX() +(reference.getWidth() /2) -hScroll.getScrollX() -offsetX);
        taps.setY(reference.getY() +(reference.getHeight() /2) -vScroll.getScrollY() -offsetY);
    }//posicionTaps

    /**
     * Prepara una vista desplazable para mostrarla completa y luego realiza la tarea indicada.
     * @param backgnd - Referencia a la vista que representa el fondo absoluto del escenario.
     * @param todo - La tarea a realizar cuando se termine de animar el fondo.
     * @since Drag Screen 1.0, november 2016
     */
    protected void prepareCompleteDisplay(final View backgnd, final Runnable todo) {
        ViewTreeObserver vto = backgnd.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                backgnd.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                DisplayMetrics metrics = new DisplayMetrics();
                getWindowManager().getDefaultDisplay().getMetrics(metrics);
                backgnd.setScaleX(metrics.widthPixels /(backgnd.getMeasuredWidth() -300f));
                backgnd.setScaleY(metrics.heightPixels /(backgnd.getMeasuredHeight() -300f));
                backgnd.setX(-backgnd.getWidth() /4f);
                backgnd.setY(-backgnd.getHeight() /4f);
                if(todo != null) {
                    if(handler == null) {
                        handler = new Handler();
                    }//si no hay un gestor
                    handler.post(todo);
                }//si hay algo que hacer
            }//onGlobalLayout
        });
    }//prepareCompleteDisplay

    /**
     * Inicializa un dialogo.
     * @param bubble - Vista con la burbuja de texto.
     * @param textV - Referencia la vista de texto.
     * @param character - La vista que representa al personaje que est&aacute; hablando.
     * @param dialogId - Referencia num&eacute;rica al primer texto de la secuencia que desea
     * mostrarse en pantalla.
     * @param bOrientation - La orientaci&oacute;n del globo de texto.
     * @param tOrientation - La orietnaci&oacute;n del texto.
     * @param toTap - Referencia a la vista que contiene la animaci&oacute;n que invita al jugador
     * a tocar la pantalla.
     * @param taps - Referencia a la animaci&oacute;n que indica que el usuario debe tocar la
     * pantalla.
     * @param nextSec - La secuencia que debe animarse tras este primer texto. Si nada continua
     * puede usar GameActivity.DO_NOTHING
     * @param dialogTrackId - El identificador del recurso de la narraci&oacute;n del texto.
     * @param activateScroll - Si va a permitir el despazamiento 2D durante el texto o no.
     * @param textAnimLen - El tiempo que tardar&aacute; en colocar cada letra en la vista de texto.
     * @param finalTextAnimLen - El tiempo que permanecer&aacute; todo el texto visible en
     * pantalla.
     * @param animationTime - El tiempo que debe transcurrir para realizar la animación
     * @param animListener - Receptor de eventos de animaci&oacute;n.
     * @return byte - El estado del texto inicializado.
     * @since Drag Screen 1.0, november 2016
     */
    protected byte initDialog(TextBubbleContainer bubble, View textV, View character, int dialogId,
                              boolean bOrientation, boolean tOrientation, AnimationDrawable taps,
                              ImageView toTap, byte nextSec, int dialogTrackId,
                              boolean activateScroll, long textAnimLen, long finalTextAnimLen,
                              long animationTime, Animator.AnimatorListener animListener) {
        activated = activateScroll;
        return super.initDialog(bubble, textV, dialogId, character, bOrientation, tOrientation,
                taps, toTap, nextSec, dialogTrackId, textAnimLen, finalTextAnimLen, animationTime,
                animListener);
    }//Activa el scroll

    /**
     * Inicializa un di&aacute;logo esperando a que termine el &uacute;ltimo inicializado que no
     * permita inicializar el deseado.
     * @param bubble - Referencia a la burbuja de texto.
     * @param textV - Referencia a la vista de texto.
     * @param dialogId - Identificador del di&aacute;logo que se quiere mostrar.
     * @param character - Referencia al personaje que va a hablar
     * @param bOrientation - Orientaci&oacute;n del bocadillo.
     * @param tOrientation - Orientaci&oacute;n del texto.
     * @param taps - Animaci&oacute;n que invita al jugador a tocar la pantalla.
     * @param toTap - Referencia al contenedor de la animaci&oacute;n que invita al jugador a tocar
     * la pantalla.
     * @param nextSec - Siguiente secuencia a ejecutar.
     * @param dialogTrackId - Idenificador del di&aacute;logo a narrar.
     * @param activateScroll - Si debe activar el scroll
     * @param textAnimLen - Duraci&oacute;n de la animaci&oacute;n de cada caracter.
     * @param finalTextAnimLen - Duraci&oacute;n completa del bodadillo de texto.
     * @param animationTime - Tiempo que deber&aacute; ocurrir hasta que se ejecute la
     * animaci&oacute;n
     * @param overwrite - Indica si debe escribir en el globo actual o esperar a que termine y
     * mostrar uno nuevo.
     * @param animListener - Receptor de eventos de animaci&oacute;n.
     * @since Game Activity 2.0, october 2016
     */
    protected void initDialogAfterAnim(TextBubbleContainer bubble, View textV, int dialogId,
                                       View character, boolean bOrientation, boolean tOrientation,
                                       AnimationDrawable taps, ImageView toTap, byte nextSec,
                                       int dialogTrackId, boolean activateScroll, long textAnimLen,
                                       long finalTextAnimLen, long animationTime, boolean overwrite,
                                       Animator.AnimatorListener animListener) {
        super.initDialogAfterAnim(bubble, textV, dialogId, character, bOrientation, tOrientation,
                taps, toTap, nextSec, dialogTrackId, textAnimLen, finalTextAnimLen, animationTime,
                overwrite, animListener);
        activated = activateScroll;
    }//initDialogAfterAnim

}//Drag Screen Activity
