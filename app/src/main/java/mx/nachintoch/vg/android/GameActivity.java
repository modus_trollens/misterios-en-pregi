package mx.nachintoch.vg.android;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewPropertyAnimator;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Random;
import java.util.Timer;
import java.util.concurrent.ConcurrentHashMap;

import mx.nachintoch.vg.chaton.R;

/**
 * Implementa una actividad del juego. Para facilitar la tarea de hacer las actividades como
 * pantalla completa, s&oacute;lo hay que hacer que todas las actividades del juego extiendan a
 * esta.
 * @author <a href="mailto:contact@nachintoch.mx" >Manuel "Nachintoch" Castillo</a>
 * @version 2.0, october 2016
 * @since Nachintoch Android Library for Videogames 1.0, october 2016
 */
public abstract class GameActivity extends AppCompatActivity {

    // atributos de clase

    /**
     * Manejador de retrasos en el hilo gr&aacute;fico.
     * @since Game Activity 0.5, december 2015
     */
    protected Handler handler;

    /**
     * Referencia a un reproductor de m&uacute;sica. Este deber&iacute;a usarse para reproducir el
     * inicio de una pista antes de loop (en pistas en las que hay que dividir el inicio de la
     * m&uacute;sica del resto del loop).
     * @since Game Activity 0.5, december 2015
     */
    protected MusicHandler beginPlayer;

    /**
     * Referencia a un reproductor de m&uacute;sica. Este deber&iacute;a usarse para reproducir
     * el bucle de una pista tras el inicio (en pistas en las que hay que dividir el inicio de
     * la m&uacute;sica del resto del loop).
     * @since Game Activity 0.5, december 2015
     */
    protected MusicHandler loopPlayer;

    /**
     * Referencia a un reproductor de m&uacute;sica. Este deber&iacute;a usarse para reproducir
     * efectos de sonido. Solo es posible un sonido por reproductor, si en una escena de juego
     * es necesario reproducir varios efectos simult&aacute;neamente, deben declararse
     * expl&iacute;citamente el resto de los reproductores necesarios.
     * @since Game Activity 0.5, december 2015
     */
    protected MusicHandler soundFx;

    /**
     * Reproductor de m&uacute;sica dedicado reproducir una voz.
     * @since Game Activity 2.0, march 2017
     */
    protected MusicHandler voiceTrack;

    /**
     * Booleano que indica si debe poner a reproducir el reproductor de m&uacute;sica.
     * @since Game Activity 0.5, december 2015
     */
    protected boolean musicOn;

    /**
     * Booleano que indica si se debe poner a reproducir el reproductor de narraciones.
     * @since Game Activity 2.0, march 2017
     */
    protected boolean voiceOn;

    /**
     * Men&uacute; de pausa.
     * @since Game Activity 0.5, december 2015
     */
    protected AlertDialog pauseMenu;

    /**
     * Indica si se solicit&oacute; hacer pausa.
     * @since Game Activity 0.5, december 2015
     */
    protected boolean pauseRequested;

    /**
     * Referencia al cuadro donde aparece el personaje.
     * @since Game Activity 0.5, december 2015
     */
    protected SpriteView character;

    /**
     * Referencia a la vista que anima textos.
     * @since Game Activity 0.5, december 2015
     */
    protected Typewriter typewriterView;

    /**
     * Referencia a la vista padre.
     * @since Game Activity 0.5, december 2015
     */
    protected RelativeLayout background;

    /**
     * Cuenta en que secuencia va la animaci&oacute;n de inicio.
     * @since Game Activity 0.5, december 2015
     */
    protected byte nextSequence;

    /**
     * Indica si se permite pausar el juego.
     * @since Game Activity 0.5, december 2015
     */
    protected boolean pauseAllowed;

    /**
     * Indica el progreso de la partida actual.
     * @since Main Menu 0.5, december 2015
     */
    protected long gameProgress;

    /**
     * Recuerda que partida es esta.
     * @since Game Activity 1.0, december 2015
     */
    protected byte currentParty;

    /**
     * Indica si hay un di&aacute;logo en progreso.
     * @since Game Activity 2.0, october 2016
     */
    protected boolean dialogInProgress;

    /**
     * Indica si est&aacute; esperando a que el usuario realice alguna acci&oacute;n espec&iacute;fica.
     * @since Game Activity 2.0, october 2016
     */
    protected boolean waiting;

    /**
     * Contiene el puntaje del jugador.
     * @since Game Activity 2.0, october 2016
     */
    protected long score;

    /**
     * Temporizador de eventos.
     * @since Game Activity 2.0, november 2016
     */
    protected Timer timer;

    /**
     * Tiempo para realizar una actividad.
     * @since Game Activity 2.0, november 2016
     */
    protected long time;

    /**
     * Recuerda cu&aacute;l es el nivel de difiultad con el que se lleva a cabo est&aacute;
     * sesi&oacute;n.
     * @since Module 1 1.0, january 2016
     */
    protected byte currentDifficulty;

    /**
     * Recuerda si est&aacute; animando texto o no.
     * @since Game Activity 2.0, november 2016
     */
    protected boolean animatingText;

    /**
     * Generador de eventos aleatoreos.
     * @since Game Actiivty 2.0, march 2017
     */
    protected Random randomizer;

    /**
     * Recuerda si ya ha terminado el juego.
     * @since Module 1 3.0, november 2016
     */
    protected boolean gameOver;

    /**
     * Referencia a todos los hilos animadores. Deben registrarse cada vez que se quiera usar un
     * hilo. Esta limitado a solo 10 hilos por cuestiones de eficiencia.
     * @since Game Activity 2.0, october 2016
     */
    protected ConcurrentHashMap<Class<? extends AnimatorThread>, AnimatorThread> animatorThreads;

    /**
     * De manera similar al mapa con animadores para cuidar su parada, tambi&eacute;n hacemos lo
     * mismo con todos los posibles reproductores de sonido
     */
    protected ArrayList<MusicHandler> soundsHandlers;

    /**
     * Mantiene una referencia de las burbujas de texto que se est&aacute;n mostrando actualmente.
     * @since Game Activity 2.0, november 2016
     */
    protected SparseBooleanArray inflatedTextBubbles;

    /**
     * Contenedor de la animaci&oacute;n que invita al jugador a tocar la pantalla.
     * @since Chaton Game Activity 1.0, december 2016
     */
    protected ImageView toTap;

    /**
     * Referencia a la animaci&oacute;n que invita al usuario a tocar la pantalla.
     * @since Chaton Game Activity 1.0, december 2016
     */
    protected AnimationDrawable taps;

    /**
     * Indica si el juego ha sido pausado.
     * @since Game Activity 2.0 april 2017
     */
    protected boolean pause;

    /**
     * Tiempo de duraci&oacute;n para la mayor&iacute;a de la animaciones.
     * @since Game Activity 0.5, december 2015
     */
    protected static final int ANIMATION_DURATION = 2250;

    /**
     * Tiempo de dureci&oacute;n de la animaci&oacute;n del indicador de toque.
     * @since Game Activity 0.5, december 2015
     */
    protected static final int CLICK_INDICATOR_DURATION = 2000;
    protected static final int CLICK_INDICATOR_FLASH = 650;

    /**
     * Indicador de estado de actividad para se&ntilde;alar que se quiere terminar el juego.
     * @since Game Activity 0.5, december 2015
     */
    protected static final byte END_GAME_FINISH_STATUS = 10;

    /**
     * Indica que la partida es nueva.
     * @since Game Activity 1.0, december 2015
     */
    protected static final int PROGRESS_NONE = 0b00000000000000000;

    /**
     * Indica que un nivel no ha sido alcanzado.
     * @since Game Activity 1.0, december 2015
     */
    protected static final byte UNREACHED = 0;

    /**
     * Indica que un nivel ha sido alcanzado a dificultad f&aacute;cil.
     * @since Game Activity 1.0, december 2015
     */
    protected static final byte EASY = 1;

    /**
     * Indica que un nivel ha sido alcanzado a dificultad normal.
     * @since Game Activity 1.0, december 2015
     */
    protected static final byte NORMAL = 2;

    /**
     * Indica que un nivel ha sido alcanzado a dificultad dif&iacute;l.
     * @since Game Activity 1.0, december 2015
     */
    protected static final byte HARD = 3;

    /**
     * Identificador del estado de la partida actual.
     * @since Game Activity 0.5, december 2015
     */
    protected static final String GAME_PROGRESS_ID = "mx.nachintoch.vg.android.GAME_PROGRESS";

    /**
     * Identificador del estado persistente del indicador de reproducir m&uacute;sica.
     * @since Game Activity 0.5, december 2015
     */
    protected static final String SHOULD_PLAY_ID = "mx.nachintoch.vg.android.SHOULD_PLAY";

    /**
     * Identificador del estado persistente del indicador de pausa.
     * @since Game Activity 0.5, december 2015
     */
    protected static final String PAUSE_REQUEST_ID = "mx.nachintoch.vg.android.PAUSE_REQUESTED";

    /**
     * Identificador del estado persistente de permitir la pausa.
     * @since Game Activity 0.5, december 2015
     */
    protected static final String PAUSE_ALLOWED_ID = "mx.nachintoch.vg.android.PAUSE_ALLOWED_ID";

    /**
     * Identificador de la partida actual.
     * @since Game Activity 1.0, december 2015
     */
    protected static final String CURRENT_PARTY_ID = "mx.nachintoch.vg.android.CURRENT_PARTY_ID";

    /**
     * Identificador del puntaje general del jugador.
     * @since Game Activity 2.0, december 2016
     */
    protected static final String SCORE_ID = "mx.nachintoch.vg.android.SCORE_ID";

    /**
     * Identificador del registro de la aplicaci&oacute;n.
     * @since Game Activity 0.5, december 2015
     */
    public static final String LOGGER_ID = "mx.nachintoch.vg.LOGGER";

    /**
     * Indicador de escena inactiva.
     * @since Map 0.5, december 2015
     */
    protected static final byte DO_NOTHING = 120;

    /**
     * Indica que tras la escena, se debe indicar cómo listo el juego.
     * @since Map 0.5, december 2015
     */
    protected static final byte SET_READY = 121;

    /**
     * Indica que tras la escena, se debe finalizar el nivel.
     * @since Game Activity 1.0, january 2016
     */
    protected static final byte END_LEVEL = 122;

    /**
     * Indica que no se debe hacer nada.
     * @since Game Activity 2.0, october 2016
     */
    protected static final byte NOP = 123;

    /**
     * Indicador de s&oacute;lo hacer aparecer la vista dada.
     * @see #viewTransition(View, int, int, byte)
     */
    protected static final byte TRANSITION_APPEAR = 0;

    /**
     * Indicador de s&oacute;lo desaparecer la vista dada.
     * @see #viewTransition(View, int, int, byte)
     */
    protected static final byte TRANSITION_DISAPPEAR = 1;

    /**
     * Indicador de aparecer y luego desaparecer la vista dada.
     * @see #viewTransition(View, int, int, byte)
     */
    protected static final byte TRANSITION_COMPLETE = 2;

    /**
     * Tama&ntilde;o l&iacute;mite de la lista de hilos animadores y reproductores de sonido. No
     * deber&iacute;an usarse m&aacute;s de estos tantos para evitar problemas de eficiencia.
     * @since Game Activity 2.0, october 2016
     */
    public static final byte MAXIMUM_ANIMATOR_THREADS = 10;
    public static final byte MAXIMUM_SOUNDS_PLAYERS = 10;

    /**
     * Indicadores del estado del texto a inicialzar.
     * @since Game Activity 2.0, october 2016
     */
    public static final byte TEXT_INITIALIZED_OK = 0;
    public static final byte TEXT_INITIALIZED_FAILED = 1;
    public static final byte TEXT_INITIALIZED_UNKNOWN = 2;

    /**
     * Indica que un di&aacute;logo no va acompa&ntilde;ado de ninguna pista de voz.
     * @since Game Activity 2.0, march 2017
     */
    public static final byte NO_VOICE = 0;

    /**
     * Identificador gen&eacute;rico de un no-nivel.
     * @since Game Activity 2.0, march 2017
     */
    public static final byte NO_LEVEL = 0;

    // métodos de implementación

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        super.onCreate(savedInstanceState);
        gameProgress = getIntent().getLongExtra(GameActivity.GAME_PROGRESS_ID,
                GameActivity.PROGRESS_NONE);
        currentParty = getIntent().getByteExtra(GameActivity.CURRENT_PARTY_ID, (byte) 4);
        inflatedTextBubbles = new SparseBooleanArray();
        pauseAllowed = false;
        dialogInProgress = false;
        waiting = false;
    }//onCreate

    @Override
    protected void onPause() {
        pauseMusicHandlers();
        if(dialogInProgress && voiceTrack != null && voiceTrack.isInitialized() &&
                voiceTrack.isPlaying()) {
            voiceTrack.fadePause(MusicHandler.DEFAULT_DELAY);
        }//si hay que detener una narraci&oacute;n
        nextSequence = GameActivity.DO_NOTHING;
        super.onPause();
    }//onPause

    @Override
    protected void onResume() {
        super.onResume();
        if(loopPlayer != null && musicOn) {
            loopPlayer.fadePlay(MusicHandler.DEFAULT_DELAY);
        }//reanuda la música
        if(pauseRequested) {
            pauseGame();
        }//si debe pausar el juego
    }//onResume

    @Override
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putBoolean(GameActivity.PAUSE_REQUEST_ID, pauseRequested);
        bundle.putBoolean(GameActivity.SHOULD_PLAY_ID, musicOn);
        bundle.putBoolean(GameActivity.PAUSE_ALLOWED_ID, pauseAllowed);
        bundle.putByte(GameActivity.CURRENT_PARTY_ID, currentParty);
        bundle.putLong(GameActivity.GAME_PROGRESS_ID, gameProgress);
        bundle.putLong(GameActivity.SCORE_ID, score);
    }//onSaveInstanceState

    @Override
    public void onRestoreInstanceState(Bundle bundle) {
        super.onRestoreInstanceState(bundle);
        pauseRequested = bundle.containsKey(GameActivity.PAUSE_REQUEST_ID) &&
                bundle.getBoolean(GameActivity.PAUSE_REQUEST_ID);
        musicOn = bundle.containsKey(GameActivity.SHOULD_PLAY_ID) &&
                bundle.getBoolean(GameActivity.SHOULD_PLAY_ID);
        pauseAllowed = bundle.containsKey(GameActivity.PAUSE_ALLOWED_ID) &&
                bundle.getBoolean(GameActivity.PAUSE_ALLOWED_ID);
        currentParty = bundle.getByte(GameActivity.CURRENT_PARTY_ID, currentParty);
        gameProgress = bundle.getLong(GameActivity.GAME_PROGRESS_ID, gameProgress);
        score = bundle.getLong(GameActivity.SCORE_ID, score);
    }//onRestoreInstanceState

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(pauseAllowed && (keyCode == KeyEvent.KEYCODE_ESCAPE ||
                keyCode == KeyEvent.KEYCODE_BACK)) {
            pauseRequested = true;
            pauseGame();
        }//si se quiere pausar el juego
        return true;
    }//onKeyDown

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == GameActivity.END_GAME_FINISH_STATUS &&
                resultCode == GameActivity.END_GAME_FINISH_STATUS) {
            setResult(GameActivity.END_GAME_FINISH_STATUS);
            finish();
            return;
        }//si se solicitó terminar el juego, reacción en cadena hacia atrás
        super.onActivityResult(requestCode, resultCode, data);
    }//onActivityResult

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if(hasFocus) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION |
                    View.SYSTEM_UI_FLAG_FULLSCREEN | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        }//si tiene foco la aplicación
    }//onWindowFocusChanged

    @Override
    protected void onStart() {
        super.onStart();
        if(animatorThreads != null) {
            AnimatorThread anim;
            for(Class<? extends AnimatorThread> c : animatorThreads.keySet()) {
                anim = animatorThreads.get(c);
                if(anim != null && anim.run) {
                    continue;
                } try {
                    anim = c.newInstance();
                    anim.run = true;
                    animatorThreads.put(c, anim);
                    anim.start();
                } catch (InstantiationException | IllegalAccessException e) {
                    throw new RuntimeException(e);
                }//trata de crear los hilos animadores
            }//inicializa todos los hilos animadores
        }//si hay hilos animadores que crear

    }//onRestart

    @Override
    protected void onStop() {
        if(animatorThreads != null) {
            AnimatorThread anim;
            boolean retry;
            for(Class<? extends AnimatorThread> c : animatorThreads.keySet()) {
                anim = animatorThreads.remove(c);
                retry = true;
                while(retry) {
                    try {
                        anim.run = false;
                        anim.join(1);
                        retry = false;
                        anim = null;
                    } catch(InterruptedException e) {
                        Log.d(GameActivity.LOGGER_ID, "Exception when stoping animator thread", e);
                    }//intenta detener los hilos
                }//puede requerir más de un intento
            }//destruye todos los hilos animadores
        }//si hay hilos que instanciar
        if(soundsHandlers != null) {
            for(MusicHandler han : soundsHandlers) {
                if(han != null && !han.isFadingOut() && !han.isDisposed()) {
                    han.dispose();
                }//si puede/debe pausar el reproductor
            }//destruye todos los reproductores
        }//si hay reproductores en uso
        super.onStop();
    }//onStop

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        jumpText();
        if(taps != null && toTap != null) stopTaps(taps, toTap);
        return super.onTouchEvent(event);
    }//onTouchEven

    /**
     * Inicializa la lista de hilos animadores.
     * @since Game Activity 2.0, november 2016
     */
    protected final void initAnimatorThreadsReference() {
        if(animatorThreads == null) animatorThreads =
                new ConcurrentHashMap<>(GameActivity.MAXIMUM_ANIMATOR_THREADS);
    }//initThreadsReference

    /**
     * Inicializa la lista de reproductores de sonido.
     * @since Game Activity 2.0, march 2017
     */
    protected final void initSoundsHandlersReference() {
        if(soundsHandlers == null) soundsHandlers =
                new ArrayList<>(GameActivity.MAXIMUM_SOUNDS_PLAYERS);
        if(soundFx == null) soundFx = new MusicHandler(this);
        soundsHandlers.add(soundFx);
        if(loopPlayer != null) loopPlayer = new MusicHandler(this);
        soundsHandlers.add(loopPlayer);
        if(beginPlayer != null) beginPlayer = new MusicHandler(this);
        soundsHandlers.add(beginPlayer);
    }//initSoundsHandlersReference

    /**
     * Crea la animaci&oacute;n de indicio de toque de pantalla.
     * @return AnimationDrawable - La animación para indicar toque generada.
     * @since Game Activity 0.5, december 2015
     */
    protected AnimationDrawable createTaps(int ...resIds) {
        AnimationDrawable taps = new AnimationDrawable();
        for(int id : resIds) {
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                taps.addFrame(getResources().getDrawable(id, getTheme()), CLICK_INDICATOR_FLASH);
            } else {
                taps.addFrame(getResources().getDrawable(id), CLICK_INDICATOR_FLASH);
            }//carga dependiendo de la vesión
        }//recorre todos los sprite de la pata
        taps.setOneShot(false);
        return taps;
    }//createTaps

    /**
     * Inicia la animaci&oacute;n de tocar la pantalla o similar.
     * @param taps - Referencia a la animaci&oacute;n que indica tocar la pantalla.
     * @param toTap - La vista que contiene la im&aacute;gen que indica tocar la pantalla.
     * @param initDelay - El retraso inicial.
     * @since Game Activity 2.0, february 2017
     */
    protected void startTaps(final AnimationDrawable taps, final ImageView toTap, long initDelay) {
        if(initDelay > 0) {
            if(handler == null) handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        handler.removeCallbacks(this);
                        startTaps(taps, toTap, 0);
                    }//run
                }, initDelay);
            return;
        }//si hay un retraso
        toTap.setVisibility(View.VISIBLE);
        taps.start();
    }//startTaps

    /**
     * Detiene la animaci&oacute;n que indica tocar la pantalla.
     * @param taps - Referencia a la animaci&oacute;n que indica tocar la pantalla.
     * @param toTap - La vista que contiene la im&aacute;gen que indica tocar la pantalla.
     * @since Game Activity 2.0, febrary 2017
     */
    protected void stopTaps(AnimationDrawable taps, ImageView toTap) {
        toTap.setVisibility(View.INVISIBLE);
        taps.stop();
        taps.setVisible(false, true);
    }//stopTaps

    /**
     * Posiciona un elemento en referncia a otro.
     * @param taps - El elemento que se quiere posicionar.
     * @param reference - La vista cuya posici&oacute;n servir&aacute; de referencia para la
     * posici&oacute;n.
     * @param offsetX - Umbral de desplazamiento en X.
     * @param offsetY - Umbral de desplazamiento en Y.
     * @since Drag Screen 1.1, april 2017
     */
    protected void posicionTaps(View taps, View reference, int offsetX, int offsetY) {
        taps.setX(reference.getX() +(reference.getWidth() /2) -offsetX);
        taps.setY(reference.getY() +(reference.getHeight() /2) -offsetY);
    }//posicionTaps

    /**
     * Pausa el juego y muestra la pantalla de pausa con opciones de continuar y salir.
     * @since Game Activity 0.5, december 2015
     */
    protected void pauseGame() {
        if(pauseMenu == null) {
            pauseMenu = createPauseMenu();
        }//si el menú de pausa es nulo
        pauseMusicHandlers();
        pauseAnimatorThreads();
        pause = true;
        pauseMenu.show();
    }//pauseGame

    /**
     * Reanuda el juego.
     * @since Game Activity 2.0, april 2017
     */
    protected abstract void resumeGame();

    /**
     * Muestra un di&aacute;logo de un personaje.
     * @param dialogueId - El texto que va a decir el personaje.
     * @param dialogueContainer - Vista donde est&aacute; contenida la vista de texto.
     * @param taps - Referencia a la vista que anima la indicación de tocar la pantalla.
     * @param toTap - Referencia a la vista que contiene la animaci&oacute;n que indica tocar la
     * pantalla.
     * @param voiceTrackId - El identificador del recurso que representa la voz a reproducir.
     * @since Game Activity 0.5, december 2015
     */
    protected void displayDialog(int dialogueId, View dialogueContainer, AnimationDrawable taps,
                                 ImageView toTap, int voiceTrackId) {
        dialogueContainer.setVisibility(View.VISIBLE);
        typewriterView.animateText(getText(dialogueId));
        if(taps != null && toTap != null) {
            startTaps(taps, toTap, GameActivity.CLICK_INDICATOR_DURATION);
        }//si hay animación
        if(voiceTrackId != NO_VOICE && voiceOn) {
            if(voiceTrack == null) {
                voiceTrack = new MusicHandler(this);
                soundsHandlers.add(voiceTrack);
            }//si hay que reporducir un sonido
            else if(voiceTrack.isInitialized() && voiceTrack.isPlaying()) voiceTrack.pause();
            voiceTrack.load(voiceTrackId, false);
            voiceTrack.play();
        }//si hay que repdocucir algo
        dialogueContainer.setClickable(true);
        dialogInProgress = true;
    }//displayDialog

    /**
     * Termina un di&aacute;logo mostrado en pantalla.
     * @param dialogueContainer - Vista donde est&aacute; contenida la vista de texto.
     * @param character - Referencia a la vista que representa al personaje que habla.
     * @param taps - Referencia a la vista con la indicaci&oacute;n de tocar la pantalla.
     * @param toTap - Referencia al contenedor de la animaci&oacute;n que indica tocar la pantalla.
     * @since Game Activity 0.5, december 2015
     */
    protected void terminateDialog(View dialogueContainer, View character, AnimationDrawable taps,
                                   ImageView toTap) {
        endAnimateDialog(dialogueContainer, character);
        nextSequence = GameActivity.DO_NOTHING;
        if(taps != null && toTap != null) {
            stopTaps(taps, toTap);
        }// si hay animación
        typewriterView.terminateDialog();
        if(voiceTrack != null && voiceTrack.isInitialized() && voiceTrack.isPlaying())
            voiceTrack.pause();
        dialogInProgress = false;
        dialogueContainer.setClickable(false);
    }//terminateDialog

    /**
     * Prepara todos los elementos necesarios para mostrar un di&aacute;logo en pantalla.
     * @param bubble - Vista de la burbuja de texto donde va a
     * desplegarse el texto.
     * @param textV - Referencia a la vista del texto.
     * @param character - Referencia a la vista que representa al personaje que est&aacute;
     * hablando.
     * @param bOrientation - La orientaci&oacute;n de la burbuja de texto.
     * @param tOrientation - La orientaci&oacute;n del texto.
     * @param taps - Animaci&oacute;n que invita al jugador a tocar la pantalla.
     * @param toTap - Contenedor de la animaci&oacute;n para tocar la pantalla.
     * @param textAnimLen - Duraci&oacute;n en los intervalos de tiempo de animaci&ocute;n del
     * texto.
     * @param finalTextAnimLen - Duraci&oacute;n final para mostrar el &uacute;ltimo texto.
     * @param animListener - Receptor de eventos de animaci&oacute;n
     * @return boolean - <tt>true</tt> si puede iniciar el di&aacute;logo, <tt>false</tt> en otro
     * caso.
     * @since Game Activity 0.5, december 2015
     */
    protected boolean prepareDialog(TextBubbleContainer bubble, View textV, View character,
                                    boolean bOrientation, boolean tOrientation,
                                    AnimationDrawable taps, ImageView toTap, long textAnimLen,
                                    long finalTextAnimLen, Animator.AnimatorListener animListener) {
        if(!initAnimateDialog(bubble, textV, character, bOrientation, tOrientation, animListener)) {
            return false;
        }//si no puede iniciar el diálogo
        typewriterView.setAnimationDelay(textAnimLen);
        typewriterView.setFinalDelay(Math.round(finalTextAnimLen));
        typewriterView.setBubble(bubble);
        typewriterView.setCharacter(character);
        typewriterView.setTaps(taps);
        typewriterView.setTapsContainer(toTap);
        typewriterView.setActivity(this);
        return true;
    }//prepareDialog

    /**
     * Prepara una secuencia de diálogos para ser mostrados en pantalla.
     * @param bubble - Vista con la burbuja de texto.
     * @param textV - Referencia a la vista de texto.
     * @param dialogId - Referencia num&eacute;rica al primer texto de la secuencia que desea
     * mostrarse en pantalla.
     * @param character - Referencia a la vista que representa al personaje que va a hablar.
     * @param bOrientation - La orientaci&oacute;n de la burbuja de texto. Use <tt>true</tt> para
     * indicar que la burbuja debe ir del lado derecho del personaje o <tt>false</tt> para indicar
     * que debe ir del lado izquierdo.
     * @param tOrientation . La orientaci&ocute;n del texto. Use un valor diferente de
     * <tt>bOrientation</tt> pra indicar que el texto debe ir en sentido opuesto a la escala de la
     * burbuja de texto, o el mismo valor para que tengan la misma orientaci&oacute;n.
     * @param taps - La vista que indica que se debe tocar la pantalla.
     * @param toTap - El contenedor de la animaci&ocute;n que invita al jugador a tocar la pantalla.
     * @param nextSec - La secuencia que debe animarse tras este primer texto. Si nada continua
     * puede usar GameActivity.DO_NOTHING
     * @param dialogTackId - El identificador de la pista que narra el di&aacute;logo.
     * @param animationTime - El tiempo que debe transcurrir para realizar la animación
     * @param animListener - Receptor de eventos de animaci&oacute;n.
     * @return byte - C&oacute;digo de estado del di&aacute;logo.
     * @see GameActivity#DO_NOTHING
     * @see GameActivity#animationSequence()
     * @see GameActivity#prepareDialog(TextBubbleContainer, View, View, boolean, boolean, AnimationDrawable, ImageView, long, long, android.animation.Animator.AnimatorListener)
     * @see GameActivity#displayDialog(int, View, AnimationDrawable, ImageView, int)
     * @see GameActivity#terminateDialog(View, View, AnimationDrawable, ImageView)
     * @see #TEXT_INITIALIZED_OK
     * @see #TEXT_INITIALIZED_FAILED
     * @see #TEXT_INITIALIZED_UNKNOWN
     * @since Chaton Game Activity 1.0, october 2016
     */
    protected byte initDialog(final TextBubbleContainer bubble, final View textV,
                              final int dialogId, final View character, final boolean bOrientation,
                              final boolean tOrientation, final AnimationDrawable taps,
                              final ImageView toTap, byte nextSec, final int dialogTackId,
                              final long textAnimLen, final long finalTextAnimLen,
                              long animationTime, final Animator.AnimatorListener animListener) {
        nextSequence = nextSec;
        byte result;
        if(animationTime > 0) {
            result = GameActivity.TEXT_INITIALIZED_UNKNOWN;
            if(handler == null) handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    prepareDialog(bubble, textV, character, bOrientation, tOrientation, taps, toTap,
                            textAnimLen, finalTextAnimLen, animListener);
                }//run
            }, animationTime);
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    displayDialog(dialogId, bubble, taps, toTap, dialogTackId);
                }//run
            }, Math.round(animationTime *1.5));
        } else {
            result = prepareDialog(bubble, textV, character, bOrientation, tOrientation, taps,
                    toTap, textAnimLen, finalTextAnimLen, animListener) ?
                    GameActivity.TEXT_INITIALIZED_OK : GameActivity.TEXT_INITIALIZED_FAILED;
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    displayDialog(dialogId, bubble, taps, toTap, dialogTackId);
                }//run
            }, GameActivity.ANIMATION_DURATION /2);
        }//hace la diferencia entre un tiempo de espera y ninguna espera
        return result;
    }//initDialog

    /**
     * Inicializa un di&aacute;logo esperando a que termine el &uacute;ltimo inicializado que no
     * permita inicializar el deseado.
     * @param bubble - Referencia a la burbuja de texto.
     * @param textV - Referencia a la vista de texto.
     * @param dialogId - Identificador del di&aacute;logo que se quiere mostrar.
     * @param character - Referencia al personaje que va a hablar
     * @param bOrientation - Orientaci&oacute;n del bocadillo.
     * @param tOrientation - Orientaci&oacute;n del texto.
     * @param taps - La vista que anima la indicaci&oacute;n para tocar la pantalla.
     * @param toTap - El contenedor de la animaci&oacute;n que invita al jugador a tocar la
     * pantalla.
     * @param nextSec - Siguiente seciencia a ejecutar.
     * @param dialogTrackId - Identificador del recurso de sonido que representa la narraci&oacute;n
     * del di&aacute;logo.
     * @param textAnimLen - Duraci&oacute;n de la animaci&oacute;n de cada caracter.
     * @param finalTextAnimLen - Duraci&oacute;n completa del bodadillo de texto.
     * @param animationTime - Tiempo que deber&aacute; ocurrir hasta que se ejecute la
     * animaci&oacute;n
     * @param overwrite - Si debe sobreescribir en el globo de texto actual o esperar a que termine.
     * @param animListener - Receptor de eventos de animaci&oacute;n.
     * @since Game Activity 2.0, october 2016
     */
    protected void initDialogAfterAnim(TextBubbleContainer bubble, View textV, int dialogId,
                                       View character, boolean bOrientation, boolean tOrientation,
                                       AnimationDrawable taps, ImageView toTap, byte nextSec,
                                       int dialogTrackId, long textAnimLen, long finalTextAnimLen,
                                       long animationTime, boolean overwrite,
                                       Animator.AnimatorListener animListener) {
        Tryer tryer = new Tryer();
        tryer.bubble = bubble;
        tryer.textV = textV;
        tryer.dialogId = dialogId;
        tryer.character = character;
        tryer.bOrientation = bOrientation;
        tryer.tOrientation = tOrientation;
        tryer.nextSec = nextSec;
        tryer.textAnimLen = textAnimLen;
        tryer.finalTextAnimLen = finalTextAnimLen;
        tryer.animationTime = animationTime;
        tryer.animListener = animListener;
        tryer.taps = taps;
        tryer.toTap = toTap;
        tryer.overwrite = overwrite;
        tryer.dialogTrackId = dialogTrackId;
        if(overwrite) tryer.text = getText(dialogId);
        if(handler == null) handler = new Handler();
        handler.post(tryer);
    }//initDialogAfterAnim

    /**
     * Anima el di&aacute;logo antes de mostrarlo en pantalla.
     * @param textBubble - Referencia a la vista del globo de texto.
     * @param textView - Referencia a la vista de texto. Puede ser una referencia nula. Se usa para
     * invertir el texto independientemente del fondo en el que se coloque.
     * @param character - Referencia al personaje que va a hablar.
     * @param bOrientation - La orientaci&oacute;n de la burbuja de texto.
     * @param tOrientation - La otientación del texto, que puede ser independiente a la de la
     * burbuja.
     * @param animListener - Receptor de animaciones.
     * @return boolean - <tt>true</tt> si pudo iniciar el di&aacute;logo, <tt>false</tt> en otro
     * caso.
     * @since Chaton Game Activity - 1.0, october 2016
     */
    private boolean initAnimateDialog(TextBubbleContainer textBubble, View textView, View character,
                                     boolean bOrientation, boolean tOrientation,
                                      Animator.AnimatorListener animListener) {
        textBubble.animate().setDuration(GameActivity.ANIMATION_DURATION /2);
        textBubble.animate().setListener(animListener);
        if(inflatedTextBubbles.get(textBubble.getId())) {
            textBubble.animate().start();
            return false;
        }//si la burbuja ya ha sido inflada
        inflatedTextBubbles.append(textBubble.getId(), true);
        textBubble.resetScales();
        textBubble.setX(character.getX());
        textBubble.setY(character.getY());
        textBubble.animate().alpha(1);
        textBubble.animate().translationY(200);
        if(textView != null) {
            if((tOrientation && textView.getScaleX() < 0) ||
                    (!tOrientation && textView.getScaleX() >= 0)) {
                textView.setScaleX(-textView.getScaleX());
            }//invierte la orientación del texto
        } if(bOrientation) {
            textBubble.animate().x(character.getX() +(character.getMeasuredWidth() /2));
            if(textBubble.getScaleX() < 0) {
                textBubble.animate().scaleX(textBubble.getScaleX() *2);
            } else {
                textBubble.animate().scaleX(textBubble.getScaleX() *-2);
            } if(textBubble.getScaleY() < 0) {
                textBubble.animate().scaleY(textBubble.getScaleY() *-2);
            } else {
                textBubble.animate().scaleY(textBubble.getScaleY() *2);
            }//ajusta la escala de acuerdo a si está volteado o no
        } else {
            textBubble.animate().x(character.getX() -textBubble.getMeasuredWidth());
            if(textBubble.getScaleX() < 0) {
                textBubble.animate().scaleX(textBubble.getScaleX() *-2);
            } else {
                textBubble.animate().scaleX(textBubble.getScaleX() *2);
            } if(textBubble.getScaleY() < 0) {
                textBubble.animate().scaleY(textBubble.getScaleY() *-2);
            } else {
                textBubble.animate().scaleY(textBubble.getScaleY() *2);
            }//ajusta la escala de acuerdo a si está volteado o no
        }//anima de acuerdo a la orientación
        textBubble.animate().start();
        return true;
    }//animateDialog

    /**
     * Anima el di&aacute;logo despu&eacute;s de mostrarlo.
     * @param textBubble - Referencia a la vista del globo de texto.
     * @param character - Referencia al personaje que est&aacute; hablando.
     * @since Chaton Game Activity - 1.0, october 2016
     */
    private void endAnimateDialog(final View textBubble, View character) {
        if(animatingText || textBubble.getAlpha() == 0) {
            return;
        }//revisa si se está animando el texto
        animatingText = true;
        textBubble.animate().setDuration(GameActivity.ANIMATION_DURATION /2);
        textBubble.animate().alpha(0);
        textBubble.animate().x(character.getX());
        textBubble.animate().y(character.getY() +character.getMeasuredHeight());
        textBubble.animate().scaleY(textBubble.getScaleX() /2);
        textBubble.animate().scaleX(textBubble.getScaleY() /2);
        textBubble.animate().setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                animatingText = false;
                inflatedTextBubbles.append(textBubble.getId(), false);
            }//onAnimationEnd
        });
        textBubble.animate().start();
    }//endAnimateDialog

    /**
     * Pausa los hilos animadores.
     * @since Game Activity 2.0, february 2017
     */
    protected void pauseAnimatorThreads() {
        for(AnimatorThread animator: animatorThreads.values()) {
            animator.pause = true;
        }//pausa TODOS los animadores
    }//pauseAnimatorThreads

    /**
     * Pausa los reproductores de música.
     * @since Game Activity 2.0, february 2017
     */
    protected void pauseMusicHandlers() {
        if(soundsHandlers != null) {
            for(MusicHandler han : soundsHandlers) {
                if(han != null && !han.isFadingOut() && !han.isDisposed()) {
                    han.fadePause(MusicHandler.DEFAULT_DELAY);
                }//si puede/debe pausar el reproductor
            }//pausa todos los reproductores
        }//si hay sonidos que podemos detener
    }//pauseMusicHandlers

    /**
     * Reanuda los reproductores de sonido seleccionados.
     * @param hans - Los reproductores de sonido a reanudar.
     * @since Game Activity 2.0, march 2017
     */
    protected void resumeMusicHandlers(MusicHandler ...hans) {
        for(MusicHandler han : hans) {
            han.fadePlay(MusicHandler.DEFAULT_DELAY);
        }//reanuda la reproducción de los reproductores deseados
    }//resumeMusicHandlers

    /**
     * Reanuda los hilos animadores
     * @since Game Activity 2.0, february 2017
     */
    protected void resumeAnimatorThreads() {
        for(AnimatorThread animator: animatorThreads.values()) {
            animator.pause = false;
            animator.interrupt();
        }//pausa TODOS los animadores
    }//resumeAnimatorThreads

    /**
     * Controla el flujo de las animaciones.
     * @since Game Activity 0.5, december 2015
     */
    protected abstract void animationSequence();

    /**
     * Crea el men&uacute; de pausa.
     * @return AlertDialog - El men&uacute; de pausa.
     * @since Game Activity 0.5, december 2015
     */
    protected abstract AlertDialog createPauseMenu();

    /**
     * Realiza la transici&oacute;n entre dos vistas. Si se pasa nula la segunda
     * @param view - La vista que quiere hacerse aparecer y/o luego desaparecer.
     * @param duration - Duraci&oacute;n deseada de la transici&oacute;n.
     * @param nextSequence - La siguiente escena que se desea hacer aparecer.
     * @param effect - Indica que efecto es el que quiere conseguirse.
     * @see GameActivity#TRANSITION_APPEAR
     * @see GameActivity#TRANSITION_DISAPPEAR
     * @see GameActivity#TRANSITION_COMPLETE
     * @since Game Activity 1.0, january 2016
     */
    protected void viewTransition(final View view, final int duration, final int nextSequence,
                                  final byte effect) {
        float alpha = 0f;
        switch (effect) {
            case GameActivity.TRANSITION_APPEAR :
                alpha = 1f;
            case GameActivity.TRANSITION_DISAPPEAR :
                view.animate().alpha(alpha).setDuration(duration).setListener(
                        new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                if(effect == TRANSITION_DISAPPEAR) {
                                    view.setVisibility(View.GONE);
                                }//lol manejo de memoria
                                if(nextSequence != GameActivity.DO_NOTHING) {
                                    transitionControl(nextSequence);
                                }//si tiene que hacer algo
                            }//onAnimationEnd
                        });
                break;
            case GameActivity.TRANSITION_COMPLETE :
                view.animate().alpha(1f).setDuration(duration).setListener(
                        new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        view.animate().alpha(0f).setDuration(duration)
                                                .setListener(new AnimatorListenerAdapter() {
                                                    @Override
                                                    public void onAnimationEnd(Animator animation) {
                                                        view.setVisibility(View.GONE);
                                                        if(nextSequence != GameActivity.DO_NOTHING) {
                                                            transitionControl(nextSequence);
                                                        }//si tiene que hacer algo
                                                    }//onAnimationEnd
                                                });
                                    }//run
                                }, duration);
                            }//onAnimationEnd
                        });
        }//realiza la transición solicitada
    }//viewTransition

    /**
     * Cambia imagenes en animaci&ocute;n de escenas mediante una transici&oacute;n.
     * @see #viewTransition(View, int, int, byte)
     * @since Main Menu 0.5
     */
    protected abstract void transitionControl(int nextSequence);

    /**
     * Realiza una animaci&oacute;n de traslaci&oacute;n.
     * @param toAnim - La vista a animar.
     * @param x - La posici&oacute;n en X final de la vista a animar.
     * @param y - La posici&oacute;n en Y final de la vista a animar.
     * @param todo - Acci&oacute;n a ejecutar. Si es nulo, se ignora.
     * @since Game Activity 2.0, october 2016
     */
    protected void translationAnim(View toAnim, float x, float y, Animator.AnimatorListener todo) {
        toAnim.animate().translationX(x);
        toAnim.animate().translationY(y);
        toAnim.animate().setDuration(GameActivity.ANIMATION_DURATION);
        if(todo != null) {
            toAnim.animate().setListener(todo);
        }//si hay alguna acción a realizar
        toAnim.animate().start();
    }//translationAnim

    /**
     * Devuelve el valor del recurso flotante con el identificador (nombre) dado.
     * @param dimenResName - El identificador del recurso tipo flotante deseado.
     * @return float - El valor del recurso dedeado.
     * @since GameActivity 2.0, october 2016
     */
    protected float getFloatResource(int dimenResName) {
        TypedValue tv = new TypedValue();
        getResources().getValue(dimenResName, tv, true);
        return tv.getFloat();
    }//getFloatValue

    /**
     * Activa o desactiva los sprites dados.
     * @param enable - El estado de los sprites.
     * @param reset - Si se desea resetear los sprites
     * @param sprites - Los sprites a activar/desactivar.
     * @since Game Activity 2.0, november 2016.
     */
    protected void enableSprites(boolean enable, boolean reset, SpriteView ...sprites) {
        int i;
        Drawable d;
        for(SpriteView sprite : sprites) {
            sprite.setActive(enable);
            if(reset) {
                if((d = sprite.FRAMES.get(0)) != null) d.setAlpha(1);
                for(i = 1; i < sprite.FRAMES.size(); i++) {
                    if((d = sprite.FRAMES.get(i)) != null) d.setAlpha(0);
                }//resetea todos los frames posibles
            }//si se desea resetear a los sprites
            sprite.animate().cancel();
            sprite.setScaleY(1);
            sprite.setScaleX(1);
        }//activa/desactiva los sprites
    }//enableSprites

    /**
     * Actualiza el puntaje en pantalla.
     * @param scoreView - Referencia a la vista de texto donde se muestra el puntaje.
     * @param scoreStringId - Identificador del recurso cadena que contiene el texto para el puntaje
     * @since Chaton Game Activity 1.0, november 2016
     */
    protected void displayScore(TextView scoreView, int scoreStringId) {
        if(scoreView == null) return;
        String score = String.valueOf(this.score);
        while(score.length() < 8) {
            score = "0" +score;
        }//rellena con 0 mientras sea necesario
        scoreView.setText(getString(scoreStringId) +" " +score);
    }//displayScore

    /**
     * Anima la vista que presenta la pantalla.
     * @param fadeScreen - Referencia a la pantalla de desvanecimiento.
     * @return ViewPropertyAnimator - Referencia al animador de la <tt>fadeScreen</tt> dada,
     * ya listo para realizar la animaci&oacute;n de fundido, requiere una llamada a start()
     * pero puede usarse la referencia para agregar otros efectos a la animaci&oacute;n.
     * @since Chaton Game Activity 3.0, october 2016
     */
    protected ViewPropertyAnimator fadeInAnim(View fadeScreen) {
        float dimen = getResources().getDimension(R.dimen.fade_screen_offset);
        if(fadeScreen.getX() != dimen || fadeScreen.getScaleY() != 1.5) {
            fadeScreen.setScaleX(1.75f);
            fadeScreen.setScaleY(1.75f);
            fadeScreen.setX(dimen);
            fadeScreen.setY(dimen);
        }//si es necesario ajustar la pantalla
        fadeScreen.setVisibility(View.VISIBLE);
        fadeScreen.setAlpha(1f);
        fadeScreen.animate().alpha(0);
        return fadeScreen.animate().setDuration(GameActivity.ANIMATION_DURATION);
    }//fadeInAnim

    protected ViewPropertyAnimator fadeOutAnim(View fadeScreen) {
        fadeScreen.setAlpha(0f);
        fadeScreen.setVisibility(View.VISIBLE);
        fadeScreen.animate().alpha(1);
        return fadeScreen.animate().setDuration(GameActivity.ANIMATION_DURATION);
    }//fadeOutAnim

    /**
     * Responde a toques sobre un bocadillo para saltar el texto.
     * @param textB - Referencia a la vista tocada.
     * @since Game Activity 2.0, march 2017
     */
    public void textBubbleClicked(View textB) {
        jumpText();
    }//textBubbleClicked

    /**
     * Salta un texto en progreso.
     * @since Game Activity 2.0, march 2017
     */
    protected void jumpText() {
        if(dialogInProgress && !animatingText) {
            animationSequence();
        }//ejecuta la siguiente escena
    }//jumpText

    // clases anidadas

    /**
     * Define un hilo animador.
     * @author <a href="mailto:contact@nachintoch.mx" >Manuel "Nachintoch" Castillo</a>
     * @version 1.0, november 2016
     * @since Game Activity 2.0, november 2016
     */
    protected class AnimatorThread extends Thread {

        // atributos de clase

        /**
         * Indica si debe seguirse ejecutando.
         * @since Animator Thread 1.0, november 2016
         */
        public boolean run;

        /**
         * Indica si debe pausar su ejecuci&oacute;n.
         * @since animator Thread 1.0, february 2017
         */
        public boolean pause;

        // métodos constructores

        /**
         * Construye un hilo animador por omisi&oacute;n.
         * @since Animator Thread 1.0, november 2016
         */
        protected AnimatorThread() {
            super();
        }//constructor por omisión

        /**
         * Construye un hilo animador nombrado.
         * @param threadName - Nombre del hilo.
         * @since Animator Thread 1.0, november 2016
         */
        protected AnimatorThread(String threadName) {
            super(threadName);
        }//constructor nombrado

        /**
         * Construye un hilo animador nombrado y con grupo.
         * @param group - El grupo del hilo.
         * @param name - El nombre del hilo.
         */
        protected AnimatorThread(ThreadGroup group, String name) {
            super(group, name);
        }//consructor nombrado y con grupo

        /**
         * Pausa el hilo animador
         * @since Animator Thread 1.0, february 2017
         */
        protected void pause() {
            try {
                Thread.sleep(Integer.MAX_VALUE);
            } catch(InterruptedException e) {}//pausa
        }//pause

    }//Animator Thread

    /**
     * Intenta postear un texto, esperando a que el anterior termine de ser animado.
     * @author <a href="mailto:manuel_castillo_cc@ciencias.unam.mx" >Manuel "Nachintoch"
     * Castillo</a>
     * @version 1.0, november 2016
     * @since Game Activity 2.0, november 2016
     */
    protected class Tryer implements Runnable {

        /**
         * Referencia al contenedor de la vista de texto.
         * @since Tryer 1.0, november 2016
         */
        TextBubbleContainer bubble;

        /**
         * Referencia a la vista de texto. Puede ser nula.
         * @since Tryer 1.0, november 2016
         */
        View textV;

        /**
         * Identificador del recurso del di&aacute;logo que deber&aacute; mostrarse.
         * @since Tryer 1.0, november 2016
         */
        int dialogId;

        /**
         * Referencia al personaje que deber&aacute; hablar.
         * @since Tryer 1.0, november 2016
         */
        View character;

        /**
         * Indica la orientaci&oacute;n del bocadillo subyacente.
         * @since Tryer 1.0, november 2016
         */
        boolean bOrientation;

        /**
         * Indica la orientaci&oacute;n del texto.
         * @since Tryer 1.0, november 2016
         */
        boolean tOrientation;

        /**
         * Indica la siguiente secuencia de acciones.
         * @since Tryer 1.0, november 2016
         */
        byte nextSec;

        /**
         * Duraci&oacute;n de la animaci&oacute;n del texto.
         * @since Tryer 1.0,november 2016
         */
        long textAnimLen;

        /**
         * Duraci&oacute;n de la &uacute;ltima escena de texto.
         * @since Tryer 1.0, november 2016
         */
        long finalTextAnimLen;

        /**
         * Duraci&oacute;n que deber&aacute; ocurrir para iniciar el texto.
         * @since Tryer 1.0, november 2016
         */
        long animationTime;

        /**
         * Receptor de eventos de animaci&oacute;n.
         * @since Tryer 1.0, november 2016
         */
        Animator.AnimatorListener animListener;

        /**
         * Vista animada que muestra una animación para indicar que se debe tocar la pantalla.
         * @since Tryer 1.0, december 2016
         */
        AnimationDrawable taps;

        /**
         * <tt>true</tt> indica que debe utilizar el globo existente (de existir), <tt>false</tt>
         * indica que debe esperar a que termine.
         * @since Tryer 1.0, december 2016
         */
        boolean overwrite;

        /**
         * Referencia al texto a mostrar.
         * @since Tryer 1.0, december 2016
         */
        CharSequence text;

        /**
         * Referencia al contenedor de la animaci&oacute;n que invita al jugador a tocar la
         * pantalla.
         * @since Tryer 1.0, december 2016
         */
        ImageView toTap;

        /**
         * Identificador de la narraci&oacute;n del di&aacute;logo.
         * @since Tryer 1.0, march 2017
         */
        int dialogTrackId;

        /**
         * Periodo de tiempo en el que espera que termine la animaci&oacute;n.
         * @since Tryer 1.0, november 2016
         */
        static final byte RETRY_PERIOD = 35;

        @Override
        public void run() {
            if(animationTime > Tryer.RETRY_PERIOD || inflatedTextBubbles.get(bubble.getId())) {
                animationTime -= Tryer.RETRY_PERIOD;
                handler.removeCallbacks(this);
                handler.postDelayed(this, Tryer.RETRY_PERIOD);
                return;
            }//espera el tiempo solicitado
            handler.removeCallbacks(this);
            animationTime = 0;
            bubble.animate().cancel();
            initDialog(bubble, textV, dialogId, character, bOrientation, tOrientation,
                    taps, toTap, nextSec, dialogTrackId, textAnimLen, finalTextAnimLen, 0,
                    animListener);
        }//run

    }//Tryer runnable

}//GameActivity
