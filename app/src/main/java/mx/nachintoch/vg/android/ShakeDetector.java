package mx.nachintoch.vg.android;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;

/**
 * Detecta movimientos equivalentes a sacudir el dispositivo.
 * @author <a href="mailto:contact@nachintoch.mx" >Manuel "Nachintoch" Castillo</a>
 * @author <a href="http://stackoverflow.com/users/443387/n-joy" >N-JOY</a>
 * @version 1.0, december 2015
 * @since Misterios en Pregui 1.0, december 2015
 */
public class ShakeDetector implements SensorEventListener {

    // atributos de clase

    /**
     * Recuerda cuando fue la &uacute;ltima vez que se hizo una lectura.
     * @since Shake Detector 1.0, december 2015
     */
    private long lastUpdate;

    /**
     * Recuerda el &uacute;ltimo valor de x le&iacute;do.
     * @since Shake Detector 1.0, december 2015
     */
    private float lastX;

    /**
     * Recuerda el &uacute;ltimo valor de y le&iacute;do.
     * @since Shake Detector 1.0, december 2015
     */
    private float lastY;

    /**
     * Recuerda el &uacute;ltimo valor de z le&iacute;do.
     * @since Shake Detector 1.0, december 2015
     */
    private float lastZ;

    /**
     * Mantiene una tarea a realizar cuando se detecte que el dispositivo es agitado.
     * <b>IMPORTANTE</b> a pesar de pedir un <i>Runnable</i>, no se le dedica un hilo especial a
     * esta tarea, sino que se ejecuta en el mismo que donde sea utilizado el sensor.
     * @since Shake Detector 1.0, december 2015
     */
    private final Runnable TODO;

    /**
     * Umbral de moivimiento bajo el que se considera que el dispositivo ha sido agitado.
     * @since Shake Detector 1.0, december 2015
     */
    public static final int SHAKE_THRESHOLD = 250;

    // métodos constructores

    /**
     * Construye un detector de agitamiento con la tarea a procesar dada.
     * @param toRun - La tarea que deber&aacute; ejecutarse cuando se detecte que el dispositivo
     * es agitado.
     * @since Shake Detector 1.0, december 2015
     */
    public ShakeDetector(Runnable toRun) {
        TODO = toRun;
    }//consctructor

    // métodos de implementación

    @Override
    public void onSensorChanged(SensorEvent event) {
        if(event.sensor.getType() != Sensor.TYPE_ACCELEROMETER) {
            return;
        }//ignora eventos que le puedan pasar no relacionados al acelerómetro
        long time = System.currentTimeMillis();
        long diffTime = time -lastUpdate;
        if(diffTime > 100) {//TODO remove hardcode
            lastUpdate = time;
            float x = event.values[0];
            float y = event.values[1];
            float z = event.values[2];
            float speed = Math.abs(x +y +z -lastX -lastY -lastZ) /diffTime *1000;
            if(speed > SHAKE_THRESHOLD) {
                TODO.run();
            }//si pasó el umbral de agitamiento
            lastX = x;
            lastY = y;
            lastZ = z;
        }//sólo actua cada 100 ms
    }//onSensorChanged

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }//onAccuracyChanged

}//ShakeDetector Sensor Listener
