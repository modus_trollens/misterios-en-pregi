package mx.nachintoch.vg.android;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import mx.nachintoch.vg.chaton.MainMenu;
import mx.nachintoch.vg.chaton.R;

/**
 * Interfaz que define el comportamiento del men&uacute; principal y las acciones en los fragmentos
 * para escoger y editar una partida (de crisma).
 * @author <a href="mailto:contact@nachintoch.mx" >Manuel "Nachintoch" Castillo</a>
 * @version 1.0, january 2016
 * @since Misterios en Pregui 0.5, january 2016
 */
public interface SessionManager {

    /**
     * Cambia la partida seleccionada actualmente.
     * @param party - La partida que se quiere escojer ahora.
     * @since Session Manager 1.0, january 2016
     */
    void setCurrentParty(byte party);

    /**
     * Indica la partida seleccionada actualmente.
     * @return byte - El identificador de la partida actual.
     * @since Session Manager 1.0, january 2016
     */
    byte getCurrentParty();

    /**
     * Solicita el inicio del juego.
     * @param party - La partida con la que se quiere iniciar a jugar.
     * @param progress - El progreso le&iacute;do de la partida en cuesti&oacute;n.
     * @since Session Manager 1.0, january 2016
     */
    void startGame(byte party, int progress);

    /**
     * Realiza la animaci&oacute;n de girar el men&uacute; de selecci&oacute;n de partidas.
     * @since Main Menu 1.0, december 2015
     */
    void flipMenu();

    // clases anidadas

    /**
     * Fragmento especial para la vista de selecci&oacute;n de partida.
     * @author <a href="mailto:manuel_castillo_cc@ciencias.unam.mx" >Manuel "Nachintoch"
     * Castillo</a>
     * @version 3.0, february 2017
     * @since Main Menu 1.0, december 2015
     */
    abstract class PartySelectorFragment extends Fragment {

        // atirbutos de clase

        /**
         * Referencia a la vista padre.
         * @since Party Selector Fragment 1.0, december 2015
         */
        private MainMenu parent;

        /**
         * Referencia al t&iacute;tulo del selector.
         * @since Party Selector Fragment 1.0, december 2015
         */
        private TextView selectorTitle;

        /**
         * Referencia a la vista de texto con el nombre de la partida 1.
         * @since Main Menu 1.0, december 2015
         */
        private TextView party1Name;

        /**
         * Referencia a la vista de texto con el nombre de la partida 2.
         * @since Main Menu 1.0, december 2015
         */
        private TextView party2Name;

        /**
         * Referencia a la vista de texto con el nombre de la partida 3.
         * @since Main Menu 1.0, december 2015
         */
        private TextView party3Name;

        /**
         * Referencia al bot&oacute;n jugar.
         * @since Main Menu 1.0, december 2015
         */
        private Button playButton;

        /**
         * Referencia al bot&oacute;n copiar.
         * @since Main Menu 1.0, december 2015
         */
        private Button copyButton;

        /**
         * Referencia al bot&oacute;n borrar.
         * @since Main Menu 1.0, december 2015
         */
        private Button deleteButton;

        /**
         * Recuerda que partida se est&aacute; editando.
         * @since Main Menu 1.0, december 2015
         */
        private byte editingParty;

        /**
         * Indica si se est&aacute; copiando una partida.
         * @since Main Menu 1.0, december 2015
         */
        private boolean isCopying;

        /**
         * Indica si la partida en el &iacute;dice dado es copiable.
         * @since Main Menu 0.5, december 2015
         */
        private boolean[] copyable;

        /**
         * Contiene el progreso de todas las partidas.
         * @since Main Menu 1.0, december 2015
         */
        private Integer[] partyProgress;

        /**
         * Indica que el men&uacute; est&aacute; disponible para interactuar.
         * @since Session Manager 1.0, january 2016
         */
        protected boolean isReady;

        /**
         * Contiene el n&uacute;mero m&aacute;ximo de partidas a manejar en este juego.
         * @since Party Selector Fragment 3.0, febrary 2017
         */
        protected int maxPartyNumber;

        /**
         * Identificador del n&uacute;mero m&aacute;ximo de sesiones a manejar en el juego.
         * @since Party Selector Fragment 3.0, february 2017
         */
        public static final String PARTY_NUMBER_ID =
                "mx.nachintoch.vg.SessionManager$PARTY_NUMBER_ID";

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstance) {
            View self = inflater.inflate(R.layout.party_selector, container, false);
            maxPartyNumber = savedInstance.getInt(PARTY_NUMBER_ID, 3);
            partyProgress = new Integer[maxPartyNumber];
            party1Name = (TextView) self.findViewById(R.id.party1);
            party2Name = (TextView) self.findViewById(R.id.party2);
            party3Name = (TextView) self.findViewById(R.id.party3);
            playButton = (Button) self.findViewById(R.id.play_button);
            copyButton = (Button) self.findViewById(R.id.copy_button);
            deleteButton = (Button) self.findViewById(R.id.delete_button);
            selectorTitle = (TextView) self.findViewById(R.id.select_title);
            View.OnClickListener defaultListener = new View.OnClickListener() {
                @Override
                public void onClick(final View v) {
                    parent.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            manageParty(v);
                        }//run
                    });//*/
                }//onClick*/
            };
            setItemsOnClickListener(defaultListener);
            return self;
        }//onCreateView

        @Override
        public void onDestroyView() {
            party1Name = party2Name = party3Name = selectorTitle = null;
            playButton = copyButton = deleteButton = null;
            super.onDestroyView();
        }//onDestroyView

        @Override
        public void onAttach(Context activity) {
            super.onAttach(activity);
            parent = (MainMenu) activity;
        }//onAttach

        @Override
        public void onAttach(Activity activity) {
            super.onAttach(activity);
            parent = (MainMenu) activity;
        }//onAttach

        /**
         * Responde a los toques hechos sobre uno de los elementos en el men&uacute; de partidas.
         * @param which - El elemento tocado.
         * @since Main Menu 1.0, december 2015
         */
        public abstract void manageParty(View which);

        /**
         * Altera el color del nombre de la partida seleccionada.
         * @param which - Identificador de la partida seleccionada.
         * @param colorId - El identificador del color a usar.
         * @param toAll - Indica si se debe alterar el color del resto de las partidas o no.
         * @since Party Selector Fragment 1.0, december 2015
         */
        private void setSelectedColor(byte which, int colorId, boolean toAll) {
            int toSet = getResources().getColor(colorId);
            int _default = getResources().getColor(R.color.black);
            switch (which) {
                case 0 :
                    party1Name.setTextColor(toSet);
                    if(toAll) {
                        party2Name.setTextColor(_default);
                        party3Name.setTextColor(_default);
                    }//si debe cambiar el color de las demás
                    break;
                case 1 :
                    party2Name.setTextColor(toSet);
                    if(toAll) {
                        party1Name.setTextColor(_default);
                        party3Name.setTextColor(_default);
                    }//si debe cambiar el color de las demás
                    break;
                case 2 :
                    party3Name.setTextColor(toSet);
                    if(toAll) {
                        party1Name.setTextColor(_default);
                        party2Name.setTextColor(_default);
                    }//si debe cambiar el color de las demás
            }//altera los casos para cuando se seleccciona un elemento
        }//setSelectedColor

        /**
         * Asigna el receptor de eventos para los elementos interactivos en pantalla.
         * @param listener - El receptor de eventos a utilizar.
         * @since Session Manager 1.0, january 2016
         */
        private void setItemsOnClickListener(View.OnClickListener listener) {
            party1Name.setOnClickListener(listener);
            party2Name.setOnClickListener(listener);
            party3Name.setOnClickListener(listener);
            playButton.setOnClickListener(listener);
            copyButton.setOnClickListener(listener);
            deleteButton.setOnClickListener(listener);
        }//setItemsOnClickListener

        /**
         * Asigna los datos de todas partidas, tal y como se leen de la base de gatos.
         * @param party - Los datos de las partidas.
         * @since Session Manager 1.0, january 2016
         */
        protected void setParty(Object[] party) {
            party1Name.setText(party[0].toString());
            party2Name.setText(party[2].toString());
            party3Name.setText(party[4].toString());
            for(int i = 0; i < maxPartyNumber; i++) {
                partyProgress[i] = (Integer) party[(i +1) *2 -1];
            }//carga los progresos
        }//setPartyNames

    }//PartySelector Fragment

    /**
     * Fragmento especial para la vista de de partida nueva.
     * @author <a href="mailto:manuel_castillo_cc@ciencias.unam.mx" >Manuel "Nachintoch"
     * Castillo</a>
     * @version 2.0, february 2017
     * @since Main Menu 1.0, december 2015
     */
    abstract class PartyEditorFragment extends Fragment {

        // atirbutos de clase

        /**
         * Referencia a la vista padre.
         * @since Party Editor Fragment 1.0, december 2015
         */
        private MainMenu parent;

        /**
         * Referencia a la vista de este mismo fragmento.
         * @since Party Editor Fragment 1.0, december 2015
         */
        private View self;

        /**
         * Referencia al campo de edici&oacute;n de texto del jugador.
         * @since Main Menu 1.0, december 2015
         */
        private EditText playerName;

        /**
         * Referencia al bot&oacute;n OK en pantalla.
         * @since Party Editor Fragment 1.0, january 2016
         */
        private View okButton;

        /**
         * Referencia al bot&oacute;n Cancelar en pantalla.
         * @since Party Editor Fragment 1.0, january 2016
         */
        private View cancelButton;

        /**
         * Identificador del receptor de eventos para los botones del fragmento.
         * @since Party Editor Fragment 2.0, february 2017
         */
        public static final String BUTTON1_LISTENER_ID =
                "mx.nachintoch.vg.SessionManager$BUTTON1_LISTENER_ID";
        public static final String BUTTON2_LISTENER_ID =
                "mx.nachintoch.vg.SessionManager$BUTTON2_LISTENER_ID";

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstance) {
            self = inflater.inflate(R.layout.party_editor, container, false);
            okButton = self.findViewById(R.id.ok_button);
            cancelButton = self.findViewById(R.id.cancel_button);
            setButtonsListeners((View.OnClickListener) savedInstance.get(BUTTON1_LISTENER_ID),
                    (View.OnClickListener) savedInstance.get(BUTTON2_LISTENER_ID));
            return self;
        }//onCreateView

        @Override
        public void onDestroyView() {
            okButton = cancelButton = null;
            self = null;
            super.onDestroyView();
        }//onDestroyView

        /**
         * Responde al toque sobre el bot&oacute;n OK en la edici&oacute;n de partida.
         * @since Main Menu 1.0, december 2015
         */
        public abstract void okSelected();

        @Override
        public void onAttach(Context activity) {
            super.onAttach(activity);
            parent = (MainMenu) activity;
        }//onAttach

        @Override
        public void onAttach(Activity activity) {
            super.onAttach(activity);
            parent = (MainMenu) activity;
        }//onAttach

        /**
         * Asigna los receptores de eventos para los botones mostrados en pantalla.
         * @param okListener - Receptor de eventos para el bot&oacute;n OK.
         * @param cancelListener - Receptor de eventos para el bot&oacute;n Cancelar.
         * @since Pary Editor Fragment 1.0, january 2016
         */
        private void setButtonsListeners(View.OnClickListener okListener,
                                         View.OnClickListener cancelListener) {
            okButton.setOnClickListener(okListener);
            cancelButton.setOnClickListener(cancelListener);
        }//setButtonsListener

    }//PartyEditorFragment

}//Session Manager Interface
