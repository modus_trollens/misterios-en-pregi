package mx.nachintoch.vg.android;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

/**
 * Permite al jugador dibujar sobre la pantalla.
 * @author <a href="mailto:contact@nachintoch.mx" >Manuel "Nachintoch" Castillo</a>
 * @version 2.0, november 2016
 * @since Nachintoch Android Library for Videogames 1.0, october 2016
 */
public class DrawView extends AppCompatImageView implements View.OnTouchListener {

    // atributos de clase

    /**
     * Lista de puntos por donde el usuario ha pasado el dedo.
     * @since Draw View 1.0, october 2012
     */
    private PointSet currentPoints;

    /**
     * Separa los puntos por toques.
     */
    private List<PointSet> pointsAndTouchs;

    /**
     * Indica si el canvas est&aacute; listo para responder a eventos de toque.
     * @since Draw View 1.1, december 2015
     */
    private boolean shouldRespond;

    /**
     * Camino que deja el dedo del usuario con cada arrastre.
     * @since Draw View 1.1, january 2016
     */
    private Path fingerPath;

    /**
     * Texto de sugerencia que se le mostrar&aacute;a al jugador.
     * @since Draw View 2.0, november 2016
     */
    private String hint;

    /**
     * Posici&oacute;n del hint.
     * @since Draw View 2.0, november 2016
     */
    private float hintX, hintY;

    /**
     * Propiedades del hint.
     * @since Draw View 2.0, november 2016
     */
    private Paint paint;

    /**
     * Referencia a un reproductor de sonido que debe reproducirse al dibujar sobre la pantalla.
     * @since Draw View 2.0, march 2017
     */
    private MusicHandler drawFx;

    /**
     * En ancho que deber&aacute;n tener los trazos en pantalla.
     * @since Draw View 2.0, march 2017
     */
    private int strokeWidth;

    /**
     * Indica el tama&ntilde;o por defecto que se le atribuye a una lista de puntos.
     * @since Draw View 1.1, january 2016
     */
    private static final short DEFAULT_LIST_SIZE = 1024;

    // métodos construcotres

    /**
     * Construye una vista de dibujado con el contexto dado.
     * @param context - El contexto de la aplicaci&oacute;n.
     * @since Draw View 1.0, october 2012
     */
    public DrawView(Context context) {
        super(context);
        currentPoints = new PointSet();
        setFocusable(true);
        setFocusableInTouchMode(true);
        setOnTouchListener(this);
        pointsAndTouchs = new ArrayList<>(DrawView.DEFAULT_LIST_SIZE);
        shouldRespond = false;
        fingerPath = new Path();
        strokeWidth = 2;
    }//constructor

    /**
     * Construye una vista de dibujado con el contexto y atributos dados.
     * @param context - El contexto de la aplicaci&oacute;n.
     * @param attrs - Los atributos definidos en el XML.
     * @since Draw View 1.1, december 2015
     */
    public DrawView(Context context, AttributeSet attrs) {
        super(context, attrs);
        currentPoints = new PointSet();
        setFocusable(true);
        setFocusableInTouchMode(true);
        setOnTouchListener(this);
        pointsAndTouchs = new ArrayList<>(DrawView.DEFAULT_LIST_SIZE);
        shouldRespond = false;
        fingerPath = new Path();
        strokeWidth = 2;
    }//constructor

    // métodos de modificación

    /**
     * Asigna el estado de respuesta.
     * @param should - <tt>true</tt> para indicar que debe responder al rastro del dedo del usuario,
     * <tt>false</tt> en otro caso.
     * @since Draw View 1.1, december 2015
     */
    public void setShouldRespond(boolean should) {
        shouldRespond = should;
    }//setShouldRespond

    /**
     * Agrega un hint a la vista de dibujo.
     * @param hint - El texto que se quiere mostrar.
     * @param x - La posici&oacute;n en X del hint relativo a esta vista.
     * @param y - La posici&oacute;n en Y del hint relativo a esta vista.
     * @param textSize - El tama&ntilde;o de letra.
     * @since color - El color del texto.
     * @param customFontName - El nombre de la fuente personalizada a usar.
     * @param context - El contexto de la aplicaci&oacute;n.
     * @since Draw View 2.0, november 2016
     */
    public void setHint(int hint, float x, float y, float textSize, int color,
                        String customFontName, Context context) {
        this.hint = context.getString(hint);
        hintX = x;
        hintY = y;
        paint = new Paint();
        paint.setTextSize(textSize);
        paint.setColor(color);
        if(customFontName != null) {
            paint.setTypeface(Typeface.createFromAsset(context.getAssets(),
                    "fonts/" +customFontName));
        }//si hay que usar una fuente personalizada
    }//setHint

    // métodos de acceso

    /**
     * Indica si el canvas deber&iacute;a responder o no.
     * @return boolean - El estado del canvas.
     */
    public boolean shouldRespond() {
        return this.shouldRespond;
    }//shouldRespond

    // métodos de implementación

    @Override
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        boolean first;
        if(hint != null && pointsAndTouchs.size() == 0) {
            canvas.drawText(hint, hintX, hintY, paint);
        }//muestra un hint
        for(PointSet touch : pointsAndTouchs) {
            fingerPath.reset();
            first = true;
            for(Point point : touch) {
                if(first) {
                    first = false;
                    fingerPath.moveTo(point.x, point.y);
                } else {
                    fingerPath.lineTo(point.x, point.y);
                }//si es el primer punto de la serie
            }//pinta todos los puntos
            canvas.drawPath(fingerPath, touch.color);
        }//pinta todos los toques*/
        first = true;
        fingerPath.reset();
        //fingerPath = new Path();
        for(Point point : currentPoints) {
            if(first) {
                first = false;
                fingerPath.moveTo(point.x, point.y);
            } else {
                fingerPath.lineTo(point.x, point.y);
            }//pinta un punto
        }//falta pintar los puntos no guardados
        canvas.drawPath(fingerPath, currentPoints.color);
    }//onDraw

    /**
     * Responde a los eventos t&aacute;ctiles.
     * @param _this - Referencia a si mismo.
     * @param event - El evento de entrda
     * @return boolean - Siempre devuelve falso.
     * @since Draw View 1.1, december 2015
     */
    public boolean onTouch(View _this, MotionEvent event) {
        if(shouldRespond) {
            if(event.getAction() == MotionEvent.ACTION_DOWN && drawFx != null &&
                    !drawFx.isPlaying()) {
                drawFx.rewind();
                drawFx.play();
            } if(event.getAction() == MotionEvent.ACTION_MOVE) {
                    Point point = new Point();
                    point.x = (int) event.getX();
                    point.y = (int) event.getY();
                    if (!currentPoints.contains(point)) {
                        currentPoints.add(point);
                        invalidate();
                    }//si el punto aún no existe
            } if(event.getAction() == MotionEvent.ACTION_UP) {
                pointsAndTouchs.add(currentPoints);
                currentPoints = new PointSet();
                invalidate();
            }//actua dependiendo el tipo de evento*/
        }//si debe responder él o el padre
        return true;
    }//onTouch

    /**
     * Borra el dibujo que se muestra en pantalla.
     * @since Draw View 1.1, january 2016
     */
    public void deleteDraw() {
        currentPoints = new PointSet();
        pointsAndTouchs = new ArrayList<>(DrawView.DEFAULT_LIST_SIZE);
        invalidate();
    }//deleteDraw

    /**
     * Asigna un reproductor de efectos de sonido que debe reproducirse al iniciar trazos sobre este
     * lienzo.
     * @param fx - El reproductor de efectos de sonido del lienzo.
     * @throws IllegalStateException - Si el reproductor dado es nulo, si no se le ha cargado
     * ninguna pista o si ya ha sido destruido.
     * @since Draw View 2.0, march 2017
     */
    public void setDrawFx(MusicHandler fx) throws IllegalStateException {
        try {
            fx.getTrackLength();
        } catch(NullPointerException e) {
            throw new IllegalStateException("Given music handler is null or non-initialized", e);
        }//verifica que el reproductor sea válido
        if(fx.isDisposed())
            throw new IllegalStateException("Given music handler is already disposed");
        this.drawFx = fx;
    }//setDrawFx

    /**
     * Asigna el ancho de los trazos en pantalla.
     * @param sw - El ancho de los trazos en pantalla.
     * @throws IllegalStateException - Si el ancho dado no es positivo.
     * @since Draw View 2.0, march 2017
     */
    public void setStrokeWidth(int sw) throws IllegalStateException {
        if(sw <= 0) throw new IllegalStateException("Stroke Width must be positive");
        this.strokeWidth = sw;
    }//setStrokeWidth

    // clases anidadas

    /**
     * Arreglo especial de puntos que adem&aacute;s de mantener la lista de putos en memoria,
     * tambi&eacute;n mantiene el color con el que se piuntaron.
     * @author <a href="mailto:manuel_castillo_cc@ciencias.unam.mx" >Manuel "Nachintoch"
     * Castillo</a>
     * @version 1.0, december 2015
     * @since Draw View 1.0, december 2015
     */
    protected class PointSet extends ArrayList<Point> {

        /**
         * El color de los puntos.
         * @since Point Set 1.0, december 2015
         */
        private Paint color;

        // métodos constructores

        /**
         * Construye un conjunto colorado de puntos.
         * @since Point Set 1.0, december 2015
         */
        private PointSet() {
            super(DrawView.DEFAULT_LIST_SIZE);
            color = new Paint();
            color.setColor(Color.BLACK);
            color.setAntiAlias(true);
            color.setStyle(Paint.Style.STROKE);
            color.setStrokeWidth(strokeWidth);
        }//contructor

        // métodos de modificación

        /**
         * Asigna el color con el que se van a pintar los puntos.
         * @param color - El color para los puntos.
         * @since Point Set 1.0, december 2015
         */
        public void setColor(int color) {
            this.color.setColor(color);
        }//setColor

        /**
         * Asigna el grosor de la linea a dibujar con los puntos.
         * @param stroke - El grosor del dibujado.
         * @since Point Set 1.0, december 2015
         */
        public void setStroke(float stroke) {
            this.color.setStrokeWidth(stroke);
        }//setStroke

        // métodos de implementación

        @Override
        public boolean add(Point point) {
            return !super.contains(point) && super.add(point);
        }//add

    }//PointSet ArrayList

}//DrawView
