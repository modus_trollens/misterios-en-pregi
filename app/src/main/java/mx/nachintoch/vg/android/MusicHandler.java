package mx.nachintoch.vg.android;

import android.content.Context;
import android.media.MediaPlayer;
import android.net.Uri;

import java.io.File;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Envuelve un reproductor de audio que implementa efectos de fundido.
 * @author <a href="http://stackoverflow.com/users/1689647/sngreco" >sngreco</a>
 * @author <a href="mailto:contact@nachintoch.mx" >Manuel "Nachintoch" Castillo</a>
 * @version 1.2, march 2017
 * @since Nachintoch Android Library for Videogames 1.0, october 2016
 */
public class MusicHandler {

    // atributos de clase

    /**
     * Referencia al reproductor de multimedios del sistema.
     * @since Music Handler 1.0, november 2012
     */
    private MediaPlayer player;

    /**
     * Mantiene el nivel de volumen actual.
     * @since Music Handler 1.0, november 2012
     */
    private int currentVolume;

    /**
     * Indica si el reproductor ha sido liberado (como pokemon)
     * @since Music Handler 1.1, december 2015
     */
    private boolean isDisposed;

    /**
     * Indica si el reproductor est&aacute; haciendo un fundido de salida.
     * @since Music Handler 1.1, december 2015
     */
    private boolean isFadingOut;

    /**
     * Referencia al contexto de la aplicaci&ocute;n.
     * @since Music Handler 1.0, november 2012
     */
    private final Context CONTEXT;

    /**
     * Volumen m&iacute;nimo del reproductor (en porcentaje respecto al volumen del sistema).
     * @since Music Handler 1.0, november 2012
     */
    public static final int MIN_VOLUME = 0;

    /**
     * Volumen m&aacute;ximo del reproductor (en porcentaje respecto al volumen del sistema).
     * @since Music Handler 1.0, november 2012
     */
    public static final int MAX_VOLUME = 100;

    /**
     * Retraso por defecto para retardar el efecto de fundido del sonido en reproducci&oacute;n.
     * @since Music Handler 1.1, december 2015
     */
    public static final int DEFAULT_DELAY = 750;

    // métodos constructores

    /**
     * Construye un manejador de sonido con referencia al contexto de la aplicaci&ocute;n.
     * @param context - El contexto de la aplicaci&oacute;n.
     * @since Music Handler 1.0, november 2012
     */
    public MusicHandler(Context context) {
        this.CONTEXT = context;
        isDisposed = false;
        isFadingOut = false;
    }//constructor

    // métodos de implementación

    /**
     * Carga un archivo de m&uacute;sica desde una carpeta en el dispositivo (<b>&iexcl;para
     * usar memoria esterna declarar en el manifiesto de la aplicaci&oacute;n!</b>)
     * @param path - Ruta al archivo de musica que se quiere reproducir.
     * @param shouldLoop - <tt>true</tt> indica que la pista debe repetirse indefinidamente.
     * <tt>false</tt> en otro caso.
     * @since Music Handler 1.0, november 2012
     */
    public void load(String path, boolean shouldLoop) {
        player = MediaPlayer.create(CONTEXT, Uri.fromFile(new File(path)));
        player.setLooping(shouldLoop);
    }//load

    /**
     * Reproduce un archivo de sonido a decodificar dentro de la aplicaci&oacute;n.
     * @param resID - El identificador del recurso de sonido de la aplicaci&oacute;n (R.raw.algo)
     * @param shouldLoop - <tt>true</tt> indica que la pista debe repetirse indefinidamente.
     * <tt>false</tt> en otro caso.
     * @since Music Handler 1.0, november 2012
     */
    public void load(int resID, boolean shouldLoop) {
        if(player != null) player.release();
        isDisposed = false;
        player = MediaPlayer.create(CONTEXT, resID);
        player.setLooping(shouldLoop);
    }//load

    /**
     * Reproduce la m&uacute;sica.
     * @since Music Handler 1.1, december 2015
     */
    public void play() {
        if(!player.isPlaying()) {
            player.start();
        }//si no está reproduciendo
    }//play

    /**
     * Pausa la m&uacute;sica.
     * @since Music Hnalder 1.1, december 2015
     */
    public void pause() {
        if(!isDisposed && player.isPlaying()) {
            player.pause();
        }//si estpa tocando
    }//stop

    /**
     * Reproduce con efecto de fundido de entrada.
     * @param duration - La duraci&oacute;n del retraso del efecto de fundido.
     * @since Music Handler 1.0, november 2012
     */
    public void fadePlay(int duration) {
        currentVolume = duration > 0 ? MIN_VOLUME : MAX_VOLUME;
        updateVolume(0);
        play();
        if(duration > 0) {
            final Timer timer = new Timer(true);
            TimerTask timerTask = new TimerTask() {
                @Override
                public void run() {
                    updateVolume(1);
                    if(currentVolume == MAX_VOLUME) {
                        timer.cancel();
                        timer.purge();
                    }//si llega al tope
                }//run
            };
            int delay = duration /MAX_VOLUME;
            if(delay == 0) {
                delay = 1;
            }//calcula el retraso
            timer.schedule(timerTask, delay, delay);
        }//si hay que aplicar retraso alguno
    }//fadePlay

    /**
     * Pausa la m&uacute;sica con efecto de fundido de salida.
     * @param duration - La duraci&acute;n del efecto de fundido de salida.
     * @since Music Handler 1.0, november 2012
     */
    public void fadePause(int duration) {
        isFadingOut = true;
        currentVolume = duration > 0 ? MAX_VOLUME : MIN_VOLUME;
        updateVolume(0);
        if(duration > 0) {
            final Timer timer = new Timer(true);
            TimerTask timerTask = new TimerTask() {
                @Override
                public void run() {
                    updateVolume(-1);
                    if (currentVolume == MIN_VOLUME) {
                        pause();
                        timer.cancel();
                        timer.purge();
                        isFadingOut = false;
                    }//si alcanza el mínimo
                }//run
            };
            int delay = duration / MAX_VOLUME;
            if (delay == 0) {
                delay = 1;
            }//si se hace 0 el retraso
            timer.schedule(timerTask, delay, delay);
        } else {
            isFadingOut = false;
        }//si hay que aplicar retraso alguno
    }//fadeStop

    /**
     * Termina la m&uacute;sica con efecto de fundido de salida y libera los recursos asociados al
     * reproductor.
     * @param duration - La duraci&oacute;n del efecto de fundido.
     * @since Music Handler 1.1, december 2015
     */
    public void fadeRelease(int duration) {
        isFadingOut = true;
        currentVolume = duration > 0 ? MAX_VOLUME : MIN_VOLUME;
        updateVolume(0);
        if(duration > 0) {
            final Timer timer = new Timer(true);
            TimerTask timerTask = new TimerTask() {
                @Override
                public void run() {
                    updateVolume(-1);
                    if (currentVolume == MIN_VOLUME) {
                        pause();
                        isFadingOut = false;
                        timer.cancel();
                        timer.purge();
                        dispose();
                    }//si alcanza el mínimo
                }//run
            };
            int delay = duration / MAX_VOLUME;
            if (delay == 0) {
                delay = 1;
            }//si se hace 0 el retraso
            timer.schedule(timerTask, delay, delay);
        } else {
            isFadingOut = false;
        }//si hay que aplicar retraso alguno
    }//fadeRelease

    /**
     * Rebobina la pista.
     * @since Music Handler 1.1, december 2015
     */
    public void rewind() {
        player.seekTo(0);
    }//rewind

    /**
     * Suelta el reproductor de m&uacute;sica y libera los recursos asociados a &eacute;l. Esto es
     * muy importante para evitar el agotamiento de recursos en dispositivos de gama media-baja.
     * @since Music Handler 1.1, december 2015
     */
    public void dispose() {
        if(player == null) {
            isDisposed = true;
            return;
        } else if(isDisposed) {
            return;
        }//si ya está deshecho
        player.release();
        player = null;
        isDisposed = true;
    }//dispose

    /**
     * Devuelve la duraci&oacute;n de la pista en milisegundos.
     * @return int - La duraci&oacute;n de la pista.
     * @since Music Handler 1.1, december 2015
     */
    public int getTrackLength() {
        return player.getDuration();
    }//getTrackLength

    /**
     * Indica si el reproductor ha sido liberado (tambien como pokemon)
     * @return boolean - <tt>true</tt> indica que el reproductor ha sido liberado y que ya no puede
     * volver a ser utilizado. <tt>false</tt> indica que el reproductor sigue vivo y sigue siendo
     * &uacute;til.
     * @since Music Handler 1.1, december 2015
     */
    public boolean isDisposed() {
        return isDisposed;
    }//isDisposed

    /**
     * Indica si el reproductor est&aacute; realizando un fundido de salida.
     * @return boolean - <tt>true</tt> si se est&aacute; realizando un fundido de salida.
     * <tt>false</tt> en otro caso.
     * @since Music Handler 1.1, december 2015
     */
    public boolean isFadingOut() {
        return isFadingOut;
    }//isFadingOut

    /**
     * Indica si el reproductor est&aacute; tocando m&ucute;sica.
     * @return boolean - <tt>true</tt> indica que el reproductor en efecto est&aacute; tocando
     * m&uacute;sica. <tt>false</tt> en otro caso.
     * @since Music Handler 1.1, december 2015
     */
    public boolean isPlaying() {
        return player.isPlaying();
    }//isPlaying

    /**
     * Indica si el reproductor ha sido inicializado o no.
     * @return boolean - <tt>true</tt> - si el reproductor est&aacute; inicializado. <tt>false</tt>
     * en otro caso.
     * @since Music Handler 1.2, march 2017
     */
    public boolean isInitialized() {
        return player != null;
    }//isInitialized

    /**
     * Cambia el volumen del reproductor. El valor debe estar entre los m&iacute;nimos y
     * m&aacute;ximos definidos por esta clase.
     * @param volume - El nuevo volumen
     * @see #MAX_VOLUME
     * @see #MIN_VOLUME
     * @since Music Handler 1.2, april 2017
     */
    public void setVolume(int volume) {
        updateVolume(volume);
    }//setVolume

    // métodos auxiliares

    /**
     * Realiza elefecto de fundido, cambiando el volumen de la pista en reproducción.
     * @param volumeChange - El cambio de volumen.
     * @since Music Handler 1.0, november 2012
     */
    private void updateVolume(int volumeChange) {
        if(player == null || isDisposed) {
            return;
        }//fix a bug
        currentVolume += volumeChange;
        if(currentVolume < MIN_VOLUME) {
            currentVolume = MIN_VOLUME;
        } else if(currentVolume > MAX_VOLUME) {
            currentVolume = MAX_VOLUME;
        }//asigna valor del volumen actual
        float volume = 1 -(float) (Math.log(MAX_VOLUME -currentVolume) /Math.log(MAX_VOLUME ));
        if(volume < 0) {
            volume = 0;
        } else if(volume > 1) {
            volume = 1;
        }//asigna valor del volumen actual
        try {
            player.setVolume(volume, volume);
        } catch(IllegalStateException e) {
            Logger.getLogger(GameActivity.LOGGER_ID).log(Level.INFO, "Can't change volume");
        }//si ocurre un problema al actualizar el volumen
    }//updateVolume

}//music handler
