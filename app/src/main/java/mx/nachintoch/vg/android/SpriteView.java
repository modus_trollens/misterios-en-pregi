package mx.nachintoch.vg.android;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.view.MotionEvent;
import android.widget.ImageView;

/**
 * Vista de im&aacute;gen que permite propagar los toques a la vista superior en la jerarqu&iacute;a
 * de pantallas de Android.
 * @author <a href="mailto:contact@nachintoch.mx" >Manuel "Nachintoch" Castillo</a>
 * @version 1.0, november 2016
 * @since Nachintoch Android Library for Videogames 1.0, october 2016
 */
public class SpriteView extends ImageView {

    // atributos de clase

    /**
     * Cambia el estado del sprite para recibir eventos o delegarlos a la vista superior.
     * @since Sprite View 1.0, november 2016
     */
    private boolean active;

    /**
     * Identificador del cuadro activo actualmente.
     * @since Sprite View 1.0, november 2016
     */
    private int currentFrameId;

    /**
     * Diccionario que agrupa los cuadros de este sprite usando su identificador generado por
     * R.drawable
     * @since Sprite View 1.0, november 2016
     */
    public final SparseArray<Drawable> FRAMES;

    /**
     * N&uacute;mero m&aacute;ximo de cuadros permitidos para un sprite. Est&aacute;n limitados
     * por cuestiones de rendimiento.
     * @since Sprite View 1.0, november 2016
     */
    public static final byte MAXIMUM_FRAME_NUMBER = GameActivity.MAXIMUM_ANIMATOR_THREADS;

    // métodos constructores

    /**
     * Crea el sprite.
     * @param context - El contexto de la aplicaci&oacute;n.
     * @since Sprite View 1.0, november 2016
     */
    public SpriteView(Context context) {
        super(context);
        FRAMES = new SparseArray<>(SpriteView.MAXIMUM_FRAME_NUMBER);
        active = true;
    }//constructor

    /**
     * Crea el sprite.
     * @param context - El contexto de la aplicaci&oacute;n.
     * @param attrs - Los aributos a aplicar al campo de texto.
     * @since Sprite View 1.0, november 2016
     */
    public SpriteView(Context context, AttributeSet attrs) {
        super(context, attrs);
        FRAMES = new SparseArray<>(SpriteView.MAXIMUM_FRAME_NUMBER);
        active = true;
    }//constructor

    // métodos de modificación

    /**
     * Cambia el estado del sprite para recibir eventos o delegarlos a la vista superior.
     * @param act - <tt>true</tt> indica que el sprite debe recibir eventos.
     * @since Sprite View 1.0, november 2016
     */
    public void setActive(boolean act) {
        this.active = act;
    }//setActive

    /**
     * Agrega un cuadro al sprite.
     * @param drawable - El identificador del recurso dado de la forma R.drawable.desired_res
     * @param context - El contexto de la aplicaci&oacute;n.
     * @throws IllegalStateException - Si se intentan agregar m&aacute;s cuadros de los considerados
     * para el rendimiento de la aplicaci&oacute;n.
     * @since Sprite View 1.0, november 2016
     */
    public void loadFrame(int drawable, Context context) throws IllegalStateException {
        if(FRAMES.size() == SpriteView.MAXIMUM_FRAME_NUMBER) {
            throw new IllegalStateException("Maximum frames reached for sprite " +toString() +"\n" +
                    "If you REALLY need to use more frames per sprite than "
                    +SpriteView.MAXIMUM_FRAME_NUMBER +", then consider modifing the default " +
                    "value for constant SpriteView.MAXIMUM_FRAME_NUMBER This may affect " +
                    "application's performance");
        }//si ya se ha cargado el límite de sprites permitidos
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            FRAMES.append(drawable, getResources().getDrawable(drawable, context.getTheme()));
        } else {
            FRAMES.append(drawable, getResources().getDrawable(drawable));
        }//agrega un nuevo cuadro
    }//addFrame

    /**
     * Recicla los cuadros solicitados.
     * @param drawableId - El identificador del recurso que se desea reciclar.
     * @since Sprite View 1.0, november 2016
     */
    public void recycleFrame(int drawableId) {
        if(FRAMES.get(drawableId) instanceof BitmapDrawable) {
            BitmapDrawable bitmapD = (BitmapDrawable) FRAMES.get(drawableId);
            bitmapD.getBitmap().recycle();
        }//si es instancia de bitmap drawable
        FRAMES.remove(drawableId);
    }//recycleFrame

    // métodos de acceso

    /**
     * Indica el estado del sprite.
     * @return boolean - <tt>true</tt> indica que el sprite debe recibir eventos.
     * @since Sprite View 1.0, november 2016
     */
    public boolean isActive() {
        return active;
    }//isActive

    /**
     * Devuelve el identificador del cuadro actual del sprite.
     * @return currentFrameId - El identificador del cuadro actual.
     * @since Sprite View 1.0, november 2016
     */
    public int getCurrentFrameId() {
        return currentFrameId;
    }//getCurrentFrameId

    // métodos de implementación

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if(active) {
            super.onTouchEvent(event);
        }//si está activo
        return active;
        /*super.onTouchEvent(event);
        return active;*/
    }//onTouchEvent

    /**
     * Cambia la imagen en la vista del sprite.
     * @param drawable - El identificador del recurso que quiere ubicarse en esta imagen.
     * @since Sprite View 1.0, novemeber 2016
     */
    public void changeFrame(int drawable) {
        setImageDrawable(FRAMES.get(drawable));
        currentFrameId = drawable;
    }//setDrawable

}//Sprite View
