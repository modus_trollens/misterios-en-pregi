package mx.nachintoch.vg.android;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;

/**
 * Clase con el prop&oacute;sito de representar un envolvente invisible para una burbuja de texto,
 * de forma que sea posible escalarlo (y reflejarlo) en cualquier direcci&oacute;n Esta clase;
 * adem&aacute;s recuerda la escala original de la burbuja de texto, lo que evita
 * comportamientos no adecuados al redimensionarla entre animaciones. El no asignar el tama&ntilde;o
 * por defecto del bocadillo, se considera un estado ilegal del mismo.
 * @author <a href="mailto:contact@nachintoch.mx" >Manuel "Nachintoch" Castillo</a>
 * @version 1.0, march 2017
 * @since Nachintoch's Library for Android Game Development
 */

public class TextBubbleContainer extends LinearLayout {

    // atributos de clase

    /**
     * Tama&ntilode;os originales de la burbuja de texto.
     * @since Text Bubble Container 1.0, march 2017
     */
    protected float originalScaleX;
    protected float originalScaleY;

    /**
     * Recuerdan si ya se han asignado las escalas.
     * @since Text Bubble Container 1.0, march 2017
     */
    protected boolean xScaleSet;
    protected boolean yScaleSet;

    // métodos constructores

    /**
     * Construye un contenedor de texto a partir de tan solo el contexto dado.
     * @param context - El contexto de la aplicaci&oacute;n.
     * @since Text Bubble Container 1.0, mach 2017
     */
    public TextBubbleContainer(Context context) {
        super(context);
    }//constructor con contexto

    /**
     * Construye un contenedor de texto a partir del contexto y los atributos dados.
     * @param context - El contexto de la aplicaci&oacute;n.
     * @param attrs - Los atributos que se le quieren dar al contenedor.
     * @since Text bubble Container 1.0, march 2017
     */
    public TextBubbleContainer(Context context, AttributeSet attrs) {
        super(context, attrs);
    }//constructor con conexto y atributos.

    // métodos de modificación

    /**
     * Asigna el ancho original del contenedor de texto.
     * @param width - El ancho del contendor.
     * @since Text Bubble Container 1.0, march 2017
     */
    public void setOriginalScaleX(float width) {
        this.originalScaleX = width;
        xScaleSet = true;
    }//setOriginalScaleX

    /**
     * Asigna el alto original del contenedor de texto.
     * @param height - El alto del contenedor.
     * @since Text Bubble Container 1.0, march 2017
     */
    public void setOriginalScaleY(float height) {
        this.originalScaleY = height;
        yScaleSet = true;
    }//setOriginalScaleY

    // métodos de acceso

    /**
     * Indica el ancho original del contenedor de texto.
     * @return float - El ancho del contenedor.
     * @throws IllegalStateException - Si el ancho del contenedor no ha sido asignado.
     * @since Text Bubble Container 1.0, march 2017
     */
    public float getOriginalScaleX() throws IllegalStateException {
        if(!xScaleSet) throw new IllegalStateException("Text Container Scale X hasn't "
                +"been asigned");
        return this.originalScaleX;
    }//getOriginalScaleX

    /**
     * Indica el alto original del contenedor de texto.
     * @return float - El alto original del contenedor de texto.
     * @throws IllegalStateException - Si el alto del contenedor no ha sido asignado.
     * @since Text Bubble Container 1.0, march 2017
     */
    public float getOriginalScaleY() throws IllegalStateException {
        if(!yScaleSet) throw new IllegalStateException("Text Container Scale Y hasn't "
                +"been asigned");
        return this.originalScaleY;
    }//getOriginalScaleY

    // métodos de implementación

    /**
     * Resetea la escala de un globo de texto.
     * @throws IllegalStateException - Si las escalas originales no han sido asignadas.
     * @since Text bubble Container 1.0, march 2017
     */
    public void resetScales() throws IllegalStateException{
        if(!xScaleSet || !yScaleSet) throw new IllegalStateException("A Text Container Scale " +
                "hasn't been set: xScale: " +xScaleSet +" and yScale: " +yScaleSet);
        this.setScaleX(this.originalScaleX);
        this.setScaleY(this.originalScaleY);
    }//resetScales

    /**
     * Usa las escalas por defecto como las escalas originales a usar por el bocadillo.
     * @since Text bubble Container 1.0, march 2017
     */
    public void autoSetScales() {
        this.originalScaleX = super.getScaleX();
        this.originalScaleY = super.getScaleY();
        xScaleSet = yScaleSet = true;
    }//autoSetScales

}//TextBubbleContainer Layout
