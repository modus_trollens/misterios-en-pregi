package mx.nachintoch.vg.android;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Define un campo de texto personalizado, con la &uacute;nica funcionalidad adicional que permite
 * usar una fuente no predeterminada del sistema.
 * @author <a href="http://stackoverflow.com/users/748530/vins" >Vins</a>
 * @author <a href="mailto:contact@nachintoch.mx" >Manuel "Nachintoch" Castillo</a>
 * @version 1.0, october 2016
 * @since Nachintoch Android Library for Videogames 1.0, october 2016
 */
public class CustomFontTextView extends TextView {

    // atributos de clase

    // métodos constructores

    /**
     * Construye una vista de texto con fuente personalizada.
     * @param context - El contexto de la aplicaci&oacute;n.
     * @param attrs - Conjunto de atributos.
     * @param defStyle - ?
     * @since Custom Font Text View 1.0, october 2016
     */
    public CustomFontTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }//constructor con todo

    /**
     * Construye una vista de texto con fuente personalizada.
     * @param context - El contexto de la aplicaci&oacute;n.
     * @param attrs - Conjunto de atributos.
     * @since Custom Font Text View 1.0, october 2016
     */
    public CustomFontTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }//constructor con contexto y atributos

    /**
     * Construye una vista de texto con fuente personalizada.
     * @param context - El contexto de la aplicaci&oacute;n.
     * @since Custom Font Text View 1.0, october 2016
     */
    public CustomFontTextView(Context context) {
        super(context);
    }//consructor con contexto

    // métodos de implemenación

    /**
     * Asigna la fuente personalizada a el campo de texto
     * @param fontName - El nombre de la fuente a utilizar
     */
    public void setFont(String fontName) {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/" +fontName);
        setTypeface(tf);
    }//init

}//Custom Font Text View
