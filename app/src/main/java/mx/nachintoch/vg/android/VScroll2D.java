package mx.nachintoch.vg.android;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.ScrollView;

/**
 * Scroll vertical personalizado para usar con pantallas que permiten el desplazamiento total en 2D.
 * @author <a href="http://stackoverflow.com/users/451013/mahdi-hijazi" >Mahdi Hijazi</a>
 * @author <a href="mailto:contact@nachintoch.mx" >Manuel "Nachintoch" Castillo</a>
 * @version 1.0, october 2016
 * @since Nachintoch Android Library for Videogames 1.0, october 2016
 */
public class VScroll2D extends ScrollView {

    // métodos constructores

    /**
     * Construye un scroll vertical.
     * @param context - El contexto de la aplicaci&oacute;n.
     * @param attrs - Conjunto de atributos.
     * @param defStyle - ?
     * @since VScroll2D 1.0, october 2016
     */
    public VScroll2D(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }// constructor con tdo

    /**
     * Construye un scroll vertical.
     * @param context - El contexto de la aplicaci&oacute;n.
     * @param attrs - Conjunto de atributos.
     * @since VScroll2D 1.0, october 2016
     */
    public VScroll2D(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    /**
     * Construye un scroll vertical.
     * @param context - El contexto de la aplicaci&oacute;n.
     * @since VScroll2D 1.0, october 2016
     */
    public VScroll2D(Context context) {
        super(context);
    }

    // métodos de implementación

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        return false;
    }

    /**
     * Devuelve una animaci&oacute;n sobre Y de este scroll.
     * @param scrollTo - La posici&oacute;n en Y a donde se quiere scrollear
     * @return ObjectAnimator - El animador de este scroll.
     * @since Vertical Scroll 2D 1.0, november 2016
     */
    public ObjectAnimator getAnimateScroll(int scrollTo) {
        return ObjectAnimator.ofInt(this, "scrollY", scrollTo);
    }//animateScroll

}//VScroll2D
