#include <jni.h>
#include <string>

extern "C"
jstring
Java_mx_nachintoch_vg_chaton_MainActivity_stringFromJNI(
        JNIEnv* env,
        jobject /* this */) {
    std::string hello = "Hello from C++";
    return env->NewStringUTF(hello.c_str());
}
